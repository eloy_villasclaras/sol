exports.up = function (knex) {
    return knex.schema
        .createTable('user', function (table) {
            table.bigincrements('id').primary();
            table.string('username');
            table.string('email').defaultTo(null);
            table.string('googleId').defaultTo(null);
            table.boolean('isAdmin').defaultTo(false);
            table.boolean('isBlocked').defaultTo(false);
        })
        .createTable('document', function (table) {
            table.bigincrements('id').primary();
            table.biginteger('ownerId').unsigned().references('id').inTable('user');
            table.integer('status').defaultTo(0);
        })
        .createTable('version', function (table) {
            table.bigincrements('id').primary();
            table.biginteger('documentId').unsigned().references('id').inTable('document');
            table.biginteger('vid').unsigned();
            table.string('title');
            table.string('description');
            table.json('content');
        })
        .createTable('review', function (table) {
            table.bigincrements('id').primary();
            table.biginteger('userId').unsigned().references('id').inTable('user').onDelete('CASCADE');
            table.biginteger('documentId').unsigned().references('id').inTable('document').onDelete('CASCADE');
            table.integer('value').unsigned();
        });
};

exports.down = function (knex) {
    return knex.schema
        .dropTableIfExists('user')
        .dropTableIfExists('document')
        .dropTableIfExists('version')
        .dropTableIfExists('review');
};
