/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var extend = require('extend'),

        url = function (base, query) {
            let params = Object.keys(query).map(key => key + "=" + query[key]);
            return base + (params.length > 0 ? '?' + params.join('&') : '');
        },

        overwrite = function (base, query, params) {
            return url(base, extend({}, query, params));
        },

        path = function (from, to) {
            if (to.length === 0) {
                return from;
            } else if (!from || to.charAt(0) === '/') {
                return to;
            } else if (to.substr(0, 2) === './') {
                return from + to.substr(1);
            } else {
                var currentParts = from.substr(1).split('/'),
                    up = to.match(/^(\.\.\/)+/),
                    upLevels = up ? up[0].length / 3 : 1,
                    remainder = up ? to.substr(up[0].length) : to;

                return '/' + currentParts.slice(0, currentParts.length - upLevels).join('/') +
                    (remainder.length > 0 ? '/' + remainder : '');
            }
        };


    return {
        url: url,
        overwrite: overwrite,
        path: path
    }
}();
