/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var net = require('./net'),

        createDoc = function (data) {
            return net.post('/api/documents', data);
        },

        getDoc = function (id) {
            return net.get('/api/documents/' + id);
        },

        updateDoc = function (id, data) {
            return net.put('/api/documents/' + id, data);
        },

        listDocs = function () {
            return net.get('/api/documents');
        };

    return {
        createDoc: createDoc,
        getDoc: getDoc,
        updateDoc: updateDoc,
        listDocs: listDocs
    };

}();
