/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {Router, Route, IndexRoute, browserHistory} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import injectTapEventPlugin from "react-tap-event-plugin";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import theme from "./theme";
import store from "./stores/store";
import {updateLoginStatus} from "./stores/login-actions";
import App from "./components/app.jsx";
import HomePage from "./home/home-page.jsx";
import Elsewhere from "./components/elsewhere.jsx";
import ProfilePage from "./components/profile-page.jsx";
import ContentPage from "./components/content-page/content-page.jsx";
import DesignsHomePage from "./components/designs/home/designs-home-page.jsx.js";
import DesignCreatePage from "./components/designs/designs-create-page.jsx";
import DesignPage from "./components/designs/design/design-page.jsx";
import DesignMetadata from "./components/designs/design/metadata/design-metadata.jsx";
import DesignComponents from "./components/designs/design/components/design-components.jsx";
import DesignAnalysis from "./components/designs/design/analysis/design-analysis.jsx";

injectTapEventPlugin();

const history = syncHistoryWithStore(browserHistory, store);

store.dispatch(updateLoginStatus());

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={theme}>
            <Router history={history}>
                <Route path="/" component={App}>
                    <IndexRoute component={HomePage}/>
                    <Route path="" component={Elsewhere}>
                        <Route path="/login" component={DesignCreatePage}/>
                        <Route path="/profile" component={ProfilePage}/>
                        <Route path="/designs" component={DesignsHomePage}/>
                        <Route path="/designs/create" component={DesignCreatePage}/>
                        <Route path="/designs/:designID" component={DesignPage}>
                            <IndexRoute component={DesignMetadata}/>
                            <Route path="components" component={DesignComponents}/>
                            <Route path="analysis" component={DesignAnalysis}/>
                        </Route>
                        <Route path="*" component={ContentPage}/>
                    </Route>
                </Route>
            </Router>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('app')
);
