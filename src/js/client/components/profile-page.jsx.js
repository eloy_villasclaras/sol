/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import Helmet from "react-helmet";
import RaisedButton from "material-ui/RaisedButton";
import DocumentLayout from "./widgets/document-layout.jsx";
import {logout} from "../stores/login-actions";
import LoginPanel from "./login/login-panel.jsx";

const ProfilePage = ({login, onLogout}) => {
    if (login.user) {
        return (
            <DocumentLayout>
                <Helmet title="My profile"/>
                <p>{login.user.username}</p>
                <RaisedButton label="Logout" onClick={onLogout}/>
            </DocumentLayout>
        );
    } else {
        return (
            <DocumentLayout>
                <Helmet title="Login"/>
                <LoginPanel/>
            </DocumentLayout>
        );
    }
};

const mapStateToProps = state => {
    return {
        login: state.login
    };
};

const mapDispatchToProps = dispatch => ({
    onLogout: () => dispatch(logout())
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfilePage);
