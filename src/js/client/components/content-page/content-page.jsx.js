/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Helmet from "react-helmet";
import {connect} from "react-redux";
import {load} from "../../stores/content-page-actions";
import DocumentLayout from "../widgets/document-layout.jsx";
import Breadcrumbs from "./breadcrumbs.jsx";
import Markdown from "./markdown.jsx";

class ContentPage extends React.Component {
    componentDidMount() {
        this.props.onLoad(this.props.path);
    }

    componentDidUpdate(prevProps) {
        if (this.props.path !== prevProps.path) {
            this.props.onLoad(this.props.path);
        }
    }

    render() {
        let {path, contentPage: {content, loading}} = this.props;
        let titleMatch = content.match(/^#*(.*)$/m);
        let title = titleMatch ? titleMatch[1] : 'loading...';
        let options = {linkCurrent: path};

        return (
            <DocumentLayout>
                <Helmet title={title}/>
                <Breadcrumbs path={path} loading={loading} options={options}/>
                <Markdown content={content} options={options}/>
            </DocumentLayout>
        );
    }
}

const mapStateToProps = (state, ownProperties) => {
    return {
        contentPage: state.contentPage,
        path: ownProperties.location.pathname
    };
};

const mapDispatchToProps = dispatch => ({
    onLoad: path => dispatch(load(path))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContentPage);
