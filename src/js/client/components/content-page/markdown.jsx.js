/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {Link} from "react-router";
import urlUtils from "../../utils/url-utils";
import widgets from "./widgets";

const styles = {
    bold: {
        fontWeight: 500
    },
    italic: {
        fontStyle: 'italic'
    },
    code: {
        fontFamily: 'Roboto Mono',
        fontSize: '0.9em',
        color: '#666'
    },
    externalLinkIcon: {
        fontSize: '0.9em',
        marginLeft: '0.2em'
    }
};

const pathIsInternal = path => !path.match(/^https?:\/\//);

const internalPath = (path, options = {}, pathPrefix = '') => {
    let url = pathPrefix + (options.linkCurrent ? urlUtils.path(options.linkCurrent, path) : path);
    return options.linkHref ? options.linkHref(url) : url;
};

const makeHref = (path, options = {}, pathPrefix = '') => pathIsInternal(path) ?
    internalPath(path, options, pathPrefix) : path;

const makeStyleBlockMatch = (regex, style) => [
    regex,
    (match, i, options) => <span key={i} style={style}>{parseBlock(match[1], options)}</span>
];

const blockMatches = [
    /* image */
    [/^!\[([^\]]+)]\(([^\)]+)\)$/, (match, i, options) => (
        <img key={i} src={makeHref(match[2], options, '/images')} alt={match[1]}/>
    )],
    /* link */
    [/^\[([^\]]+)]\(([^\)]+)\)(\{((external)|\s)*})?$/, (match, i, options) => pathIsInternal(match[2]) ? (
        <Link key={i} to={internalPath(match[2], options)}>
            {parseBlock(match[1], options)}
        </Link>
    ) : (
        <a key={i} href={match[2]} target={match[5] ? '_blank' : '_self'}>
            {parseBlock(match[1], options)}
            {match[5] && <span className="material-icons" style={styles.externalLinkIcon}>launch</span>}
        </a>
    )],
    /* bold */
    makeStyleBlockMatch(/^\*\*([^\*]+)\*\*$/, styles.bold),
    /* italics */
    makeStyleBlockMatch(/^\*([^\*]+)\*$/, styles.italic),
    /* code */
    makeStyleBlockMatch(/^```([^`]+)```$/, styles.code),
    /* widget */
    [/^\{\{([^}]+)}}$/, (match, i, options) => {
        let [widget, ...params] = match[1].split(' ');
        return widget in widgets && widgets[widget](params, i, options);
    }]
];

const parseBlock = function (block, options) {
    var matches = block.trim().match(/(\{\{[^}]+}})|(```[^`]+```)|(\*\*[^\*]+\*\*)|(\*[^\*]+\*)|!?(\[[^\]]+]\([^\)]+\)({[^}]*})?)|([^\[\*`{]+)/g);
    return matches ? matches.map(function (group, i) {
        for (var j = 0; j < blockMatches.length; j++) {
            var match = group.match(blockMatches[j][0]);
            if (match) {
                return blockMatches[j][1](match, i, options);
            }
        }
        return <span key={i}>{group}</span>;
    }) : [];
};

const parse = function (content, options) {
    var lines = content.split('\n'),
        grouped = [],
        typed = [],
        elements = [],
        last = -1;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i].replace(/\s+$/, ''),
            fc = line.charAt(0);

        if (line.length === 0 || last < 0 || fc === '#' || fc === '-' || fc === '0') {
            if (line.length > 0) {
                last = grouped.length;
                grouped.push(line);
            } else {
                last = -1;
            }
        } else {
            grouped[last] += ' ' + line.replace(/^\s+/, '');
        }
    }

    last = null;

    for (i = 0; i < grouped.length; i++) {
        line = grouped[i];
        if (line.substr(0, 2) === '##') {
            typed.push({type: 'h2', content: parseBlock(line.substr(2), options)});
            last = null;
        } else if (line.substr(0, 1) === '-') {
            if (last && last.type === 'ul') {
                last.content.push(parseBlock(line.substr(1), options));
            } else {
                last = {type: 'ul', content: [parseBlock(line.substr(1), options)]};
                typed.push(last);
            }
        } else if (line.substr(0, 1) === '0') {
            if (last && last.type === 'ol') {
                last.content.push(parseBlock(line.substr(1), options));
            } else {
                last = {type: 'ol', content: [parseBlock(line.substr(1), options)]};
                typed.push(last);
            }
        } else if (line.substr(0, 1) === '>') {
            if (last && last.type === 'indent') {
                last.content.push(parseBlock(line.substr(1), options));
            } else {
                last = {type: 'indent', content: [parseBlock(line.substr(1), options)]};
                typed.push(last);
            }
        } else {
            typed.push({type: 'p', content: parseBlock(line, options)});
            last = null;
        }
    }


    for (i = 0; i < typed.length; i++) {
        var data = typed[i];
        if (data.type === 'h2') {
            elements.push(<h2 key={i}>{data.content}</h2>);
        } else if (data.type === 'p') {
            elements.push(<p key={i}>{data.content}</p>);
        } else if (data.type === 'indent') {
            elements.push(<p key={i} style={{marginLeft: 40}}>{data.content}</p>);
        } else if (data.type === 'ul' || data.type === 'ol') {
            var items = [];
            for (var j = 0; j < data.content.length; j++) {
                items.push(<li key={j}><p>{data.content[j]}</p></li>);
            }
            if (data.type === 'ul') {
                elements.push(<ul key={i}>{items}</ul>);
            } else {
                elements.push(<ol key={i}>{items}</ol>);
            }
        }
    }

    return elements;
};

export default ({content, options}) => {
    var elements = parse(content, options);
    return <div>{elements}</div>;
}
