/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {Link} from "react-router";
import RefreshIndicator from "material-ui/RefreshIndicator";

export default ({path, loading, options}) => {
    let parts = path.split('/').filter(function (part) {
        return part.length > 0;
    });
    let elements = [];

    if (parts.length > 1) {
        let acc = '';
        let iconStyle = {
            fontSize: '1em',
            verticalAlign: 'middle'
        };
        let linkHref = options.linkHref;

        parts.forEach(function (part, i) {
            let title = part.charAt(0).toUpperCase() + part.replace(/-/g, ' ').slice(1);
            if (i === parts.length - 1) {
                elements.push(<span key={acc}>{title}</span>);
            } else {
                let url = acc + '/' + part;
                let href = linkHref ? linkHref(url) : url;

                elements.push(<Link key={acc} to={href}>{title}</Link>);
                elements.push(<i key={'sep-' + i} style={iconStyle}
                                 className="material-icons">keyboard_arrow_right</i>);

                acc = url;
            }
        });
    }

    if (loading) {
        elements.push(
            <div key='ri' className="refresh-wrapper">
                <RefreshIndicator key="-p-" size={20} top={-15} left={10} status="loading"/>
            </div>
        );
    }

    return elements.length > 0 && <div className="breadcrumbs">{elements}</div>;
}
