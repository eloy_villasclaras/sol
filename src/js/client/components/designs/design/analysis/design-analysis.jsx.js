/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import Splitter from "../../../widgets/splitter.jsx";
import AnalysisList from "./forms/analysis-list.jsx";
import AnalysisVarList from "./forms/analysis-vars-list.jsx";
import {update} from "../../../../stores/design-actions";
import {select} from "../../../../stores/select-entity-actions";
import {editState} from "../../../../stores/process-state";

const DesignAnalysis = ({edit, components, analysis, analysisVars, info, selected, onUpdate, onSelect}) => {
    if (analysis) {
        let analysisList = (
            <div className="components-forms-container">
                <AnalysisList root={analysis} info={info} selectedId={selected.entityId}
                              edit={edit} onUpdate={onUpdate} onSelect={onSelect}/>
            </div>
        );

        if (selected) {
            let selectedAnalysis = analysis[selected.entityId];

            return (
                <Splitter vertical={true} defaultPosition={0.25}>
                    {analysisList}
                    <div className="components-forms-container">
                        <AnalysisVarList analysis={selectedAnalysis} vars={analysisVars} info={info}
                                         components={components} selectedVarId={selected.varId}
                                         edit={edit} onUpdate={onUpdate} onSelect={onSelect}/>
                    </div>
                </Splitter>
            );
        } else {
            return analysisList;
        }
    } else {
        return <div></div>;
    }
};


const mapStateToProps = (state) => {
    return {
        edit: editState(state),
        components: state.design.processed.components,
        analysis: state.design.processed.analysis,
        analysisVars: state.design.processed.analysisVars,
        info: state.design.processed.info,
        selected: state.selection.type === 'analysis' && state.selection
    };
};

const mapDispatchToProps = dispatch => ({
    onUpdate: changes => dispatch(update(changes)),
    onSelect: (type, eid, vid) => dispatch(select(type, eid, vid))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignAnalysis);
