/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import VarEqField from "../../widgets/fields/eq/var-eq-field.jsx";

export default class ComponentVarEqField extends React.Component {
    constructor() {
        super();

        this.onSelect = this.onSelect.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onSelect(varId) {
        this.props.onSelect('analysis', this.props.analysis.fullId, varId);
    }

    onChange(value) {
        this.props.onUpdate({
            type: 'set-analysis-var-value',
            itemId: this.props.analysis.fullId,
            key: 'eq',
            varId: this.props.varId,
            value: value
        });
    }

    render() {
        let {analysis, varId, info, vars, selectedVarId, edit} = this.props;
        let name = info.analysis[analysis.fullId].vars[varId];
        let isSelected = selectedVarId === varId;

        return <VarEqField entity={analysis} varId={varId} edit={edit}
                           vars={vars} info={info.analysis} isSelected={isSelected}
                           onSelect={this.onSelect} onChange={this.onChange}
                           label={name[0]} units={name[1]}/>;
    }
}
