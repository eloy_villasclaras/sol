/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import CidTreeDlgField from "../../widgets/fields/entity-cid-tree-dlg-field.jsx";

export default class AnalysisCidSelectField extends React.Component {
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        let {onUpdate, analysis, attr} = this.props;
        onUpdate({
            type: 'set-analysis-value',
            itemId: analysis.fullId,
            key: attr,
            value: value
        });
    }

    render() {
        let {components, analysis, attr, title, label, onSelect, selectedVarId, edit, info} = this.props;

        return (
            <div className="var-field">
                <CidTreeDlgField type="analysis" id={analysis.fullId} entity={analysis}
                                 attr={attr} componentRoot={components} acceptType="element"
                                 onSelect={onSelect} onChange={this.handleChange}
                                 rootLabel="Components" isSelected={selectedVarId === attr}
                                 info={info.components} label={label} title={title} edit={edit}/>
            </div>
        );
    }
}