/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FontIcon from "material-ui/FontIcon";
import IconButton from "material-ui/IconButton";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import TitleWidget from "../../widgets/title-widget.jsx.js";
import {Tree, TreeLeaf} from "../../widgets/tree";
import {menuIconSettings, treeIconMenuSettings, treeIconButtonSettings, treeTitleSettings} from "../../../../../theme";


class AbstractAnalysisTreeComponent extends React.Component {
    constructor() {
        super();

        this.handleSettingsMenuChange = this.handleSettingsMenuChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }


    handleSelect() {
        this.props.onSelect('analysis', this.props.item.fullId, null);
    }

    handleSettingsMenuChange(e, value) {
        switch (value) {
            case 'delete':
                if (this.props.selectedId === this.props.item.fullId) {
                    this.props.onSelect(null, null, null);
                }

                this.props.onUpdate({type: 'delete-analysis', itemId: this.props.item.fullId});
                break;
            default:
                if (this.handleMenuChange) {
                    this.handleMenuChange(e, value);
                }
                break;
        }
    }

    title() {
        let {item, isRoot, info, selectedId} = this.props;

        let title = (isRoot && 'Analysis') || info.analysis[item.fullId].title;
        let iconName = !isRoot && info.analysis[item.fullId].icon;
        let selectable = !isRoot;
        let selected = selectable && item.fullId === selectedId;
        let onClick = selectable ? this.handleSelect : false;
        let style = treeTitleSettings[selected ? 'selected' : 'unselected'];
        let menu = this.settingsMenu();

        return <TitleWidget title={title} icon={iconName} onClick={onClick}
                            style={style.text} iconStyle={style.icon} menu={menu}/>;
    }

    settingsMenu() {
        let items = (this.menuItems && this.menuItems()) || [];

        if (!this.props.isRoot) {
            if (items.length > 0) {
                items.push(<MenuDivider key="b_delete"/>);
            }

            items.push(<MenuItem key="delete" primaryText="Delete" value="delete"
                                 leftIcon={<FontIcon className="material-icons">delete</FontIcon>}/>);
        }

        if (items.length > 0) {
            var icon = <IconButton iconClassName="material-icons" {...treeIconButtonSettings}
                                   key="settings">more_vert</IconButton>;

            return (
                <IconMenu iconButtonElement={icon} {...treeIconMenuSettings}
                          onChange={this.handleSettingsMenuChange}
                          anchorOrigin={{horizontal:"right",vertical:"top"}}
                          targetOrigin={{horizontal:"left",vertical:"top"}}>{items}</IconMenu>
            );
        } else {
            return false;
        }
    }
}

class AnalysisComponent extends AbstractAnalysisTreeComponent {
    render() {
        return this.title();
    }
}

class GroupComponent extends AbstractAnalysisTreeComponent {
    menuItems() {
        return [
            <MenuItem key="analysis" primaryText="Add analysis" value="analysis"
                      leftIcon={<FontIcon {...menuIconSettings} className="material-icons">assessment</FontIcon>}/>
        ];
    }

    handleMenuChange(event, value) {
        if (value === 'analysis') {
            this.props.onUpdate({
                type: 'create-analysis'
            });
        }
    }

    render() {
        let {item, selectedId, info, onUpdate, onSelect, isRoot} = this.props;

        let children = Object.keys(item).map(childId => (
                <TreeLeaf key={childId}>
                    <AnalysisComponent item={item[childId]} selectedId={selectedId} info={info}
                                       onUpdate={onUpdate} onSelect={onSelect}/>
                </TreeLeaf>
            )
        );

        return (
            <Tree header={this.title()} className="component-list"
                  selected={!isRoot && selectedId === item.fullId}>
                {children}
            </Tree>
        );
    }
}

export default props => <GroupComponent {...props} item={props.root} isRoot={true}/>
