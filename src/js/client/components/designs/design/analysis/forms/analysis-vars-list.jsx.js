/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import IconButton from "material-ui/IconButton";
import AnalysisTextField from "../fields/analysis-text-field.jsx.js";
import AnalysisVarEqField from "../fields/analysis-var-eq-field.jsx";
import AnalysisSelectField from "../fields/analysis-select-field.jsx";
import AnalysisCidSelectField from "../fields/analysis-cid-select-field.jsx";
import TitleWidget from "../../widgets/title-widget.jsx.js";

const types = {
    'inf-point': <span>Point at <span style={{fontSize: '1.4em', verticalAlign: 'middle'}}>∞</span></span>
};

const distributions = {
    '2d': "2D",
    '3d': '3D'
};


export default props => {
    let {analysis, onSelect} = props;

    let titleField = <AnalysisTextField {...props} attr="title" defaultValue={analysis.fullId}/>;
    let close = <IconButton iconClassName="material-icons" key="settings"
                            onClick={() => onSelect(null)}>close</IconButton>;

    return (
        <div className="component-vars">
            <TitleWidget title={titleField} icon='assessment' menu={close}/>
            <AnalysisSelectField {...props} options={types} attr="type"
                                            label="Type" title="Select analysis type"/>
            <AnalysisSelectField {...props} options={distributions} attr="distribution"
                                            label="Distribution" title="Select ray distribution"/>
            <AnalysisCidSelectField {...props} attr="entrance"
                                               label="Entrance" title="Select entrance element"/>
            <AnalysisVarEqField {...props} varId="a"/>
            <AnalysisVarEqField {...props} varId="n"/>
        </div>
    );
}
