/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import DraggableItemComponent from "./draggable-item-component";
import shapeOps from "../../../../../../logic/optics/shape-ops";

const N_POINTS = 50;
const NF_POINTS = 2 * N_POINTS + 1;

export default class MirrorSvg extends DraggableItemComponent {
    drawItem(mirror) {
        let strokeWidth = 2 * this.props.scale;
        let {hap, shape, hr} = mirror;

        if (hap && shape) {
            let yStep = (hap - hr) / N_POINTS;

            if (hr > 0) {
                let ft = [];
                let bt = [];
                let fb = [];
                let bb = [];

                ft.length = fb.length = bt.length = bb.length = N_POINTS + 1;
                for (let i = 0; i <= N_POINTS; i += 1) {
                    let y = hr + i * yStep;
                    let x = shapeOps.drawPoint(shape, y);

                    ft[i] = `${x},${y}`;
                    fb[i] = `${x},${-y}`;
                    bt[N_POINTS - i] = `${x + strokeWidth},${y}`;
                    bb[N_POINTS - i] = `${x + strokeWidth},${-y}`;
                }

                return [
                    <polyline key="bt" points={bt.join(' ')} strokeWidth={strokeWidth} className="mirror-path-back"/>,
                    <polyline key="bb" points={bb.join(' ')} strokeWidth={strokeWidth} className="mirror-path-back"/>,
                    <polyline key="ft" points={ft.join(' ')} strokeWidth={strokeWidth} className="mirror-path"/>,
                    <polyline key="fb" points={fb.join(' ')} strokeWidth={strokeWidth} className="mirror-path"/>
                ];

            } else {
                let front = [];
                let back = [];

                front.length = back.length = NF_POINTS;
                front[N_POINTS] = "0,0";
                back[N_POINTS] = `${strokeWidth},0`;
                for (let i = 1; i <= N_POINTS; i += 1) {
                    let y = i * yStep;
                    let x = shapeOps.drawPoint(shape, y);

                    front[N_POINTS + i] = `${x},${y}`;
                    front[N_POINTS - i] = `${x},${-y}`;
                    back[N_POINTS + i] = `${x + strokeWidth},${y}`;
                    back[N_POINTS - i] = `${x + strokeWidth},${-y}`;
                }

                return [
                    <polyline key="back" points={back.join(' ')} strokeWidth={strokeWidth}
                              className="mirror-path-back"/>,
                    <polyline key="front" points={front.join(' ')} strokeWidth={strokeWidth} className="mirror-path"/>
                ];
            }
        } else {
            return false;
        }
    }
}
