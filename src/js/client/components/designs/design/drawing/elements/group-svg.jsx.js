/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import DraggableItemComponent from "./draggable-item-component";
import MirrorSvg from "./mirror-svg.jsx.js";
import LensSvg from "./lens-svg.jsx.js";
import elementOps from "../../../../../../logic/optics/element-ops";

const itemShape = (item) => {
    if (item.type === 'element') {
        switch (item.optics) {
            case 'mirror':
                return MirrorSvg;
            case 'lens':
                return LensSvg;
            default:
                return false;
        }
    } else {
        return GroupSvg;
    }
};

export default class GroupSvg extends DraggableItemComponent {
    drawItem(group, selected, parentLevel) {
        let props = this.props;
        let elements = [];

        if (parentLevel === 1) {
            let lineProps = {
                x0: 0,
                y0: 0,
                className: "group-coord-line",
                strokeWidth: props.scale
            };

            elements.push(
                <g key="group-coords">
                    <line {...lineProps} x2={1000} y2={0}/>
                    <line {...lineProps} x2={-1000} y2={0}/>
                    <line {...lineProps} x2={0} y2={-1000}/>
                    <line {...lineProps} x2={0} y2={1000}/>
                </g>
            );
        } else if (selected) {
            let bb = elementOps.boundingBox(group);

            elements.push(<rect key="bb" className="group-bounding-box" strokeWidth={props.scale}
                                x={bb[0] - 10} y={-bb[3] - 10}
                                width={bb[2] - bb[0] + 20} height={bb[3] - bb[1] + 20}
                                rx={5} ry={5}/>
            )
        }

        Object.keys(group.children).forEach(id => {
                let child = group.children[id];
                let ItemShape = itemShape(child);

                if (ItemShape) {
                    elements.push(<ItemShape key={id} {...props} item={child}/>);
                }
            }
        );

        return elements;
    }
}
