/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import extend from "extend";
import AbstractMouseDragComponent from "../../../../widgets/abstract-mouse-drag-component";
import systemManager from "../../../../../../logic/optics/system-manager";

export default class DraggableItemComponent extends AbstractMouseDragComponent {
    constructor() {
        super();
        extend(this.state, {
            x: false,
            y: false,
            rot: false
        });

        this.handleClick = this.handleClick.bind(this);
        this.startRotateDrag = this.startRotateDrag.bind(this);
        this.startMoveDrag = this.startMoveDrag.bind(this);
    }


    startRotateDrag(e) {
        this.startDrag(e, 'rotate');
    }

    startMoveDrag(e) {
        this.startDrag(e, 'move');
    }

    _parentRot() {
        let item = this.props.item;
        return (item.absPos.rot - item.pos.rot) * Math.PI / 180;
    }

    _posValue(item, key) {
        return this.state[key] === false ? item.pos[key] : this.state[key];
    }

    handleDrag(newX, oldX, newY, oldY, data) {
        let {item, page2viewbox} = this.props;

        let newState = {};
        let x = this._posValue(item, 'x');
        let y = this._posValue(item, 'y');

        let newPos = page2viewbox(newX, newY);
        let oldPos = page2viewbox(oldX, oldY);

        if (data === 'move') {
            let parentRot = this._parentRot();
            let cosRot = Math.cos(parentRot);
            let sinRot = Math.sin(parentRot);
            let dx = newPos[0] - oldPos[0];
            let dy = newPos[1] - oldPos[1];
            let idx = dx * cosRot + dy * sinRot;
            let idy = dy * cosRot - dx * sinRot;

            if (idx) {
                newState.x = x + idx;
            }

            if (idy) {
                newState.y = y + idy;
            }

        } else if (data === 'rotate') {
            var absPos = item.absPos;

            newState.rot = this._posValue(item, 'rot') +
                (Math.atan2(newPos[1] - absPos.y, newPos[0] - absPos.x) -
                Math.atan2(oldPos[1] - absPos.y, oldPos[0] - absPos.x)) * 180 / Math.PI;
        }

        this.setState(newState);
    }

    handleDragEnd() {
        let {item, onUpdate} = this.props;

        let actions = ['x', 'y', 'rot']
            .filter(key => this.state[key] !== false)
            .map(key => ({
                    type: 'set-component-var-value',
                    itemId: item.fullId,
                    key: 'eq',
                    varId: key,
                    value: this.state[key].toString()
                })
            );

        if (actions.length > 0) {
            onUpdate(actions);
        }

        this.setState({
            rot: false,
            x: false,
            y: false
        });
    }

    handleClick(e) {
        e.preventDefault();
        e.stopPropagation();

        this.props.onSelect('component', this.props.item.fullId);
    }


    renderControls() {
        let dragging = this.state.dragging;
        let isMoving = dragging && dragging.data === 'move';
        let isRotating = dragging && dragging.data === 'rotate';

        return [(
            <g key="rotate"
               className={"item-controls item-controls-rotate" + (isRotating ? ' active' : '')}
               transform="translate(100,0)"
               onMouseDown={this.startRotateDrag}>
                <circle cx="0" cy="0" r="5"/>
                <path transform="scale(.01) translate(-256,-256)"
                      d="M0,512h202.105V309.895l-69.961,69.961C100.447,348.158,80.842,304.368,80.842,256c0-96.737,78.421-175.158,175.158-175.158  S431.158,159.263,431.158,256c0,62.158-32.382,116.763-81.197,147.855l58.104,58.105C471.118,415.329,512,340.447,512,256  C512,114.618,397.382,0,256,0S0,114.618,0,256c0,70.685,28.651,134.685,74.98,181.026L0,512z"/>
            </g>
        ), (
            <g key="move"
               className={"item-controls item-controls-move" + (isMoving ? ' active' : '')}
               onMouseDown={this.startMoveDrag}>
                <circle cx="0" cy="0" r="5"/>
                <path transform="scale(.008) translate(-512,-512)"
                      d="M0 499.968l171.864 -171.864l0 119.133l275.373 0l0 -275.373l-119.133 0l171.864 -171.864 171.864 171.864l-119.133 0l0 275.373l275.373 0l0 -119.133l171.864 171.864 -171.864 171.864l0 -119.133l-275.373 0l0 275.373l119.133 0l-171.864 171.864 -171.864 -171.864l119.133 0l0 -275.373l-275.373 0l0 119.133z"/>
            </g>
        )];
    }

    renderSvgGroup() {
        let {item, selectedId, edit} = this.props;
        let transform = `translate(${this._posValue(item, 'x')},${-this._posValue(item, 'y')}) rotate(${-this._posValue(item, 'rot') || 0})`;
        let isSelected = selectedId === item.fullId;
        let parentLevel = selectedId && !isSelected && systemManager.parentLevel(selectedId, item.fullId);
        let itemSvg = this.drawItem(item, isSelected, parentLevel);

        let controls = edit && isSelected && this.renderControls(item);

        return (
            <g transform={transform} onClick={this.handleClick}>
                {itemSvg}
                {controls}
            </g>
        );
    }

    render() {
        return this.renderSvgGroup();
    }
}
