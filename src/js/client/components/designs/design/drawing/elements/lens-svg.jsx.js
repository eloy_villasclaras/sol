/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import DraggableItemComponent from "./draggable-item-component";
import shapeOps from "../../../../../../logic/optics/shape-ops";

const N_POINTS = 20;
const NF_POINTS = 2 * N_POINTS + 1;

export default class LensSvg extends DraggableItemComponent {
    drawItem(lens) {
        let strokeWidth = this.props.scale;
        let {hap, frontShape, backShape, th} = lens;

        if (hap && frontShape && backShape) {
            let yStep = hap / N_POINTS;
            let hth = .5 * th;
            let points = [];

            points.length = 2 * NF_POINTS;
            points[N_POINTS] = `${-hth},0`;
            points[NF_POINTS + N_POINTS] = `${hth},0`;

            for (let i = 1; i <= N_POINTS; i += 1) {
                let y = i * yStep;
                let xf = shapeOps.drawPoint(frontShape, y, -hth);
                let xb = shapeOps.drawPoint(backShape, y, hth);

                points[N_POINTS + i] = `${xf},${y}`;
                points[N_POINTS - i] = `${xf},${-y}`;
                points[NF_POINTS + N_POINTS - i] = `${xb},${y}`;
                points[NF_POINTS + N_POINTS + i] = `${xb},${-y}`;
            }

            return <polygon points={points.join(' ')} strokeWidth={strokeWidth} className="lens-path"/>;
        } else {
            return false;
        }
    }
}
