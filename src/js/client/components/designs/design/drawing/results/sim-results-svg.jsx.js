/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";

export default class ElementsSvg extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.results !== nextProps.results ||
            this.props.simulating !== nextProps.simulating ||
            this.props.scale !== nextProps.scale;
    }

    render() {
        let {scale, results = {}} = this.props;
        let keys = Object.keys(results);

        let shadows = keys.map(id => {
            let traces = results[id].map((trace, i) => {
                let points = trace.map(point => `${point[0]},${-point[1]}`).join(' ');
                return <polyline key={i} className="shadow" points={points} strokeWidth={3 * scale}/>;
            });
            return <g key={`s-${id}`}>{traces}</g>;
        });

        let lines = keys.map(id => {
            let traces = results[id].map((trace, i) => {
                let points = trace.map(point => `${point[0]},${-point[1]}`).join(' ');
                return <polyline key={i} points={points} strokeWidth={scale}/>;
            });
            return <g key={id}>{traces}</g>;
        });

        return <g className="sim-results-svg">{shadows}{lines}</g>;
    }
}

