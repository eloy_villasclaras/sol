/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';
import React from "react";
import extend from "extend";
import {connect} from "react-redux";
import AbstractMouseDragComponent from "../../../widgets/abstract-mouse-drag-component";
import {wrap} from "../../../widgets/sized-wrapper.jsx";
import ElementsSvg from "./elements/elements-svg.jsx";
import SimResultsSvg from "./results/sim-results-svg.jsx.js";
import domOps from "../../../../utils/dom-ops";
import {update} from "../../../../stores/design-actions";
import {select} from "../../../../stores/select-entity-actions";
import {editState} from "../../../../stores/process-state";


class SvgDrawing extends AbstractMouseDragComponent {
    constructor() {
        super();
        extend(this.state, {
            zoom: 1000,
            dx: 0,
            dy: 0
        });

        this.handleWheel = this.handleWheel.bind(this);
        this.page2viewbox = this.page2viewbox.bind(this);
    }

    _svgSize() {
        let element = this.refs.svg.parentNode;
        return {
            width: element.clientWidth,
            height: element.clientHeight
        };
    }

    _svgSizeOffset() {
        let r = this._svgSize();
        let offset = domOps.offset(this.refs.svg.parentNode);
        return extend(r, offset);
    }

    handleDrag(newX, oldX, newY, oldY) {
        let {zoom, dx, dy} = this.state;
        let {width, height} = this._svgSize();

        let clientSize = Math.max(width, height);
        let viewBoxScale = zoom / clientSize;
        let newDx = (oldX - newX) * viewBoxScale + dx;
        let newDy = (oldY - newY) * viewBoxScale + dy;

        return {
            dx: newDx,
            dy: newDy
        };
    }

    handleWheel(e) {
        let {zoom, dx, dy} = this.state;
        var {width, height, offsetLeft, offsetTop} = this._svgSizeOffset();

        let clientSize = Math.max(width, height);
        let viewBoxScale = zoom / clientSize;
        let clientX = e.pageX - offsetLeft - .5 * width;
        let clientY = e.pageY - offsetTop - .5 * height;
        let viewBoxX = clientX * viewBoxScale + dx;
        let viewBoxY = clientY * viewBoxScale + dy;
        let newZoom = zoom * (1 + (e.deltaY > 0 ? .1 : -.1));
        let newScale = newZoom / clientSize;
        let newDx = viewBoxX - clientX * newScale;
        let newDy = viewBoxY - clientY * newScale;

        this.setState({
            zoom: newZoom,
            dx: newDx,
            dy: newDy
        });
    }

    page2viewbox(x, y) {
        let {zoom, dx, dy} = this.state;
        let {width, height, offsetLeft, offsetTop} = this._svgSizeOffset();
        let clientSize = Math.max(width, height);
        let viewBoxScale = zoom / clientSize,

            clientX = x - offsetLeft - .5 * width,
            clientY = y - offsetTop - .5 * height,

            viewBoxX = clientX * viewBoxScale + dx,
            viewBoxY = clientY * viewBoxScale + dy;

        return [viewBoxX, -viewBoxY];
    }

    render() {
        let {rootComponent, size} = this.props;
        if (rootComponent) {
            let {zoom, dx, dy} = this.state;
            let coordMax = .5 * zoom;
            let viewBox = `${-coordMax + dx} ${-coordMax + dy} ${zoom} ${zoom}`;
            let scale = zoom / Math.max(size.width, size.height);

            let lineProps = {
                x1: 0,
                y1: 0,
                className: 'coord-line',
                strokeWidth: scale
            };

            return (
                <svg ref="svg" className="drawing-svg flex-content"
                     width="0" height="0" viewBox={viewBox} preserveAspectRatio="xMidYMid slice"
                     onWheel={this.handleWheel} onMouseDown={this.startDrag}>
                    <line {...lineProps} x2={coordMax + dx} y2={0}/>
                    <line {...lineProps} x2={-coordMax + dx} y2={0}/>
                    <line {...lineProps} x2={0} y2={-coordMax + dy}/>
                    <line {...lineProps} x2={0} y2={coordMax + dy}/>
                    <SimResultsSvg {...this.props} scale={scale} page2viewbox={this.page2viewbox}/>
                    <ElementsSvg {...this.props} scale={scale} page2viewbox={this.page2viewbox}/>
                </svg>
            );
        } else {
            return false;
        }
    }

}


const SvgSizedWrapper = wrap(SvgDrawing, {className: "flex-columns"});

const mapStateToProps = (state) => {
    return {
        edit: editState(state),
        simulating: state.design.simulating,
        rootComponent: state.design.processed.components,
        results: state.design.processed.results,
        selectedId: state.selection.type === 'component' && state.selection.entityId
    };
};

const mapDispatchToProps = dispatch => ({
    onUpdate: changes => dispatch(update(changes)),
    onSelect: (type, eid, vid) => dispatch(select(type, eid, vid))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SvgSizedWrapper);
