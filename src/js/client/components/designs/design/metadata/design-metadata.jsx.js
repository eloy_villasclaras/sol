/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import MetadataForm from "./metadata-form.jsx";
import {update} from "../../../../stores/design-actions";
import {editState} from "../../../../stores/process-state";


const DesignMetadata = ({edit, title, description, onUpdate}) => edit ? (
    <div className="design-metadata flex-columns">
        <MetadataForm title={title} description={description} onUpdate={onUpdate}/>
    </div>
) : (
    <div className="design-metadata flex-columns">
        <h2>{title}</h2>
        <p>{description}</p>
    </div>
);


const mapStateToProps = (state) => {
    return state.design.document ? {
        edit: editState(state),
        title: state.design.document.title,
        description: state.design.document.description
    } : {
        edit: false,
        title: '',
        description: ''
    }
};

const mapDispatchToProps = dispatch => ({
    onUpdate: changes => dispatch(update(changes))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignMetadata);
