/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import {loadDesign} from "../../../stores/design-actions";
import HelpWrapper from "../help/help-wrapper.jsx";
import Splitter from "../../widgets/splitter.jsx";
import DesignPageTitle from "./design-page-title.jsx";
import DesignToolbar from "./toolbar/design-toolbar.jsx";
import SvgDrawing from "./drawing/svg-drawing.jsx";
import Renderer3D from "./3d/renderer-3d.jsx";

class DesignPage extends React.Component {
    componentWillMount() {
        this.props.onLoadDesign(this.props.routeParams.designID);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.routeParams.designID !== this.props.routeParams.designID) {
            this.props.onLoadDesign(nextProps.routeParams.designID);
        }
    }

    render() {
        let {location, children, is3D} = this.props;
        let path = location.pathname;
        let currentSegmentMatch = path.match(/(\/designs\/[0-9]+)(.*)$/);
        let currentSegment = currentSegmentMatch[2];
        let designPath = currentSegmentMatch[1];
        let Renderer = is3D ? Renderer3D : SvgDrawing;

        return (
            <div className="flex-columns">
                <DesignPageTitle/>
                <DesignToolbar currentSegment={currentSegment} path={path} designPath={designPath}/>
                <div className="flex-columns">
                    <HelpWrapper basePath={path} query={location.query}>
                        <Splitter defaultPosition={0.25}>
                            {children}
                            <div className="flex-columns components-canvas-container">
                                <Renderer/>
                            </div>
                        </Splitter>
                    </HelpWrapper>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onLoadDesign: id => dispatch(loadDesign(id))
});


const mapStateToProps = (state) => {
    return {
        is3D: state.routing.locationBeforeTransitions.query.view === '3d'
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(DesignPage);
