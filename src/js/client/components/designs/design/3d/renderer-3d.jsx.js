/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import THREE from "three";
import clone from "clone";
import {wrap} from "../../../widgets/sized-wrapper.jsx";
import SceneManager from "./scene/scene-manager.jsx.js";
import WindowTimer from "./animate/window-timer";
import Animate from "./animate/animate";
import MouseImputComponent from "./mouse-input";

class Renderer3D extends MouseImputComponent {
    constructor() {
        super();

        this.state = {
            view: {
                zoom: 1,
                alt: .001,
                az: 0,
                x: 0,
                y: 0
            }
        };

        this._animate = new Animate(this._updateAnim.bind(this), new WindowTimer());

        this.handleDrag = this.handleDrag.bind(this);
        this.handleWheel = this.handleWheel.bind(this);
    }

    _updateAnim(values) {
        var view = clone(this.state.view);

        for (var key in view) {
            if (view.hasOwnProperty(key) && key in values) {
                view[key] = values[key];
            }
        }

        this.setState({view: view});
    }

    handleDrag(dx, dy, button) {
        if (dx !== 0) {
            if (button === 0) {
                this._animate.setAnim('az', 'follow', {
                    halfT: 100,
                    threshold: .001,
                    delta: -dx / 100,
                    max: Math.PI / 2,
                    min: -Math.PI / 2
                }, this.state.view.az);
            } else if (button === 2) {
                this._animate.setAnim('x', 'follow', {
                    halfT: 100,
                    threshold: .001,
                    delta: -dx
                }, this.state.view.x);
            }
        }
        if (dy !== 0) {
            if (button === 0) {
                this._animate.setAnim('alt', 'follow', {
                    halfT: 100,
                    threshold: .001,
                    delta: dy / 75,
                    max: Math.PI / 2,
                    min: -Math.PI / 2
                }, this.state.view.alt);
            } else if (button === 2) {
                this._animate.setAnim('y', 'follow', {
                    halfT: 100,
                    threshold: .001,
                    delta: dy
                }, this.state.view.y);
            }
        }
    }

    handleWheel(e) {
        e.preventDefault();

        if (e.deltaY) {
            this._animate.setAnim('zoom', 'follow', {
                halfT: 100,
                threshold: .01,
                deltaP: e.deltaY < 0 ? 1.2 : 1 / 1.2,
                min: 1,
                max: 100
            }, this.state.view.zoom);
        }
    }

    render() {
        let {size, rootComponent = {}, results = {}} = this.props;
        let view = this.state.view;

        let d = 1000 / view.zoom;

        let perspective = {
            lookAt: new THREE.Vector3(view.x, view.y, 0),
            pos: new THREE.Vector3(
                view.x + d * Math.sin(view.az) * Math.cos(view.alt),
                view.y + d * Math.sin(view.alt),
                d * Math.cos(view.az) * Math.cos(view.alt)
            ),
            up: new THREE.Vector3(
                Math.sin(view.alt) * Math.sin(view.az),
                Math.cos(view.alt),
                -Math.sin(view.alt) * Math.cos(view.az)
            ),
            far: 1e5,
            near: .1 * d,
            aspect: size.width / size.height,
            fov: 50
        };

        return (
            <div className="flex-columns renderer-3d"
                 onMouseDown={this.handleMouseDown}
                 onMouseUp={this.handleMouseUp}
                 onMouseMove={this.handleMouseMove}
                 onMouseLeave={this.handleMouseLeave}
                 onWheel={this.handleWheel}
                 onContextMenu={this.handleContextMenu}>
                <SceneManager rootComponent={rootComponent} results={results} perspective={perspective} size={size}/>
            </div>
        );
    }
}


const RendererSizedWrapper = wrap(Renderer3D, {className: "flex-columns"});

const mapStateToProps = (state) => {
    return {
        rootComponent: state.design.processed.components,
        results: state.design.processed.results
    };
};


export default connect(mapStateToProps, false)(RendererSizedWrapper);

