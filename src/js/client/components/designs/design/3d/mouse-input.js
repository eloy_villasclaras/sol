/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";

export default class MouseImputComponent extends React.Component {
    constructor() {
        super();
        this._mid = {
            p: [0, 0],
            down: false,
            moved: false
        }

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.handleMouseUp = this.handleMouseUp.bind(this);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.handleContextMenu = this.handleContextMenu.bind(this);
    }

    handleMouseDown(e) {
        e.preventDefault();
        this._mid.p[0] = e.clientX;
        this._mid.p[1] = e.clientY;
        this._mid.down = e.button;
        this._mid.moved = false;
    }

    handleMouseUp(e) {
        e.preventDefault();
        this._mid.down = false;
        if (!this._mid.moved && this.handleClick) {
            this.handleClick(e.clientX, e.clientY);
        }
    }

    handleMouseMove(e) {
        e.preventDefault();
        if (this._mid.down !== false &&
            (e.clientX !== this._mid.p[0] || e.clientY !== this._mid.p[1])) {
            this.handleDrag(e.clientX - this._mid.p[0], e.clientY - this._mid.p[1], this._mid.down);
            this._mid.p[0] = e.clientX;
            this._mid.p[1] = e.clientY;
            this._mid.moved = true;
        }
    }

    handleMouseLeave(e) {
        e.preventDefault();
        this._mid.down = false;
    }

    handleContextMenu(e) {
        e.preventDefault();
    }
}
