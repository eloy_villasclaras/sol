/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import lineal from "./types/lineal";
import follow from "./types/follow";
import poly3 from "./types/poly3";
import poly5 from "./types/poly5";

const types = {lineal, follow, poly3, poly5};

export default class {
    constructor(cb, timer) {
        this.timer = timer;
        this.animations = {};
        this.n = 0;
        this.cb = cb;

        this.timer.setFrameCallback(this.update.bind(this));
    }

    update(t) {
        let values = {};

        for (let id in this.animations) {
            if (this.animations.hasOwnProperty(id)) {
                let anim = this.animations[id];
                let handler = types[anim.type];
                let stop = handler.update(anim, t);

                values[id] = anim.v;

                if (stop) {
                    delete this.animations[id];
                    this.n--;
                }
            }
        }

        if (this.n === 0) {
            this.timer.disable();
        }

        this.cb(values);
    }

    setAnim(id, type, params, v0) {
        let t0 = this.timer.t();
        let handler = types[type];
        let exists = this.animations.hasOwnProperty(id);

        if (exists && type === this.animations[id].type) {
            handler.reset(this.animations[id], t0, params);
        } else {
            let anim = {
                type: type
            };
            handler.init(anim, t0, params, v0);
            this.animations[id] = anim;
        }

        if (!exists) {
            this.n++;
            if (this.n === 1) {
                this.timer.enable();
            }
        }
    }

    isAnimated(id) {
        return data.animations.hasOwnProperty(id);
    }

    cancel(id) {
        if (this.animations.hasOwnProperty(id)) {
            delete this.animations[id];
            this.n--;
            if (this.n === 0) {
                this.timer.disable();
            }
        }
    }
}