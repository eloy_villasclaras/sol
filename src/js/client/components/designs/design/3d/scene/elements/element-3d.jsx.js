/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */
'use strict';

import React from "react";
import THREE from "three";
import {Object3D} from "react-three";

export default ({component, rotateShape, children}) => {
    let p = new THREE.Vector3(component.absPos.x, component.absPos.y, 0);
    let az = Math.PI * (component.absPos.rot / 180 + (rotateShape ? -0.5 : 0));

    let r = new THREE.Euler(0, 0, az, 'XYZ');

    return (
        <Object3D position={p}>
            <Object3D rotation={r}>{children}</Object3D>
        </Object3D>
    );
}

