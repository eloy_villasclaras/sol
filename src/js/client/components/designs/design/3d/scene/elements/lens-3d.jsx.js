/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */
'use strict';

import React from "react";
import THREE from "three";
import {Mesh} from "react-three";
import CachedGeometryComponent from "../cached-geometry-mixin";
import Element3D from "./element-3d.jsx.js";
import shapePoints from "./shape-points";
import {lens as lensMaterial} from "./../materials";

const points = lens => {
    let hth = .5 * lens.th;
    let p = shapePoints(lens.hap, lens.frontShape, -hth, 0);
    let pb = shapePoints(lens.hap, lens.backShape, hth, 0);

    for (let i = pb.length - 1; i >= 0; i -= 1) {
        p.push(pb[i]);
    }
    return p;
};

export default class Lens3D extends CachedGeometryComponent {
    createGeometry() {
        return new THREE.LatheGeometry(points(this.props.component), 100);
    }

    render() {
        return (
            <Element3D component={this.props.component} rotateShape={true}>
                <Mesh geometry={this.geometry()} material={lensMaterial}/>
            </Element3D>
        );
    }
}

