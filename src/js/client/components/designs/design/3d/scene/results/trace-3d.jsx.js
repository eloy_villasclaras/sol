/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */
'use strict';

import React from "react";
import THREE from "three";
import {Line} from "react-three";
import {line as lineMaterial} from "../materials";
import CachedGeometryComponent from "../cached-geometry-mixin";

export default class Trace3D extends CachedGeometryComponent {

    createGeometry() {
        let trace = this.props.trace;
        let geometry = new THREE.Geometry();

        for (let i = 0; i < trace.length; i++) {
            geometry.vertices.push(new THREE.Vector3(trace[i][0], trace[i][1], trace[i][2]));
        }
        return geometry;
    }

    render() {
        return <Line geometry={this.geometry()} material={lineMaterial}/>;
    }
}
