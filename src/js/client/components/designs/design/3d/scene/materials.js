/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import THREE from "three";

export const mirror = new THREE.MeshPhongMaterial({
    color: new THREE.Color(0x2194ce),
    emissive: new THREE.Color(0x000000),
    specular: new THREE.Color(0x111111)
});

export const lens = new THREE.MeshPhongMaterial({
    color: new THREE.Color(0x2194ce),
    emissive: new THREE.Color(0x000000),
    specular: new THREE.Color(0x111111),
    transparent: true,
    opacity: .6
});

export const line = new THREE.LineBasicMaterial({color: new THREE.Color().setRGB(.8, .8, .4)});
