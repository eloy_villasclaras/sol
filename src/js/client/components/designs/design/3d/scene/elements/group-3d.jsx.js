/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {Object3D} from "react-three";
import Lens3D from "./lens-3d.jsx.js";
import Mirror3D from "./mirror-3d.jsx.js";

const Group3D = ({component}) => {
    let children = (component.children && Object.keys(component.children).map(key => {
            let child = component.children[key];
            let childProps = {key, component: child};

            if (child.type === 'group') {
                return <Group3D {...childProps}/>;
            } else if (child.optics === 'mirror') {
                return <Mirror3D {...childProps}/>;
            } else if (child.optics === 'lens') {
                return <Lens3D {...childProps}/>;
            } else {
                return false;
            }
        })) || [];

    return <Object3D>{children}</Object3D>;
};

export default Group3D
