/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */
'use strict';

import React from "react";
import {Object3D} from "react-three";
import TraceGroup3D from "./trace-group-3d.jsx";

export default class Results3D extends React.Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.results !== this.props.results;
    }

    render() {
        let results = this.props.results || {};
        let groups = Object.keys(results).map(id => <TraceGroup3D key={id} group={results[id]}/>);

        return <Object3D>{groups}</Object3D>;
    }
}
