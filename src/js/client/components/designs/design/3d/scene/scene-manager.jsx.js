/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */

'use strict';

import React from "react";
import {Renderer, Scene, PointLight, AmbientLight} from "react-three";
import SimCamera from "./sim-camera.jsx.js";
import Components3D from "./elements/components-3d.jsx";
import Results3D from "./results/results-3d.jsx";

export default ({size, perspective, rootComponent, results}) => (
    <Renderer {...size} background={0x404044}>
        <Scene camera={CAMERA_NAME} {...size}>
            <SimCamera name={CAMERA_NAME} perspective={perspective}/>
            <AmbientLight color={0x505050}/>
            <PointLight position={perspective.pos} distance={1e6} intensity={1}/>
            <Components3D rootComponent={rootComponent}/>
            <Results3D results={results}/>
        </Scene>
    </Renderer>
)

const CAMERA_NAME = 'main-camera';
