/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import THREE from "three";
import shapeOps from "../../../../../../../logic/optics/shape-ops";

export default (hap, shape, dx, hr) => {

    let p = [];
    if (hap && shape) {
        let n = 50;
        let zStep = (hap - hr) / n;

        p.length = n + 1;
        for (let i = 0; i <= n; i += 1) {
            let y = hr + i * zStep;
            let x = shapeOps.drawPoint(shape, y, dx);

            p[i] = new THREE.Vector2(y, x);
        }
    }
    return p;
}
