/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* globals window */
'use strict';

import React from "react";
import THREE from "three";
import {Mesh} from "react-three";
import CachedGeometryComponent from "../cached-geometry-mixin";
import Element3D from "./element-3d.jsx.js";
import shapePoints from "./shape-points";
import {mirror as mirrorMaterial} from "./../materials";

const points = mirror => {
    var hr = mirror.hr,
        ps = shapePoints(mirror.hap, mirror.shape, 0, hr);

    for (var i = ps.length - 1; i >= 0; i -= 1) {
        var p = ps[i];
        ps.push(new THREE.Vector2(p.x, p.y + 1));
    }

    if (hr > 0) {
        ps.push(ps[0]);
    }

    return ps;
};


export default class Mirror3D extends CachedGeometryComponent {
    createGeometry() {
        return new THREE.LatheGeometry(points(this.props.component), 100);
    }

    render() {
        return (
            <Element3D component={this.props.component} rotateShape={true}>
                <Mesh geometry={this.geometry()} material={mirrorMaterial}/>
            </Element3D>
        );
    }
}
