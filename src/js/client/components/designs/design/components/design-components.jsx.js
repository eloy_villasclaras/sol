/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import systemManager from "../../../../../logic/optics/system-manager";
import Splitter from "../../../widgets/splitter.jsx";
import ComponentList from "./forms/component-list.jsx";
import VarList from "./forms/var-list.jsx";
import {update} from "../../../../stores/design-actions";
import {select} from "../../../../stores/select-entity-actions";
import {editState} from "../../../../stores/process-state";

const DesignComponents = ({selected, rootComponent, info, componentVars, edit, onUpdate, onSelect}) => {
    if (rootComponent) {
        let componentList = <ComponentList component={rootComponent} info={info.components}
                                           selectedId={selected && selected.entityId}
                                           edit={edit} onUpdate={onUpdate} onSelect={onSelect}/>;

        if (selected) {
            let selectedComponent = systemManager.find(rootComponent, selected.entityId);

            return (
                <Splitter vertical={true} defaultPosition={0.25}>
                    <div className="components-forms-container">{componentList}</div>
                    <div className="components-forms-container">
                        <VarList component={selectedComponent} vars={componentVars}
                                 info={info.components} selectedVarId={selected.varId}
                                 edit={edit} onUpdate={onUpdate} onSelect={onSelect}/>
                    </div>
                </Splitter>
            );
        } else {
            return (
                <div className="components-forms-container">{componentList}</div>
            );
        }
    } else {
        return <div></div>;
    }
};

const mapStateToProps = (state) => {
    return {
        edit: editState(state),
        rootComponent: state.design.processed.components,
        componentVars: state.design.processed.componentVars,
        info: state.design.processed.info,
        selected: state.selection.type === 'component' && state.selection
    };
};

const mapDispatchToProps = dispatch => ({
    onUpdate: changes => dispatch(update(changes)),
    onSelect: (type, eid, vid) => dispatch(select(type, eid, vid))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignComponents);
