/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import classNames from "classnames";
import SelectDlgField from "../../widgets/fields/select-dlg-field.jsx.js";
import shapeOptions from "./shape-options";


export default class ShapeTypeSelectField extends React.Component {
    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(value) {
        let {onUpdate, component, face} = this.props;

        onUpdate({
            type: 'set-component-shape',
            itemId: component.fullId,
            shape: value,
            face: face || null
        });
    }

    render() {
        let {component, attr, selectedVarId, label, edit, onSelect} = this.props;
        let value = component[attr].type;

        return (
            <div className={classNames('var-field', {'var-field-editable': edit})}>
                <SelectDlgField type="component" id={component.fullId} value={value}
                                isSelected={selectedVarId === attr} attr={attr} label={label}
                                options={shapeOptions[component.optics]} onSubmit={this.handleSubmit}
                                onSelect={onSelect} edit={edit} title="Select shape type"/>
            </div>
        );
    }
}
