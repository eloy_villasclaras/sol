/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import VarEqField from "../../widgets/fields/eq/var-eq-field.jsx";

export default class ComponentVarEqField extends React.Component {
    constructor() {
        super();

        this.onSelect = this.onSelect.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onSelect(varId) {
        this.props.onSelect('component', this.props.component.fullId, varId);
    }

    onChange(value) {
        this.props.onUpdate({
            type: 'set-component-var-value',
            itemId: this.props.component.fullId,
            key: 'eq',
            varId: this.props.varId,
            value: value
        });
    }

    render() {
        let {component, varId, info, vars, selectedVarId, edit} = this.props;
        let name = info[component.fullId].vars[varId];
        let isSelected = selectedVarId === varId;

        return <VarEqField entity={component} varId={varId} edit={edit}
                           vars={vars} info={info} isSelected={isSelected}
                           onSelect={this.onSelect} onChange={this.onChange}
                           label={name[0]} units={name[1]}/>;
    }
}
