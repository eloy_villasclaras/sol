/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import IconButton from "material-ui/IconButton";
import TitleWidget from "../../widgets/title-widget.jsx.js";
import ComponentTextField from "../fields/component-text-field.jsx.js";
import ComponentVarEqField from "../fields/component-var-eq-field.jsx";
import Subheader from "../../widgets/subheader.jsx.js";
import ElementVars from "./element-vars.jsx";


export default (props) => {
    let {component, onSelect} = props;

    let isElement = component.type === 'element';
    let iconName = isElement ? (component.optics === 'lens' ? 'lens' : 'panorama_fish_eye') : 'group_work';
    let titleField = <ComponentTextField {...props} attr="title" defaultValue={component.fullId}/>;

    let elementVars = isElement && <ElementVars {...props}/>;

    let close = <IconButton iconClassName="material-icons" key="settings"
                            onClick={() => onSelect(null)}>close</IconButton>;

    return (
        <div className="component-vars">
            <TitleWidget title={titleField} icon={iconName} menu={close}/>
            <Subheader text="Position"/>
            <ComponentVarEqField {...props} varId="x"/>
            <ComponentVarEqField {...props} varId="y"/>
            <ComponentVarEqField {...props} varId="rot"/>
            {elementVars}
        </div>
    );
}
