/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Subheader from "../../widgets/subheader.jsx.js";
import ComponentVarEqField from "../fields/component-var-eq-field.jsx";
import ShapeVars from "./shape-vars.jsx.js";

export default (props) => (
    <div>
        <ComponentVarEqField {...props} varId="n"/>
        <ComponentVarEqField {...props} varId="v"/>
        <ComponentVarEqField {...props} varId="th"/>
        <Subheader text="Front face"/>
        <ShapeVars {...props} face="front"/>
        <Subheader text="Back face"/>
        <ShapeVars {...props} face="back"/>
    </div>
)
