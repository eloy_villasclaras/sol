/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FontIcon from "material-ui/FontIcon";
import IconButton from "material-ui/IconButton";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import Divider from "material-ui/Divider";
import TitleWidget from "../../widgets/title-widget.jsx";
import ErrorIcon from "../../widgets/error-icon.jsx";
import {Tree, TreeLeaf} from "../../widgets/tree";
import {menuIconSettings, treeIconMenuSettings, treeIconButtonSettings, treeTitleSettings} from "../../../../../theme";


class AbstractTreeComponent extends React.Component {
    constructor() {
        super();

        this.handleSettingsMenuChange = this.handleSettingsMenuChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect() {
        this.props.onSelect('component', this.props.component.fullId, null);
    }

    handleSettingsMenuChange(e, value) {
        switch (value) {
            case 'delete':
                if (this.props.selectedId === this.props.component.fullId) {
                    this.props.onSelect(null, null, null);
                }

                this.props.onUpdate({
                    type: 'delete-component',
                    itemId: this.props.component.fullId
                });
                break;
            default:
                if (this.handleMenuChange) {
                    this.handleMenuChange(e, value);
                }
                break;
        }
    }

    title() {
        let {component, isRoot, info, id, selectedId} = this.props;

        let title = (isRoot && 'Components') || info[component.fullId].title || id;
        let iconName = component.type === 'element' && info[component.fullId].icon;
        let selectable = !isRoot;
        let selected = selectable && selectedId === component.fullId;
        let onClick = selectable && this.handleSelect;
        let style = treeTitleSettings[selected ? 'selected' : 'unselected'];
        let menu = this.settingsMenu();

        if (component.error) {
            title = <ErrorIcon text={title} type={component.error}/>;
        }

        return <TitleWidget title={title} icon={iconName} onClick={onClick}
                            style={style.text} iconStyle={style.icon} menu={menu}/>;
    }

    settingsMenu() {
        let items = (this.menuItems && this.menuItems()) || [];

        if (!this.props.isRoot) {
            if (items.length > 0) {
                items.push(<Divider key="b_delete"/>);
            }

            items.push(<MenuItem key="delete" primaryText="Delete" value="delete"
                                 leftIcon={<FontIcon className="material-icons">delete</FontIcon>}/>);
        }

        if (items.length > 0) {
            let icon = <IconButton iconClassName="material-icons" {...treeIconButtonSettings}
                                   key="settings">more_vert</IconButton>;

            return <IconMenu iconButtonElement={icon} {...treeIconMenuSettings}
                             onChange={this.handleSettingsMenuChange}
                             anchorOrigin={{horizontal:"right",vertical:"top"}}
                             targetOrigin={{horizontal:"left",vertical:"top"}}>{items}</IconMenu>;
        } else {
            return false;
        }
    }
}

class OpticElement extends AbstractTreeComponent {
    render() {
        return this.title();
    }
}

class GroupComponent extends AbstractTreeComponent {
    menuItems() {
        return [
            <MenuItem key="lens" primaryText="Add lens" value="lens"
                      leftIcon={<FontIcon {...menuIconSettings} className="material-icons">lens</FontIcon>}/>,
            <MenuItem key="mirror" primaryText="Add mirror" value="mirror"
                      leftIcon={<FontIcon {...menuIconSettings} className="material-icons">panorama_fish_eye</FontIcon>}/>,
            <MenuItem key="group" primaryText="Add group" value="group"
                      leftIcon={<FontIcon {...menuIconSettings} className="material-icons">group_work</FontIcon>}/>
        ];
    }

    handleMenuChange(event, value) {
        let action = {
            type: 'create-component',
            parent: this.props.component.fullId || null
        };

        if (value === 'group') {
            action.itemType = 'group';
        } else {
            action.itemType = 'element';
            action.optics = value;
        }

        this.props.onUpdate(action);
    }

    render() {
        let {component, info, selectedId, onUpdate, onSelect} = this.props;

        let children = Object.keys(component.children).sort((a, b) => {
            var typeA = component.children[a].type,
                typeB = component.children[b].type;

            if (typeA === 'element' && typeB === 'group') {
                return -1;
            } else if (typeA === 'group' && typeB === 'element') {
                return 1;
            } else {
                return 0;
            }
        }).map(childId => {
            let child = component.children[childId];
            let TypedComponent = child.type === 'element' ? OpticElement : GroupComponent;
            return (
                <TreeLeaf key={childId}>
                    <TypedComponent parentId={component.fullId} component={child}
                                    info={info} selectedId={selectedId}
                                    onUpdate={onUpdate} onSelect={onSelect}/>
                </TreeLeaf>
            );
        });

        return (
            <Tree header={this.title()} className="component-list" selected={selectedId === component.fullId}>
                {children}
            </Tree>
        );
    }
}

export default props => <GroupComponent {...props} isRoot={true}/>
