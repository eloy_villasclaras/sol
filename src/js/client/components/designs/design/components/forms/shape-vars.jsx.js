/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import ComponentVarEqField from "../fields/component-var-eq-field.jsx.js";
import ShapeSelectField from "../fields/shape-type-select-field.jsx.js";

const prefixes = {
    'shape': 'm_',
    'frontShape': 'f_',
    'backShape': 'b_'
};

const labels = {
    'shape': 'Shape',
    'frontShape': 'Front',
    'backShape': 'Back'
};

const shapeKey = (component, face) => {
    if (component.optics === 'mirror') {
        return 'shape';
    } else if (face === 'front') {
        return 'frontShape';
    } else {
        return 'backShape';
    }
};

export default (props) => {
    let {component, selectedVarId, face = null, vars, edit, onSelect, onUpdate} = props;
    let attr = shapeKey(component, face);
    let label = labels[attr];

    let prefix = prefixes[attr];
    let varElements = prefix && Object.keys(vars[component.fullId])
            .filter(function (key) {
                return key.indexOf(prefix) === 0;
            })
            .map(function (key) {
                return (
                    <ComponentVarEqField key={key} {...props} varId={key}/>
                );
            });

    return (
        <div>
            <ShapeSelectField component={component} selectedVarId={selectedVarId}
                              attr={attr} label={label} face={face} edit={edit}
                              onSelect={onSelect} onUpdate={onUpdate}/>
            {varElements}
        </div>
    );
}
