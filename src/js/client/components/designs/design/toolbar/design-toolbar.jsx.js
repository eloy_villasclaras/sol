/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import extend from "extend";
import Toggle from "material-ui/Toggle";
import {Toolbar, ToolbarGroup, ToolbarSeparator} from "material-ui/Toolbar";
import FlatButton from "material-ui/FlatButton";
import {undo, redo, canRedo, canUndo} from "../../../../stores/design-actions";
import {postDoc} from "../../../../stores/design-list-actions";

import {editState, editableState} from "../../../../stores/process-state";

import FileMenu from "./file-menu/file-menu.jsx";
import PageIconButton from "./page-icon-button.jsx";
import ToolbarIconButton from "./toolbar-icon-button.jsx";

const routes = [
    {path: '', label: 'Information', icon: 'info'},
    {path: '/components', label: 'Components', icon: 'create'},
    {path: '/analysis', label: 'Analysis', icon: 'assessment'}
];


const DesignToolbar = ({
    editable, edit, canUndo, canRedo, saved, creating,
    designPath, path, currentSegment, query, is3D,
    onUndo, onRedo, onPush, onPostDoc, loading, document
}) => {
    let navIcons = routes.map(route => (
            <PageIconButton key={route.path} currentSegment={currentSegment}
                            segment={route.path} tooltip={route.label} icon={route.icon}
                            onClick={() => {
                                onPush({
                                pathname: designPath + route.path,
                                query: query
                            })}}/>
        )
    );

    let editTools = edit && [
            <ToolbarSeparator key="0"/>,
            <ToolbarIconButton key="1" disabled={!canUndo} onClick={onUndo}>
                undo
            </ToolbarIconButton>,
            <ToolbarIconButton key="2" disabled={!canRedo} onClick={onRedo}>
                redo
            </ToolbarIconButton>
        ];

    let viewGroup = (
        <ToolbarGroup float="right" key={5}>
            <FlatButton style={{minWidth: 50}} labelStyle={{padding: 0}} secondary={true}
                        label={is3D ? '3D' : '2D'}
                        onClick={() => {
                        onPush({pathname: path, query: extend({}, query, {view: is3D ? '2d' : '3d'})});
                        }}/>
        </ToolbarGroup>
    );

    let onToggle = (evt, value) => onPush({pathname: path, query: extend({}, query, {edit: value ? '1' : '0'})});
    let editToggle = editable &&
        [
            <ToolbarSeparator key={0} style={{marginRight:24}}/>,
            <Toggle key={1} defaultToggled={edit} label="Edit" onToggle={onToggle}
                    style={{width: 100, paddingTop: 17}}/>
        ];

    return (
        <Toolbar style={{padding: 0}}>
            <ToolbarGroup key={0}>
                {navIcons}
                {editToggle}
                {editTools}
            </ToolbarGroup>
            <ToolbarGroup key={1} float="right">
                {viewGroup}
                <FileMenu document={document} loading={loading} creating={creating}
                          edit={edit} editable={editable} query={query}
                          onPostDoc={onPostDoc} saved={saved}/>
            </ToolbarGroup>
        </Toolbar>
    );
};


const mapStateToProps = (state, ownProperties) => {
    return extend({
        editable: editableState(state),
        query: state.routing.locationBeforeTransitions.query,
        is3D: state.routing.locationBeforeTransitions.query.view === '3d',
        creating: state.list.creating,
        edit: editState(state),
        loading: state.design.loading,
        document: state.design.document,
        canUndo: canUndo(state.design),
        canRedo: canRedo(state.design),
        saved: state.design.version === state.design.saved
    }, ownProperties);
};

const mapDispatchToProps = dispatch => ({
    onPush: path => dispatch(push(path)),
    onPostDoc: (data, query) => dispatch(postDoc(data, query)),
    onUndo: value => dispatch(undo()),
    onRedo: value => dispatch(redo())
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignToolbar);
