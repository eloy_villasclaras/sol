/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
'use strict';

import React from "react";
import extend from "extend";
import IconButton from "material-ui/IconButton";
import theme from "../../../../theme";

const defaultButtonStyle = {
    height: 56,
    width: 56,
    paddingRight: 12,
    paddingLeft: 12,
    marginLeft: 12,
    marginRight: -12
};

export default class ToolbarIconButton extends React.Component {
    render() {
        let {active, disabled, onClick, tooltip, children, style} = this.props;
        var iconStyle = {};

        if (active) {
            iconStyle.color = theme.inkBar.backgroundColor;
            iconStyle.iconHoverColor = theme.inkBar.backgroundColor;
        } else if (disabled) {
            iconStyle.color = theme.toolbar.menuHoverColor;
            iconStyle.iconHoverColor = theme.toolbar.menuHoverColor;
        } else {
            iconStyle.color = theme.toolbar.iconColor;
            iconStyle.iconHoverColor = 'black';
        }

        let buttonStyle = style ? extend({}, defaultButtonStyle, style) : defaultButtonStyle;

        return (
            <IconButton style={buttonStyle} iconStyle={iconStyle} disabled={disabled}
                        iconClassName="material-icons" tooltipPosition="bottom-center"
                        tooltip={tooltip} onClick={onClick}>
                {children}
            </IconButton>
        );
    }
}
