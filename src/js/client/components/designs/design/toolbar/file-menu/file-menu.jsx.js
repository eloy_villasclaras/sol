/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import IconButton from "material-ui/IconButton";

import CloneDialog from "./clone-dlg.jsx";


const iconButtonStyle = {
    marginRight: '12px',
    padding: '16px'
};

const iconStyle = {
    color: "rgba(0, 0, 0, 0.4)"
};

export default class FileMenu extends React.Component {
    constructor() {
        super();
        this.state = {selected: null};
        this.handleSettingsMenuChange = this.handleSettingsMenuChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleSettingsMenuChange(e, value) {
        this.setState({selected: value});
    }

    handleClose() {
        this.setState({selected: null});
    }

    renderDialog() {
        let {document, loading, onPostDoc, query, creating}  = this.props;

        switch (this.state.selected) {
            case 'clone':
                return !loading && <CloneDialog open={true} onClose={this.handleClose} query={query}
                                                document={document} onPostDoc={onPostDoc}
                                                creating={creating}/>;
            default:
                return false;
        }
    }

    render() {
        let {edit, editable, saved}  = this.props;
        let selected = this.state.selected;

        if (edit) {
            let icon = saved ? 'done' : (editable ? 'save' : 'warning');
            let button = (
                <IconButton disabled={!editable} style={iconButtonStyle}
                            iconStyle={iconStyle} iconClassName="material-icons">
                    {icon}
                </IconButton>
            );

            return (
                <div>
                    <IconMenu iconButtonElement={button}
                              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                              targetOrigin={{horizontal: 'right', vertical: 'top'}}
                              onChange={this.handleSettingsMenuChange}>
                        <MenuItem primaryText="Clone design" value="clone"/>
                    </IconMenu>
                    {this.renderDialog()}
                </div>
            );
        } else {
            return false;
        }
    }
}

