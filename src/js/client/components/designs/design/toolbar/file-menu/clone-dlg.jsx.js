/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FlatButton from "material-ui/FlatButton";
import RefreshIndicator from "material-ui/RefreshIndicator";
import Dialog from "material-ui/Dialog";
import TextField from "material-ui/TextField";


export default class CloneDialog extends React.Component {
    constructor(props) {
        super();
        this.state = {value: `${props.document.title} - copy`};

        this.handleTitleInputChange = this.handleTitleInputChange.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    handleTitleInputChange(e) {
        this.setState({value: e.target.value});
    }

    handleCreate() {
        let {onPostDoc, document, query, onClose} = this.props;

        onClose();

        onPostDoc({
            title: this.state.value,
            description: document.description,
            content: document.content
        }, query);
    }

    render() {
        let {open, onClose, creating} = this.props;
        let value = this.state.value;

        let titleError = !value && 'Title cannot be empty';
        let workingIndicator = creating && <RefreshIndicator status="loading" left={100} top={0}/>;
        let createDisabled = creating || titleError;

        let customActions = [
            <FlatButton label="Clone" primary={true} disabled={createDisabled} onTouchTap={this.handleCreate}/>,
            <FlatButton label="Cancel" secondary={true} onTouchTap={onClose}/>
        ];

        return (
            <Dialog title="Copy design"
                    autoDetectWindowHeight={true}
                    autoScrollBodyContent={true}
                    actions={customActions}
                    modal={true}
                    open={open}>
                <TextField floatingLabelText="Title"
                           value={value}
                           errorText={titleError}
                           autoFocus={true}
                           onChange={this.handleTitleInputChange}
                           disabled={creating}/>
                {workingIndicator}
            </Dialog>
        );
    }
}
