/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Paper from "material-ui/Paper";
import FontIcon from "material-ui/FontIcon";
import domOps from "../../../../../../utils/dom-ops";

const entityStyle = {
    color: '#888',
    fontSize: '12px',
    marginRight: '5px'
};

const varOptions = (vars, text) => {
    let searchText = text.toLowerCase();
    let keys = Object.keys(vars).map(key => {
        let varText = vars[key].name;

        return {
            key: key,
            priority: text.length === 0 ? 1 :
                (key.toLowerCase().indexOf(searchText) >= 0 ? 1000 - (key.length - searchText.length) :
                    (varText.toLowerCase().indexOf(searchText) >= 0 ? 100 - varText.length - searchText.length : 0)),
            selectable: !vars[key].dep
        };
    }).filter(data => data.priority > 0);

    let selected = keys.reduce(
        (best, keyData, i) => keyData.selectable && keyData.priority > best.v ? {v: keyData.priority, i: i} : best,
        {v: 0, i: -1}
    ).i;

    return {
        keys: keys,
        selected: selected
    };
};

export default class VarPicker extends React.Component {
    constructor(props) {
        super();

        let {vars, text} = props;

        this.state = varOptions(vars, text);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.text !== this.props.text) {
            this.setState(varOptions(nextProps.vars, nextProps.text));
        }
    }

    _scroll() {
        let {scroller, active} = this.refs;
        if (scroller && active) {
            let height = scroller.clientHeight;
            let scroll = scroller.scrollTop;
            let activeHeight = active.clientHeight;
            let activeTop = active.offsetTop;

            if (activeTop < scroll) {
                scroller.scrollTop = activeTop;
            } else if (activeTop + activeHeight > height + scroll) {
                scroller.scrollTop = activeTop + activeHeight - height;
            }
        }
    }

    componentDidMount() {
        let node = this.refs.e;
        let parent = node.parentNode;
        let parentOffset = domOps.offsetScroll(parent);
        let viewport = domOps.viewport();
        let parentWidth = parent.clientWidth;
        let nodeHeight = node.clientHeight;
        let top = Math.min(viewport.height - nodeHeight, parentOffset.offsetTop);
        let left = parentWidth + 50;
        let style = `top:${top}px;left:${left}px;`;

        node.setAttribute('style', style);
        this._scroll();
    }

    componentDidUpdate() {
        this._scroll();
    }

    move(d) {
        let index = this.state.selected;

        if (index >= 0) {
            let keys = this.state.keys;
            let n = keys.length;

            while (true) {
                index = (index + d + n) % n;
                if (index === this.state.selected) {
                    break;
                } else if (keys[index].selectable) {
                    this.setState({
                        selected: index
                    });
                    break;
                }
            }
        }
    }

    selected() {
        return this.state.selected >= 0 ? this.state.keys[this.state.selected].key : false;
    }

    render() {
        let {selected, keys} = this.state;
        let vars = this.props.vars;
        let elements = keys.map((keyData, i) => {
            let key = keyData.key;
            let varData = vars[key];
            let selectable = keyData.selectable;
            let isSelected = i === selected;
            let style = {
                padding: '0 15px 0 10px',
                whiteSpace: 'nowrap',
                color: selectable ? 'black' : 'lightgray',
                background: selectable && isSelected ? 'lightgray' : 'white'
            };
            return (
                <div key={key} style={style} ref={isSelected ? 'active' : false}>
                    <FontIcon className="material-icons" style={entityStyle}>{varData.icon}</FontIcon>
                    <span style={entityStyle}>{varData.entity}</span>
                    {varData.name}
                </div>
            );
        });
        return (
            <div ref="e" className="eq-input-var-tree">
                <Paper zDepth={1} rounded={true}>
                    <div className="eq-input-var-tree-inner" ref="scroller">
                        {elements}
                    </div>
                </Paper>
            </div>
        );
    }
}
