/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

const mmUnits = ['mm', 'm', 'km'];

function format2decimals(val) {
    let v = (.01 * (Math.round(100 * val))).toString();
    let p = v.indexOf('.');

    return (p >= 0 && p < v.length - 3) ? v.substr(0, p + 3) : v;
}

function formatMM(value) {
    let absValue = Math.abs(value);
    if (absValue > 0) {
        let log10 = Math.log10(absValue);
        let level = Math.min(mmUnits.length - 1, Math.floor(log10 / 3));
        return [format2decimals(value * Math.pow(0.1, 3 * level)), mmUnits[level]];
    } else {
        return [format2decimals(value), 'mm'];
    }
}

function formatDegrees(value) {
    let absValue = Math.abs(value);
    if (absValue >= 1 || absValue === 0) {
        return [format2decimals(value), '°'];
    }

    if (absValue *60 >= 1) {
        return [format2decimals(value * 60), "'"];
    }

    return [format2decimals(value * 3600), "\""];
}

export default function (value, unit) {
    switch (unit) {
        case 'mm':
            return formatMM(value);
        case '°':
            return formatDegrees(value);
        default:
            return [format2decimals(value), unit || ''];
    }
}
