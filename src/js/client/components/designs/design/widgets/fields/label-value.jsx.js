/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";

export default ({selected, label, value, units, onClick}) => {
    let labelElement = label && <div className="field-label">{label}</div>;
    let className = 'field' + (selected ? ' selected' : '');
    let unitsElement = typeof units === 'string' && <div className="field-units">{units}</div>;


    return (
        <div className={className} onClick={onClick}>
            {labelElement}
            <div className="field-value">
                {value}
            </div>
            {unitsElement}
        </div>
    );
}
