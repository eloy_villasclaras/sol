/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";
import {RadioButton, RadioButtonGroup} from "material-ui/RadioButton";

export default class OptionsDialog extends React.Component {
    constructor(props) {
        super();
        this.state = {selected: props.value || null};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (!this.props.open && nextProps.open && nextProps.value !== this.state.selected) {
            this.setState({selected: nextProps.value});
        }
    }

    handleSubmit() {
        this.props.onSubmit(this.state.selected);
    }

    handleChange(e) {
        this.setState({selected: e.target.value});
    }

    render() {
        let {value, options, onCancel, title, open} = this.props;
        let {selected} = this.state;

        let customActions = [
                <FlatButton
                    label="Cancel"
                    secondary={true}
                    onTouchTap={onCancel}/>,
                <FlatButton
                    label="Ok"
                    primary={true}
                    onTouchTap={this.handleSubmit}
                    disabled={value === selected}/>
            ],

            radioButtons = Object.keys(options).map(function (option) {
                return <RadioButton
                    key={option}
                    value={option}
                    label={options[option]}/>;
            });

        return (
            <Dialog title={title}
                    actions={customActions}
                    modal={true}
                    open={open}>
                <RadioButtonGroup ref="select" name="select"
                                  valueSelected={selected}
                                  onChange={this.handleChange}>
                    {radioButtons}
                </RadioButtonGroup>
            </Dialog>
        );
    }
}
