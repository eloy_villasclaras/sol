/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import TextField from "material-ui/TextField";
import AbstractOnfocusField from "./abstract-onfocus-field.jsx";

export default class OnFocusTextField extends AbstractOnfocusField {
    constructor() {
        super();
        this.fieldType = 'text';

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    handleChange(e) {
        this.props.onChange(e.target.value);
    }


    handleKeyDown(e) {
        if (e.key === 'Enter' || e.key === 'Escape') {
            this.toggleEdit();
        }
    }


    renderField() {
        let {value, fieldStyle, underlineStyle, attr} = this.props;

        return <TextField value={value}
                          name={attr}
                          onChange={this.handleChange}
                          onBlur={this.toggleEdit}
                          onKeyDown={this.handleKeyDown}
                          style={fieldStyle}
                          underlineStyle={underlineStyle}
                          autoFocus/>;
    }
}
