/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var React = require('react'),

        mui = require('material-ui'),
        Toggle = mui.Toggle,

        LabelValue = require('./label-value.jsx'),

        designSelectionActions = require('../../../../../actions/design-selection-actions'),
        documentActions = require('../../../../../actions/document-actions');

    return React.createClass({
        propTypes: {
            id: React.PropTypes.string,
            entity: React.PropTypes.object,
            attr: React.PropTypes.string,
            label: React.PropTypes.string,
            actionType: React.PropTypes.string,
            isSelected: React.PropTypes.bool
        },

        handleChange: function (e, toggled) {
            documentActions.update({
                type: this.props.actionType,
                itemId: this.props.id,
                key: this.props.attr,
                value: toggled
            });
        },

        handleKeyDown: function (e) {
            if (e.key === 'Enter' || e.key === 'Escape') {
                //this.toggleEdit();
            }
        },

        handleClick: function () {
            designSelectionActions.select(this.props.type, this.props.id, this.props.attr);
        },

        render: function () {
            var isSelected = this.props.isSelected,
                className = "var-field" + (isSelected ? ' selected' : ''),
                value = this.props.entity[this.props.attr] || false,
                field = isSelected && <Toggle toggled={value}
                                              onToggle={this.handleChange}
                                              onKeyDown={this.handleKeyDown}
                                              autoFocus/>;

            return (
                <div className={className}>
                    <LabelValue value={value ? 'Yes' : 'No'} label={this.props.label}
                                selected={isSelected} onClick={this.handleClick}/>
                    {field}
                </div>
            );
        }
    });

}();
