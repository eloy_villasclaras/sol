/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import classNames from "classnames";
import TextField from "material-ui/TextField";
import ErrorIcon from "../../widgets/error-icon.jsx";
import LabelValue from "./label-value.jsx.js";

export default class VarField extends React.Component {
    constructor() {
        super();

        this.handleClick = this.handleClick.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClick() {
        this.props.onSelect(this.props.varId);
    }

    handleKeyDown(e) {
        if (e.key === 'Enter' || e.key === 'Escape') {
            this.handleClose();
        } else if (e.key === 'Tab') {
            e.preventDefault();
            e.stopPropagation();

            let keys = Object.keys(this.props.vars);
            let index = keys.indexOf(this.props.varId);
            let nextIndex = (index + (e.shiftKey ? (keys.length - 1) : 1)) % keys.length;
            let varId = keys[nextIndex];

            this.props.onSelect(varId);
        }
    }

    handleClose() {
        this.props.onSelect(null);
    }

    handleChange(e) {
        this.props.onChange(e.target.value);
    }

    formatNumber(val) {
        let v = (.01 * (Math.round(100 * val))).toString();
        let p = v.indexOf('.');

        return (p >= 0 && p < v.length - 3) ? v.substr(0, p + 3) : v;
    }

    render() {
        let {varId, label, units, edit, isSelected} = this.props;
        let selected = edit && isSelected;
        let className = classNames("var-field", {'selected': selected, 'var-field-editable': edit});
        let computedVar = this.props.vars[varId];
        let val = computedVar && typeof computedVar.val === 'number' ?
            this.formatNumber(computedVar.val) : <ErrorIcon/>;
        let field = selected && <TextField key={varId}
                                           name={varId}
                                           style={{width: '100%'}}
                                           value={computedVar.eq}
                                           onBlur={this.handleClose}
                                           onChange={this.handleChange}
                                           onKeyDown={this.handleKeyDown}
                                           autoFocus/>;

        return (
            <div className={className}>
                <LabelValue value={val} label={label} units={units} selected={isSelected}
                            onClick={this.handleClick}/>
                {field}
            </div>
        );
    }
}
