/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


'use strict';

import React from "react";
import LabelValue from "./label-value.jsx";
import CidTreeDialog from "./cid-tree-dlg.jsx";
import systemManager from "../../../../../../logic/optics/system-manager";

export default class EntityCidTreeDlfTield extends React.Component {
    constructor() {
        super();

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleClick() {
        let {type, id, attr} = this.props;
        this.props.onSelect(type, id, attr);
    }

    handleClose() {
        let {type, id} = this.props;
        this.props.onSelect(type, id);
    }

    handleSelect(value) {
        this.handleClose();
        this.props.onChange(value);
    }

    render() {
        let {
            attr, entity, componentRoot, acceptType, isSelected,
            info, rootLabel,
            label = attr, title = `Select: ${label}`
        } = this.props;

        let value = entity[attr] || null;
        let selectedComponent = value && systemManager.find(componentRoot, value);
        let valueText = (selectedComponent && (
                info[selectedComponent.fullId].title || selectedComponent.title || value
            )) || '';

        return (
            <div>
                <LabelValue value={valueText} label={label} selected={isSelected} onClick={this.handleClick}/>
                <CidTreeDialog ref="selectDlg" value={value} root={componentRoot} title={title}
                               rootLabel={rootLabel} acceptType={acceptType} open={isSelected} info={info}
                               onSelect={this.handleSelect} onCancel={this.handleClose}/>
            </div>
        );
    }
}
