/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";

export default class AbstractOnFocusField extends React.Component {
    constructor() {
        super();
        this.state = {editing: false};

        this.toggleEdit = this.toggleEdit.bind(this);
        this.toggleEditOff = this.toggleEditOff.bind(this);
    }

    toggleEdit() {
        if (this.props.edit) {
            this.setState({editing: !this.state.editing});
        }
    }

    toggleEditOff() {
        this.setState({editing: false});
    }


    renderWidget() {
        let {edit, label, value} = this.props;
        let editing = edit && (this.alwaysEditing || this.state.editing);
        let labelElement = label && <div className="field-label">{label}</div>;
        let className = 'field field-' + this.fieldType;

        if (editing) {
            return (
                <div className={className + ' field-editing'}>
                    {labelElement}
                    <div className="field-widget">
                        {this.renderField()}
                    </div>
                </div>
            );
        } else {
            var style = (edit && {cursor: 'pointer'}) || {};

            return (
                <div className={className} style={style} onClick={this.toggleEdit}>
                    {labelElement}
                    <div className="field-value">
                        {this.valueTxt ? this.valueTxt() : value}
                    </div>
                </div>
            );
        }
    }

    render() {
        return this.renderWidget();
    }
}