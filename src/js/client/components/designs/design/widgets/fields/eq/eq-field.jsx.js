/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import VarPicker from "./var-picker.jsx";

const parseValue = (value, vars) => {
    let matches = value.match(/\[[^\]]+]|[_a-zA-Z]+[\._a-zA-Z0-9]*|((-?[0-9]+\.?[0-9]*\s*[a-z']*\s*)+)|\+|\-|\*|\(|\)|\/|\s|\^/g);

    return matches ? matches.reduce((acc, token) => {
        let type = tokenType(token);

        if (type === 'mng') {
            let key = token.substr(1, token.length - 2);
            return acc + (key in vars ? '[' + vars[key].name + '|' + key + ']' : key);
        } else {
            return acc + token;
        }
    }, '') : '';
};

const tokenType = token => {
    if (token === ' ') {
        return 'sp';
    } else if (['+', '-', '*', '/'].indexOf(token) >= 0) {
        return 'op';
    } else if (token.match(/^[0-9]/)) {
        return 'nbr';
    } else if (token.charAt(0) === '[') {
        return 'mng';
    } else {
        return 'txt';
    }
};

export default class EqField extends React.Component {
    constructor(props) {
        super();
        this.state = {
            text: parseValue(props.value, props.vars),
            value: props.value,
            v: 0,
            caret: false
        };

        this.handleKey = this.handleKey.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }


    componentDidMount() {
        this.refs.e.focus();
    }


    componentWillUpdate(nextProps, nextState) {
        if (this.props.value !== nextProps.value && nextProps.value !== nextState.value) {
            this.setState({
                value: nextProps.value,
                text: parseValue(nextProps.value, nextProps.vars)
            });
        }
    }

    t2b(text) {
        return text.replace(/\s/g, '&nbsp;');
    }

    b2t(t) {
        return t.replace(/<[^>]+>/ig, '').replace(/&nbsp;/g, ' ').replace(/\r?\n|\r/g, '');
    }

    outputText(text) {
        let regex = /\[[^\|^\]]+\|([^\]]+)]/;
        var match;

        while ((match = regex.exec(text)) !== null) {
            text = text.substr(0, match.index) + '[' + match[1] + ']' + text.substr(match.index + match[0].length);
        }

        return text;
    }


    groups() {
        let text = this.state.text;
        let matches = text.match(/(\[[^\]]+])|((-?[0-9]+\.?[0-9]*\s*[a-z']*\s*)+)|([_a-zA-Z]+[\._a-zA-Z0-9]*)|\+|\-|\*|\(|\)|\/|\s|\^/g);

        return matches ? matches.map(token => {
            var data = {
                type: tokenType(token)
            };

            if (data.type === 'mng') {
                let mngMatch = token.match(/^\[([^\|]+)\|([^\]]+)]$/);
                data.text = mngMatch[1];
                data.managed = mngMatch[2];
            } else {
                data.text = token;
                data.managed = false;
            }
            return data;
        }) : [];
    }

    currentCaret() {
        let sel = window.getSelection();
        let range = sel && sel.getRangeAt(0);
        let node = range && range.startContainer.parentNode;
        let blockAttrs = node && node.attributes['data-index'];

        return {
            block: blockAttrs ? parseInt(blockAttrs.nodeValue) : -1,
            pos: range.startOffset,
            type: blockAttrs ? node.attributes['data-type'].nodeValue : ''
        };
    }

    componentDidUpdate() {
        if (this.state.caret !== false) {
            let caretPos = this.state.caret;
            let groups = this.groups();
            let gi = -1;

            for (let i = 0; i < groups.length; i++) {
                let gl = groups[i].text.length;
                if (caretPos <= gl) {
                    gi = i;
                    break;
                } else {
                    caretPos -= gl;
                }
            }

            if (gi >= 0 && gi < this.refs.e.children.length) {
                let elem = this.refs.e.children[gi].childNodes[0];
                let range = document.createRange();
                let sel = window.getSelection();

                range.setStart(elem, Math.min(elem.length, caretPos));
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    }

    handleKey(key, special, ctrl) {
        let caret = this.currentCaret();
        let suppress = false;
        let update = true;

        if (key === 13) {
            suppress = true;
        } else if (special && (key === 38 || key === 40)) {
            suppress = true;
            update = caret.type === 'nbr';
            if (caret.type === 'txt' && this.refs.vt) {
                this.refs.vt.move(key === 38 ? -1 : 1);
            }
        } else if (ctrl && (key === 37 || key === 38)) {
            update = false;
        }

        if (update) {
            let vars = this.props.vars;

            setTimeout(() => {
                let element = this.refs.e;

                if (element) {
                    let caret = this.currentCaret();
                    let globalCaret = caret.pos;
                    let text = '';

                    if (element && element.children.length > 0) {
                        for (let i = 0; i < element.children.length; i++) {
                            let child = element.children[i];
                            let blockText = this.b2t(child.innerHTML);
                            let managed = child.attributes['data-managed'] && child.attributes['data-managed'].nodeValue;
                            let type = child.attributes['data-type'] && child.attributes['data-type'].nodeValue;
                            let isTarget = i === caret.block;
                            let skip = !!child.attributes['data-skip'];

                            if (!skip) {
                                if (managed) {
                                    if (blockText.length >= vars[managed].name.length) {
                                        let vtext = '[' + vars[managed].name + '|' + managed + ']';
                                        if (isTarget) {
                                            let initial = child.attributes['data-initial'].nodeValue;
                                            if (caret.pos > 0 && blockText.substr(caret.pos) === initial) {
                                                vtext = blockText.substr(0, caret.pos) + vtext;
                                            } else if (caret.pos > initial.length && blockText.substr(0, initial.length) === initial) {
                                                vtext += blockText.substr(initial.length);
                                            }
                                        }
                                        text += vtext;
                                    }
                                } else if (special && isTarget && [38, 40].indexOf(key) >= 0 && type === 'nbr') {
                                    let length = blockText.length;
                                    let isNeg = blockText.charAt(0) === '-';
                                    let decP = blockText.indexOf('.');
                                    let p = length - (isNeg ? 0 : 0) - (decP >= 0 ? blockText.length - decP : 0) - caret.pos;
                                    let inc = Math.pow(10, p) * (key === 38 ? 1 : -1);
                                    let newValue = parseFloat(blockText) + inc;
                                    let newValueStr = newValue.toString();

                                    text += newValueStr;
                                    globalCaret += newValueStr.length - length;
                                } else {
                                    text += this.b2t(child.innerHTML);
                                }
                            }

                            if (caret && i < caret.block) {
                                globalCaret += blockText.length;
                            }
                        }

                    } else {
                        text = this.b2t(element.innerHTML);
                    }

                    let txtEx = /(\[[^\|]+\|([^\]]+)])|([_a-zA-Z]+[\._a-zA-Z0-9]*)/g;
                    let extraChars = 0;
                    let replaces = [];
                    var txtMatch;

                    while ((txtMatch = txtEx.exec(text)) !== null) {
                        let vtxt = txtMatch[0];
                        if (vtxt.charAt(0) === '[') {
                            extraChars += 3 + txtMatch[2].length;
                        } else {
                            let index = txtMatch.index;
                            let txtIndex = index - extraChars;

                            if (globalCaret < txtIndex || globalCaret > txtIndex + vtxt.length) {
                                if (vtxt in vars) {
                                    replaces.push({
                                        i: index,
                                        t: vtxt,
                                        v: vtxt,
                                        m: globalCaret > txtIndex + vtxt.length
                                    });
                                }
                            } else if (key === 13 && this.refs.vt) {
                                let selected = this.refs.vt.selected();
                                if (selected) {
                                    replaces.push({i: index, t: vtxt, v: selected, m: globalCaret > txtIndex});
                                }
                            }
                        }
                    }

                    for (let j = replaces.length - 1; j >= 0; j--) {
                        let replace = replaces[j];
                        text = text.substr(0, replace.i) + '[' + vars[replace.v].name + '|' + replace.v + ']' + text.substr(replace.i + replace.t.length);
                        if (replace.m) {
                            globalCaret += vars[replace.v].name.length - replace.t.length;
                        }
                    }

                    let value = this.outputText(text);

                    if (value !== this.state.value && this.props.onChange) {
                        this.props.onChange(value);
                    }

                    this.setState({
                        text: text,
                        caret: globalCaret,
                        value: value,
                        v: this.state.v + 1
                    });
                }
            }, 0);
        }

        return suppress;
    }

    handleKeyPress(e) {
        if (this.handleKey(e.which, false)) {
            e.preventDefault();
            e.stopPropagation();
        }
    }

    handleKeyDown(e) {
        let isTab = e.which === 9;
        let isDelete = e.which === 8 || e.which === 46;
        let isVerticalArrow = e.which === 38 || e.which === 40;
        let isHorizontalArrow = e.which === 37 || e.which === 39;
        let isShift = e.getModifierState("Shift");

        if (isTab || ((isDelete || isVerticalArrow || (isHorizontalArrow && !isShift)) && this.handleKey(e.which, true))) {
            e.preventDefault();
            e.stopPropagation();
        }

        if (['Tab', 'Escape'].indexOf(e.key) >= 0) {
            this.props.onKey(e);
        }
    }


    render() {
        let v = this.state.v;
        let groups = this.groups();
        let target = -1;
        let caret = this.state.caret;
        let html = '';
        let varTree = false;

        for (let i = 0; i < groups.length; i++) {
            let group = groups[i];
            let text = group.text;

            if (target < 0) {
                let overTarget = caret < text.length || (caret === text.length && (group.type === 'txt' || group.type === 'mng' || group.type === 'nbr'));
                if (overTarget || i === groups.length - 1) {
                    target = i;
                    if (group.type === 'txt') {
                        varTree = <VarPicker ref="vt" text={group.text} vars={this.props.vars}/>;
                    }
                } else {
                    caret -= text.length;
                }
            }

            let entity = group.managed ? this.props.vars[group.managed].entity : '';

            html += `<div data-v="${v}" data-index="${i}" data-content="${entity}" data-type="${group.type}" data-managed="${group.managed || ''}" data-initial="${text}" class="eq-part ${group.type + (target === i ? ' target' : '')}">${this.t2b(text)}</div>`;
        }

        return (
            <div className="eq-input">
                <div contentEditable={true} ref="e" spellCheck="false"
                     onKeyPress={this.handleKeyPress}
                     onKeyDown={this.handleKeyDown}
                     onFocus={this.props.onFocus}
                     onBlur={this.props.onBlur}
                     dangerouslySetInnerHTML={{__html: html}}></div>
                {varTree}
            </div>
        );
    }
}
