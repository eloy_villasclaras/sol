/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import OnfocusTextField from "./onfocus-text-field.jsx.js";

const style = {width: '100%'};

export default class EntityTextField extends React.Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value) {
        this.props.onUpdate({
            type: this.props.actionType,
            itemId: this.props.id,
            key: this.props.attr,
            value: value
        });
    }

    render() {
        let {value, label, edit, attr} = this.props;

        return <OnfocusTextField attr={attr} value={value} label={label} fieldStyle={style} edit={edit}
                                 onChange={this.handleChange} errorText={false}/>;
    }
}
