/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import LabelValue from "./label-value.jsx.js";
import OptionsDialog from "./options-dlg.jsx.js";

export default class SelectDlgField extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleClick() {
        let {edit, onSelect, type, id, attr} = this.props;
        if (edit) {
            onSelect(type, id, attr);
        }
    }

    handleCancel() {
        let props = this.props;
        props.onSelect(props.type, props.id, null);
    }

    handleSubmit(value) {
        this.handleCancel();
        this.props.onSubmit(value);
    }

    render() {
        let {
            attr,
            value = null,
            options,
            label = attr,
            title = 'Select: ' + label,
            edit,
            isSelected,
            emptyText = ''
        } = this.props;

        let notEmpty = value in options;
        let valueText = notEmpty ? options[value] : emptyText;
        let open = edit && isSelected;

        return (
            <div>
                <LabelValue value={valueText} label={label} selected={open} onClick={this.handleClick}/>
                <OptionsDialog value={value} options={options} title={title} open={open}
                               onSubmit={this.handleSubmit} onCancel={this.handleCancel}/>
            </div>
        );
    }
}
