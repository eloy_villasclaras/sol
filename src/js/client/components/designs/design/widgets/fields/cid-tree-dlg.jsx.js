/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";
import {Tree, TreeLeaf} from "../tree";
import TitleWidget from "../title-widget.jsx";
import systemManager from "../../../../../../logic/optics/system-manager";
import {treeTitleSettings} from "../../../../../theme";


export default class CidTreeDlg extends React.Component {
    constructor(props) {
        super();
        this.state = {selected: props.value || null};

        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit() {
        this.props.onSelect(this.state.selected);
    }

    handleClose() {
        this.props.onCancel();
    }

    handleClick(value) {
        this.setState({selected: value});
    }

    renderItemTitle(id, fullId, item, isRoot) {
        let {info, acceptType, rootLabel} = this.props;
        let selected = fullId === this.state.selected;
        let style = selected ? treeTitleSettings.selected : treeTitleSettings.unselected;
        let icon = item.type === 'element' && (item.optics === 'lens' ? 'lens' : 'panorama_fish_eye');
        let onClick = fullId && item.type === acceptType && (() => this.handleClick(fullId));
        let text = isRoot ? rootLabel : (info[fullId].title || id);

        return <TitleWidget title={text} onClick={onClick} icon={icon} s
                            tyle={style.title} iconStyle={style.icon}/>;
    }

    renderTree(id, fullId, item, isRoot) {
        let children = item.children && Object.keys(item.children).map(key => {
                let child = item.children[key];
                let childFullId = systemManager.id(fullId, key);
                let childElement = child.type === 'group' ?
                    this.renderTree(key, childFullId, child, false) :
                    this.renderItemTitle(key, childFullId, child, false);

                return <TreeLeaf key={key}>{childElement}</TreeLeaf>;
            });
        let title = this.renderItemTitle(id, fullId, item, isRoot);

        return <Tree header={title}>{children}</Tree>;
    }

    render() {
        let {value, root, title, open} = this.props;

        let customActions = [
            <FlatButton
                label="Cancel"
                secondary={true}
                onTouchTap={this.handleClose}/>,
            <FlatButton
                label="Ok"
                primary={true}
                onTouchTap={this.handleSubmit}
                disabled={value === this.state.selected}/>
        ];

        let tree = this.renderTree('Components', false, root, true);

        return (
            <Dialog title={title}
                    autoDetectWindowHeight={true}
                    autoScrollBodyContent={true}
                    actions={customActions}
                    modal={true}
                    open={open}>
                <div className="component-select-tree">
                    {tree}
                </div>
            </Dialog>
        );
    }
}
