/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import classNames from "classnames";
import TextField from "material-ui/TextField";
import entityManager from "../../../../../../../logic/entity/entity";
import ErrorIcon from "../../../widgets/error-icon.jsx";
import LabelValue from "../label-value.jsx.js";
import EqField from "./eq-field.jsx";
import formatNumber from "./number-format";


export default class VarEqField extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.handleKey = this.handleKey.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChangeValue = this.handleChangeValue.bind(this);
    }

    handleClick() {
        this.props.onSelect(this.props.varId);
    }


    entityVars() {
        return this.props.vars[this.props.entity.fullId];
    }

    handleKey(e) {
        if (e.key === 'Escape') {
            this.handleClose();
        } else if (e.key === 'Tab') {
            e.preventDefault();
            e.stopPropagation();

            let keys = Object.keys(this.entityVars());
            let index = keys.indexOf(this.props.varId);
            let nextIndex = (index + (e.shiftKey ? (keys.length - 1) : 1)) % keys.length;
            let varId = keys[nextIndex];

            this.props.onSelect(varId);
        }
    }

    handleClose() {
        this.props.onSelect(null);
    }

    handleChangeValue(value) {
        this.props.onChange(value);
    }

    render() {
        let {varId, label, units, isSelected, info, entity, vars, edit} = this.props;
        let className = classNames("var-field", {'selected': isSelected, 'var-field-editable': edit});
        let computedVar = this.entityVars()[varId];
        let [displayValue, displayUnits] = computedVar && typeof computedVar.val === 'number' ?
            formatNumber(computedVar.val, units) :
            [<ErrorIcon/>, units];

        let relVars = entityManager.relative(info, entity.fullId, varId, vars),

            field = isSelected && (
                    <TextField key={varId}
                               name={varId}
                               style={{width: '100%'}}
                               autoFocus>
                        <EqField value={computedVar.eq} vars={relVars}
                                 onChange={this.handleChangeValue}
                                 onKey={this.handleKey}/>
                    </TextField>
                );

        return (
            <div className={className}>
                <LabelValue value={displayValue} label={label} units={displayUnits} selected={isSelected}
                            onClick={this.handleClick}/>
                {field}
            </div>
        );
    }
}
