/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FontIcon from "material-ui/FontIcon";

export default ({text, type}) => {
    let errorStyle = {
        color: '#FF6C40',
        fontSize: 16,
        verticalAlign: 'middle',
        marginLeft: text ? 10 : 0
    };

    let iconName = type === 'child' ? 'error_outline' : 'error';
    let icon = <FontIcon className="material-icons" style={errorStyle}>{iconName}</FontIcon>;

    return text ? <span>{text}{icon}</span> : icon;
}
