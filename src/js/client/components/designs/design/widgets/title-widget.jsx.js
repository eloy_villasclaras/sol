/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import extend from "extend";
import FontIcon from "material-ui/FontIcon";

const defaultIconStyle = {
    padding: 12
};

export default ({icon, iconStyle, style, menu, onClick, title}) => {
    let appliedIconStyle = icon && (iconStyle ? extend({}, defaultIconStyle, iconStyle) : defaultIconStyle);
    let iconElement = icon && (
            <div className="component-title-icon">
                <FontIcon className="material-icons" style={appliedIconStyle}>{icon}</FontIcon>
            </div>
        );

    let menuElement = menu && (
            <div className="component-title-menu">
                <div>
                    {menu}
                </div>
            </div>
        );

    let textStyle = onClick ? {cursor: 'pointer'} : null;
    let className = 'component-title-text' + (onClick ? ' clickable' : '');

    return (
        <div className="component-title" style={style}>
            {iconElement}
            <div className={className} style={textStyle} onClick={onClick}>
                {title}
            </div>
            {menuElement}
        </div>
    );
}
