/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import IconButton from "material-ui/IconButton";
import {selectedTreeIconButtonSettings, treeIconButtonSettings} from "../../../../../theme";

export default class Tree extends React.Component {
    constructor(props) {
        super();
        this.state = {open: props.open !== false};

        this.onToggle = this.onToggle.bind(this);
    }


    onToggle() {
        this.setState({open: !this.state.open});
    }

    render() {
        let open = this.state.open;
        let {header, className = '', children, selected} = this.props;
        let leaves = open && <div className="tree-children">{children}</div>;
        let settings = selected ? selectedTreeIconButtonSettings : treeIconButtonSettings;

        return (
            <div className={`tree ${className}`}>
                <div className="tree-header">
                    <div className="tree-header-toggle">
                        <IconButton iconClassName="material-icons" {...settings}
                                    onClick={this.onToggle}>
                            {open ? 'indeterminate_check_box' : 'add_box'}
                        </IconButton>
                    </div>
                    <div className="tree-header-content">{header}</div>
                </div>
                {leaves}
            </div>
        );
    }
}
