/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {Card, CardActions, CardHeader, CardMedia} from "material-ui/Card";
import RaisedButton from "material-ui/RaisedButton";
import DesignTeaserDrawing from "./design-teaser-drawing.jsx";

const mediaWrapperStyle = {
    position: 'relative',
    paddingBottom: '60%'
};

const mediaStyle = {
    position: 'absolute',
    background: '#eee',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%'
};

export default ({designs, onOpen}) => {
    let items = designs.map(design => (
            <div key={design.id} className="design-list__item">
                <Card>
                    <CardHeader title={design.title}/>
                    <CardMedia style={mediaWrapperStyle} mediaStyle={mediaStyle}>
                        <DesignTeaserDrawing design={design}/>
                    </CardMedia>
                    <CardActions>
                        <RaisedButton label="Open" onClick={() => onOpen(design.id)}/>
                    </CardActions>
                </Card>
            </div>
        )
    );

    return (
        <div className="design-list">
            {items}
        </div>
    );
}
