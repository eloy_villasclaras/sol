/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Helmet from "react-helmet";
import {connect} from "react-redux";
import {Link} from "react-router";
import {push} from "react-router-redux";
import {searchDocs} from "../../../stores/design-list-actions";
import permissions from "../../../../model/permissions";
import LoginNotice from "../../widgets/login-notice.jsx";
import DocumentLayout from "../../widgets/document-layout.jsx.js";
import DesignList from "./design-list.jsx";

class DesignsHomePage extends React.Component {
    constructor() {
        super();
        this.handleOpen = this.handleOpen.bind(this);
    }

    componentWillMount() {
        this.props.onSearch();
    }

    handleOpen(id) {
        this.props.onPush(`/designs/${id}`);
    }

    renderCreateSection() {
        var user = this.props.login.user;
        if (user) {
            if (!permissions.check(user, 'create-doc')) {
                return (
                    <div>
                        <Link to="/designs/create">Create new design</Link>
                    </div>
                );
            } else {
                return false;
            }
        } else {
            return (
                <LoginNotice hideKey="login-to-create">
                    <p>Login or register to create your own designs!</p>
                </LoginNotice>
            );
        }
    }

    render() {
        return (
            <DocumentLayout>
                <Helmet title="Designs"/>
                {this.renderCreateSection()}
                <DesignList designs={this.props.list.designs} onOpen={this.handleOpen}/>
            </DocumentLayout>
        );
    }
}


const mapStateToProps = state => {
    return {
        login: state.login,
        list: state.list
    };
};

const mapDispatchToProps = dispatch => ({
    onSearch: () => dispatch(searchDocs()),
    onPush: path => dispatch(push(path))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignsHomePage);

