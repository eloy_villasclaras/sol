/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {wrap} from "../../widgets/sized-wrapper.jsx";
import ElementsSvg from "../design/drawing/elements/elements-svg.jsx";
import {boundingBox, analyze} from "../../../../logic/optics/element-ops";
import {evalVars} from "../../../../logic/entity/entity";

const DesignTeaserDrawing = ({design: {content: {components}}, size}) => {
    let componentVars = evalVars(components);
    let rootComponent = analyze(components, componentVars);
    let bb = boundingBox(rootComponent);

    let viewBox = [bb[0] - 10, -bb[3] - 10, bb[2] - bb[0] + 20, bb[3] - bb[1] + 20];
    let scale = size.width / Math.max(viewBox[2], viewBox[3]);

    return (
        <svg className="drawing-svg drawing-svg-teaser" width="0" height="0"
             viewBox={viewBox.join(' ')}
             preserveAspectRatio="xMidYMid meet">
            <ElementsSvg rootComponent={rootComponent} scale={scale} selectedId={null}/>
        </svg>
    );
};

export default wrap(DesignTeaserDrawing, {style: {height: '100%'}});
