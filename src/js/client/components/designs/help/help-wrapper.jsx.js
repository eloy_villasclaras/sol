/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import {load} from "../../../stores/content-page-actions";
import urlUtils from "../../../utils/url-utils";
import Splitter from "../../widgets/splitter.jsx";
import Breadcrumbs from "../../content-page/breadcrumbs.jsx";
import Markdown from "../../content-page/markdown.jsx";

class HelpWrapper extends React.Component {
    componentDidMount() {
        if (this.props.help) {
            this.props.onLoad(this.props.help);
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.help && this.props.help !== prevProps.help) {
            this.props.onLoad(this.props.help);
        }
    }

    renderHelp() {
        if (this.props.help) {
            let {help, content, loading, basepath, query} = this.props;
            let options = {
                linkCurrent: help,
                linkHref: path => {
                    return urlUtils.overwrite(basepath, query, {help: path})
                }
            };


            return (
                <div className="side-help">
                    <Breadcrumbs path={help} loading={loading} options={options}/>
                    <Markdown content={content} options={options}/>
                </div>
            );
        }

        else {
            return
            false;
        }
    }

    render() {
        var help = this.renderHelp();
        return help ? <Splitter defaultPosition={0.66}>{this.props.children}{help}</Splitter> : this.props.children;
    }
}

const mapStateToProps = (state, ownProperties) => {
    var help = state.routing.locationBeforeTransitions.query.help;
    if (help) {
        help = help.replace(/\/$/, '');
        if (help.charAt(0) !== '/') {
            help = '/' + help;
        }
    }
    return {
        loading: state.contentPage.loading,
        content: state.contentPage.content,
        basepath: state.routing.locationBeforeTransitions.pathname,
        query: state.routing.locationBeforeTransitions.query,
        help: help,
        children: ownProperties.children
    };
};

const mapDispatchToProps = dispatch => ({
    onLoad: path => dispatch(load(path))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HelpWrapper);
