/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import TextField from "material-ui/TextField";
import RaisedButton from "material-ui/RaisedButton";
import RefreshIndicator from "material-ui/RefreshIndicator";
import {postDoc} from "../../stores/design-list-actions";
import permissions from "../../../model/permissions";
import DocumentLayout from "../widgets/document-layout.jsx";
import HelpWrapper from "./help/help-wrapper.jsx";
import Notice from "../widgets/notice.jsx";
import LoginNotice from "../widgets/login-notice.jsx";

class DesignCreatePage extends React.Component {
    constructor() {
        super();
        this.state = {
            title: 'New design',
            description: ''
        };

        this.handleTitleInputChange = this.handleTitleInputChange.bind(this);
        this.handleDescriptionInputChange = this.handleDescriptionInputChange.bind(this);
        this.handleCreate = this.handleCreate.bind(this);
    }

    handleTitleInputChange(e) {
        this.setState({title: e.target.value});
    }

    handleDescriptionInputChange(e) {
        this.setState({description: e.target.value});
    }

    handleCreate() {
        this.props.onPostDoc({title: this.state.title, description: this.state.description}, this.props.query);
    }

    renderContent() {
        let {title, description} = this.state;
        let {login, working} = this.props;

        if (login.user) {
            if (!permissions.check(login.user, 'create-doc')) {
                let titleError = !title && 'Title cannot be empty';
                let workingIndicator = working && <RefreshIndicator status="loading" left={100} top={0}/>;
                let createDisabled = working || titleError;

                return (
                    <div>
                        <div>
                            <TextField floatingLabelText="Title"
                                       value={title}
                                       errorText={titleError}
                                       onChange={this.handleTitleInputChange}
                                       disabled={working}/>
                        </div>
                        <div>
                            <TextField floatingLabelText="Description"
                                       value={description}
                                       onChange={this.handleDescriptionInputChange}
                                       disabled={working}/>
                        </div>
                        <div style={{position: 'relative'}}>
                            <RaisedButton label="Create"
                                          disabled={createDisabled}
                                          onClick={this.handleCreate}/>
                            {workingIndicator}
                        </div>
                    </div>
                );
            } else {
                return (
                    <Notice>
                        <p>Sorry, you don't have permission to create new designs.</p>
                    </Notice>
                );
            }
        } else {
            return (
                <LoginNotice>
                    <p>Login or register to create your own designs!</p>
                </LoginNotice>
            );
        }
    }

    render() {
        return (
            <HelpWrapper>
                <DocumentLayout>
                    {this.renderContent()}
                </DocumentLayout>
            </HelpWrapper>
        )
    }
}


const mapStateToProps = state => {
    return {
        login: state.login,
        working: state.list.creating,
        query: state.routing.locationBeforeTransitions.query
    };
};

const mapDispatchToProps = dispatch => ({
    onPostDoc: (data, query) => dispatch(postDoc(data, query))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DesignCreatePage);
