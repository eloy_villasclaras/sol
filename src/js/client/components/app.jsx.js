/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Paper from "material-ui/Paper";
import Helmet from "react-helmet";


export default ({children})=> {
    return (
        <div style={{background: '#eee', height: '100%'}}>
            <Paper rounded={false} zDepth={0} style={{height: '100%'}}>
                <Helmet title="Simple Optics Lab"/>
                {children}
            </Paper>
        </div>
    );
}
