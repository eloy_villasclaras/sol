/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import extend from "extend";
import RaisedButton from "material-ui/RaisedButton";
import settings from "../../settings/settings";
import {openGoogleLogin, adminTokenLogin} from "../../stores/login-actions";


const LoginPanel = ({onAdminTokenLogin, onOpenGoogleLogin, children}) => {
    let tokenLogin = settings.login.token && <RaisedButton label="Admin token login" onClick={onAdminTokenLogin}/>;

    return (
        <div>
            {children}
            <div>
                {tokenLogin}
                <RaisedButton label="Google login" onClick={onOpenGoogleLogin}/>
            </div>
        </div>
    );
};

const mapDispatchToProps = (dispatch, ownProps) => extend(
    {},
    ownProps,
    {
        onOpenGoogleLogin: () => dispatch(openGoogleLogin()),
        onAdminTokenLogin: () => dispatch(adminTokenLogin())
    }
);

export default connect(false, mapDispatchToProps)(LoginPanel);
