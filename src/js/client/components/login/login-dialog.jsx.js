/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";
import LoginPanel from "./login-panel.jsx";


export default ({open, onCancel, children}) => {
    let customActions = [
        <FlatButton label="Cancel" secondary={true} onTouchTap={onCancel}/>
    ];

    return (
        <Dialog title="Login"
                autoDetectWindowHeight={true}
                autoScrollBodyContent={true}
                actions={customActions}
                modal={true}
                open={open}>
            <LoginPanel>{children}</LoginPanel>
        </Dialog>
    );
};
