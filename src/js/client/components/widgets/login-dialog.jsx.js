/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import extend from "extend";
import {connect} from "react-redux";

import RaisedButton from "material-ui/RaisedButton";
import FlatButton from "material-ui/FlatButton";
import Dialog from "material-ui/Dialog";

import {openGoogleLogin, adminTokenLogin} from "../../stores/login-actions";

import settings from "../../settings/settings";


const LoginDialog = ({open, onCancel, onAdminTokenLogin, onOpenGoogleLogin, children}) => {
    let token = settings.login.token ?
        <RaisedButton label="Admin token login" onClick={onAdminTokenLogin}/> : false;

    let customActions = [
        <RaisedButton label="Google login" onClick={onOpenGoogleLogin}/>,
        <FlatButton label="Cancel" secondary={true} onTouchTap={onCancel}/>
    ];

    return (
        <Dialog title="Login"
                autoDetectWindowHeight={true}
                autoScrollBodyContent={true}
                actions={customActions}
                modal={true}
                open={open}>
            {children}
        </Dialog>
    );
};

const mapDispatchToProps = (dispatch, ownProps) => extend(
    {},
    ownProps,
    {
        onOpenGoogleLogin: () => dispatch(openGoogleLogin()),
        onAdminTokenLogin: () => dispatch(adminTokenLogin())
    }
);

export default connect(false, mapDispatchToProps)(LoginDialog);
