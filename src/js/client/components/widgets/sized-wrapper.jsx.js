/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


'use strict';

import React from "react";
import throttle from "lodash.throttle";

const listeners = [];
const onResize = () => {
    for (let i = 0; i < listeners.length; i++) {
        listeners[i]();
    }
};

const onResizeThrottled = throttle(onResize, 10);

window.addEventListener("resize", onResizeThrottled);

const addListener = function (listener) {
    if (listeners.indexOf(listener) < 0) {
        listeners.push(listener);
    }
};

const removeListener = function (listener) {
    let i = listeners.indexOf(listener);
    if (i >= 0) {
        listeners.splice(i, 1);
    }
};


export default class SizedWrapper extends React.Component {
    constructor() {
        super();
        this.state = {size: false};
        this._updateSize = this._updateSize.bind(this);
        addListener(this._updateSize);
    }

    _updateSize() {

        this.setState({
            size: this.refs.wrapper ? {
                width: this.refs.wrapper.clientWidth,
                height: this.refs.wrapper.clientHeight
            } : false
        });
    }

    componentDidMount() {
        this._updateSize();
    }

    componentWillUnmount() {
        removeListener(this._updateSize);
    }

    render() {
        let content = this.state.size && this.renderWrapped();
        return <div {...this.wrapperProps} ref="wrapper">{content}</div>;
    }
}

export const wrap = (WrappedComponent, wrapperProps) => class extends SizedWrapper {
    constructor() {
        super();
        this.wrapperProps = wrapperProps;
    }

    renderWrapped() {
        return <WrappedComponent {...this.props} size={this.state.size}/>;
    }
};
