/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import Paper from "material-ui/Paper";
import FlatButton from "material-ui/FlatButton";
import storage from "../../utils/local-storage";
import LoginDialog from "../login/login-dialog.jsx";

export default class LoginNotice extends React.Component {
    constructor(props) {
        super();
        this.state = {
            open: false,
            hidden: props.hideKey && storage.load(props.hideKey, false)
        };

        this.handleHide = this.handleHide.bind(this);
        this.handleToggle = this.handleToggle.bind(this);
    }


    handleHide() {
        if (this.props.hideKey) {
            storage.save(this.props.hideKey, true);
        }
        this.setState({hidden: true});
    }

    handleToggle() {
        this.setState({open: !this.state.open});
    }

    render() {
        let {hideKey, children} = this.props;
        let {open, hidden} = this.state;

        if (hidden) {
            return false;
        } else {
            let hideButton = hideKey && <FlatButton label="I got it, hide this" onClick={this.handleHide}/>;

            return (
                <div style={{padding: '0 20px'}}>
                    <Paper zDepth={1} style={{background: '#eee', padding: '10px 20px 5px'}}>
                        {children}
                        <div style={{textAlign: 'right'}}>
                            <FlatButton label="Login" onClick={this.handleToggle}/>
                            {hideButton}
                        </div>
                    </Paper>
                    <LoginDialog open={open} onCancel={this.handleToggle}>
                        {children}
                    </LoginDialog>
                </div>
            );
        }
    }
}
