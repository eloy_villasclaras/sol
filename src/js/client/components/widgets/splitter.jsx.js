/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import domOps from "../../utils/dom-ops";
import AbstractMouseDragComponent from "./abstract-mouse-drag-component";

export default class Splitter extends AbstractMouseDragComponent {
    constructor(props) {
        super();
        this.state.position = props.defaultPosition || .5;
    }

    handleDrag(newX, oldX, newY, oldY) {
        var node = this.refs.domNode,
            offset = domOps.offset(node),
            pos = Math.min(1, Math.max(0, this.props.vertical ? (newY - offset.offsetTop) / node.clientHeight : (newX - offset.offsetLeft) / node.clientWidth));

        return {
            position: pos
        };
    }

    render() {
        var firstComponent = this.props.children[0],
            firstWidth = this.state.position,
            secondComponent = this.props.children.slice(1),
            secondWidth = 1 - firstWidth,
            directionClass = this.props.vertical ? 'flex-columns' : 'flex-rows',
            typeClass = (this.props.className ? this.props.className + ' splitter ' : 'splitter ') +
                directionClass;

        return (
            <div ref="domNode" className={typeClass}>
                <div className="splitter-content-wrapper flex-columns"
                     style={{flex: firstWidth + ' ' + firstWidth}}>
                    <div className={directionClass}>
                        {firstComponent}
                    </div>
                </div>
                <div className="splitter-separator" onMouseDown={this.startDrag}/>
                <div className="splitter-content-wrapper flex-columns"
                     style={{flex: secondWidth + ' ' + secondWidth}}>
                    <div className={directionClass}>
                        {secondComponent}
                    </div>
                </div>
            </div>
        );
    }
}
