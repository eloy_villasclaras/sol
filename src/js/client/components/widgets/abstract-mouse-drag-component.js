/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";

export default class AbstractMouseDragComponent extends React.Component {
    constructor() {
        super();
        this.state = {drag: false};

        this.startDrag = this.startDrag.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
    }

    componentDidUpdate(props, state) {
        if (this.state.dragging && !state.dragging) {
            document.addEventListener('mousemove', this.onMouseMove);
            document.addEventListener('mouseup', this.onMouseUp);
        } else if (!this.state.dragging && state.dragging) {
            document.removeEventListener('mousemove', this.onMouseMove);
            document.removeEventListener('mouseup', this.onMouseUp);
        }
    }

    startDrag(e, data) {
        e.stopPropagation();
        e.preventDefault();

        if (e.button === 0) {
            this.setState({
                dragging: {
                    x: e.pageX,
                    y: e.pageY,
                    data: data || null
                }
            });
        }
    }

    onMouseUp(e) {
        e.stopPropagation();
        e.preventDefault();
        this.setState({dragging: false});
        if (this.handleDragEnd) {
            this.handleDragEnd();
        }
    }

    onMouseMove(e) {
        var dragging = this.state.dragging;
        if (dragging) {
            var newState = this.handleDrag(e.pageX, dragging.x, e.pageY, dragging.y, dragging.data) || {};
            newState.dragging = {
                x: e.pageX,
                y: e.pageY,
                data: dragging.data
            };

            this.setState(newState);
        }
    }
}
