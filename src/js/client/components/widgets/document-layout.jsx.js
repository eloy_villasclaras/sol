/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {Link} from "react-router";

export default ({children}) => (
    <div className="flex-columns document-layout">
        <div className="flex-content document-layout-content">
            <div>
                {children}
            </div>
        </div>
        <div className="footer">
            <div>
                <span>© 2015-2016 Eloy Villasclaras</span>
                <div>
                    <Link to="/about">About SOL</Link>
                    <Link to="/contact">Contact</Link>
                    <Link to="/cookies">Cookies</Link>
                </div>
            </div>
        </div>
    </div>
);