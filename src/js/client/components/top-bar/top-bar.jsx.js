/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import React from "react";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import {Link} from "react-router";
import Paper from "material-ui/Paper";
import Tabs from "material-ui/Tabs/Tabs";
import Tab from "material-ui/Tabs/Tab";
import theme from "../../theme";

const tabDefinition = [
    {value: '/designs', label: 'Designs'},
    {value: '/guide', label: 'Guide'}
];

const styles = {
    bar: {
        background: theme.appBar.color
    },
    text: {
        color: theme.appBar.textColor
    }
};

const TopBarView = ({title, login, path, onPush}) => {
    let loggedIn = !!login.user;
    let profileLabel = loggedIn ? 'My profile' : 'Log in/Register';

    let titleElement = title ? (
        <div className="top-bar-title">
            <Link to="/" style={styles.text}>{title}</Link>
        </div>
    ) : false;

    let value = path;
    let tabs = [...tabDefinition, {value: '/profile', label: profileLabel}].map((def, i) => {
        if (path !== '/' && path.indexOf(def.value) === 0) {
            value = def.value;
        }
        return <Tab key={i} {...def} onClick={() => onPush(def.value)}></Tab>;
    });

    return (
        <Paper zDepth={0} rounded={false} style={styles.bar} className="top-bar">
            {titleElement}
            <div className="top-bar-tabs">
                <Tabs value={value}>
                    {tabs}
                </Tabs>
            </div>
        </Paper>
    );
};

const mapStateToProps = (state, ownProps) => {
    return {
        title: ownProps.title,
        login: state.login,
        path: state.routing.locationBeforeTransitions.pathname
    };
};

const mapDispatchToProps = dispatch => ({
    onPush: path => dispatch(push(path))
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TopBarView);
