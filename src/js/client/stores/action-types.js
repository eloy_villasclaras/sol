/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

export default {
    SEARCH_DESIGNS: 'SEARCH_DESIGNS',
    RECEIVE_DESIGNS: 'RECEIVE_DESIGNS',
    CREATE_DESIGN: 'CREATE_DESIGN',
    CREATE_DESIGN_FAILED: 'CREATE_DESIGN_FAILED',
    LOAD_CONTENT_PAGE: 'LOAD_CONTENT_PAGE',
    RECEIVE_CONTENT_PAGE: 'RECEIVE_CONTENT_PAGE',
    SELECT_ENTITY: 'SELECT_ENTITY',
    LOAD_DOCUMENT: "LOAD_DOCUMENT",
    RECEIVE_DOCUMENT: "RECEIVE_DOCUMENT",
    DOCUMENT_SAVED: "DOCUMENT_SAVED",
    UPDATE_DOCUMENT: "UPDATE_DOCUMENT",
    UNDO_CHANGE: "UNDO_CHANGE",
    REDO_CHANGE: "REDO_CHANGE",
    SIM_RESULTS_COMPLETE: "SIM_RESULTS_COMPLETE",
    UPDATE_LOGIN: "UPDATE_LOGIN"
};