/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import qwest from "qwest";
import settings from "../settings/settings";
import actionTypes from "./action-types";

const cache = {};

function receive(path, content) {
    return {
        type: actionTypes.RECEIVE_CONTENT_PAGE,
        path: path,
        content: content
    };
}

export function load(path) {
    return function (dispatch) {
        if (settings.usePageCache && path in cache) {
            dispatch(receive(path, cache[path]));
        } else {
            dispatch({
                type: actionTypes.LOAD_CONTENT_PAGE,
                path: path
            });
            return qwest.get('/pages' + path)
                .then(function (res) {
                    cache[path] = res.response;
                    return dispatch(receive(path, res.response));
                })
                .catch(function (res, a, b, c) {
                    return dispatch(receive(path, res));
                });
        }
    };
}
