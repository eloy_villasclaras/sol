/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import extend from "extend";
import clone from "clone";
import deepEqual from "deep-equal";
import designActionImpl from "../../logic/actions/design-impl";
import entity from "../../logic/entity/entity";
import elementOps from "../../logic/optics/element-ops";
import analysisOps from "../../logic/optics/analysis/analysis-ops";
import varNames from "./var-names";
import varUnits from "./var-units";
import actionTypes from "./action-types";

const INITIAL_STATE = {
    loading: false,
    saved: 0,
    version: 0,
    id: null,
    document: null,
    mementos: [],
    mementoHead: 0,
    processed: {}
};


function load(id) {
    return extend({}, INITIAL_STATE, {
        loading: true,
        id: id,
        saved: 0,
        version: 0
    });
}

function processed(simState, document) {
    var content = document.content,
        componentVars = entity.evalVars(content.components, varUnits),
        analysisVars = entity.evalVars(content.analysis, varUnits),
        components = elementOps.analyze(content.components, componentVars),
        analysis = analysisOps.analyze(content.analysis, analysisVars, componentVars),
        componentsChange = !deepEqual(components, simState.components),
        analysisChange = !deepEqual(analysis, simState.analysis),
        update = componentsChange || analysisChange;

    return {
        info: {
            components: entity.getInfo(content.components, varNames.component),
            analysis: entity.getInfo(content.analysis, varNames.analysis)
        },
        componentVars: componentVars,
        analysisVars: analysisVars,
        components: componentsChange ? components : simState.components,
        analysis: analysisChange ? analysis : simState.analysis,
        results: simState.results,
        simulating: update
    };
}

function update(state, changes) {
    var nextDocument = clone(state.document),
        mementos = [],
        _changes = Array.isArray(changes) ? changes : [changes];

    _changes.forEach(function (change) {
        var memento = {
            action: change,
            data: {}
        };

        designActionImpl.doAction(nextDocument, change, memento.data);
        mementos.push(memento);
    });

    return extend({}, state, {
        document: nextDocument,
        version: state.version + 1,
        mementos: [...state.mementos.slice(0, state.mementoHead), mementos],
        mementoHead: state.mementoHead + 1,
        processed: processed(state.processed, nextDocument)
    });
}

function save(state, version) {
    return extend({}, state, {saved: version});
}

function undo(state) {
    var nextDocument = clone(state.document),
        mementos = state.mementos[state.mementoHead - 1];

    for (var i = mementos.length - 1; i >= 0; i--) {
        designActionImpl.undoAction(nextDocument, mementos[i].action, mementos[i].data);
    }

    return extend({}, state, {
        document: nextDocument,
        version: state.version + 1,
        mementoHead: state.mementoHead - 1,
        processed: processed(state.processed, nextDocument)
    });
}

function redo(state) {
    var nextDocument = clone(state.document),
        mementos = state.mementos[state.mementoHead];

    for (var i = 0; i < mementos.length; i++) {
        designActionImpl.doAction(nextDocument, mementos[i].action, mementos[i].data);
    }

    return extend({}, state, {
        document: nextDocument,
        version: state.version + 1,
        mementoHead: state.mementoHead + 1,
        processed: processed(state.processed, nextDocument)
    });
}

function simResults(state, results) {
    return extend({}, state, {
        processed: extend({}, state.processed, {
            results: results,
            simulating: false
        })
    });
}

function receive(id, document) {
    return extend({}, INITIAL_STATE, {
        id: id,
        document: document,
        processed: processed({}, document),
        version: document.vid,
        saved: document.vid
    });
}


export default function reducer(state, action) {
    switch (action.type) {
        case actionTypes.LOAD_DOCUMENT:
            return load(action.id);
        case actionTypes.RECEIVE_DOCUMENT:
            return receive(action.id, action.document);
        case actionTypes.UPDATE_DOCUMENT:
            return update(state, action.changes);
        case actionTypes.DOCUMENT_SAVED:
            return save(state, action.version);
        case actionTypes.UNDO_CHANGE:
            return undo(state);
        case actionTypes.REDO_CHANGE:
            return redo(state);
        case actionTypes.SIM_RESULTS_COMPLETE:
            return simResults(state, action.results);
        default:
            return state || INITIAL_STATE;
    }
}
