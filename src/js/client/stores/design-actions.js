/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import {getDoc, updateDoc} from "../api";
import analysisOps from "../../logic/optics/analysis/analysis-ops";
import actionTypes from "./action-types";

const SAVE_TIMEOUT = 3000;

let save = false;

function wrapAction(actionImpl) {
    return function () {
        let args = arguments;

        return function (dispatch, getState) {
            let s1 = getState();
            dispatch(actionImpl.apply(null, args));
            let s2 = getState();
            if (!s1.design.processed ||
                s1.design.processed.components !== s2.design.processed.components ||
                s1.design.processed.analysis !== s2.design.processed.analysis) {

                simulate(dispatch, s2.design.processed);
            }

            if (!save) {
                save = setTimeout(() => {
                    save = false;
                    let state = getState();
                    updateDoc(state.design.id, state.design.document)
                        .then(function () {
                            dispatch({
                                type: actionTypes.DOCUMENT_SAVED,
                                version: state.design.version
                            })
                        });
                }, SAVE_TIMEOUT);
            }
        };
    };
}

function simulate(dispatch, processed) {
    analysisOps.simulateAsync(processed.components, processed.analysis, results => dispatch({
        type: actionTypes.SIM_RESULTS_COMPLETE,
        results: results
    }));
}

export function loadDesign(id) {
    return function (dispatch, getState) {
        dispatch({
            type: actionTypes.LOAD_DOCUMENT,
            id: id
        });
        return getDoc(id).then(doc => {
            dispatch({
                type: actionTypes.RECEIVE_DOCUMENT,
                id: id,
                document: doc
            });

            let state = getState();

            if (state.design.processed) {
                simulate(dispatch, state.design.processed);
            }
        });
    }
}

export const update = wrapAction(designActions => ({
    type: actionTypes.UPDATE_DOCUMENT,
    changes: designActions
}));


export const undo = wrapAction(() => ({
    type: actionTypes.UNDO_CHANGE
}));

export const redo = wrapAction(() => ({
    type: actionTypes.REDO_CHANGE
}));


export const canUndo = ({mementoHead})=> mementoHead > 0;
export const canRedo = ({mementoHead, mementos})=> mementoHead < mementos.length;
