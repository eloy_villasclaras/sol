/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import {listDocs, createDoc} from "../api";
import {push} from "react-router-redux";
import actionTypes from "./action-types";
import urlUtils from "../utils/url-utils";

export function searchDocs() {
    return function (dispatch) {
        dispatch({
            type: actionTypes.SEARCH_DESIGNS
        });

        listDocs().then(designs =>dispatch({
            type: actionTypes.RECEIVE_DESIGNS,
            designs: designs
        }));
    };
}

export function postDoc(data, query) {
    return function (dispatch) {
        dispatch({
            type: actionTypes.CREATE_DESIGN
        });

        createDoc(data).then(function (response) {
                if (response.id) {
                    dispatch(push(urlUtils.url(`/designs/${response.id}`, query)));
                } else {
                    dispatch({type: actionTypes.CREATE_DESIGN_FAILED});
                }
            })
            .catch(function (e) {
                dispatch({type: actionTypes.CREATE_DESIGN_FAILED});
            });
    }
}
