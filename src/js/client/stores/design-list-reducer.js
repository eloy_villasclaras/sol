/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import actionTypes from "./action-types";
import {LOCATION_CHANGE} from "react-router-redux";

export default function reducer(state, action) {
    switch (action.type) {
        case actionTypes.SEARCH_DESIGNS:
            return {
                loading: true,
                designs: state.designs,
                creating: false
            };
        case actionTypes.RECEIVE_DESIGNS:
            return {
                loading: false,
                designs: action.designs,
                creating: false
            };
        case actionTypes.CREATE_DESIGN:
            return {
                loading: state.loading,
                designs: state.designs,
                creating: true
            };
        case LOCATION_CHANGE:
        case actionTypes.CREATE_DESIGN_FAILED:
            return state.creating ? {
                loading: state.loading,
                designs: state.designs,
                creating: false
            } : state;
        default:
            return state || {
                    loading: false,
                    designs: [],
                    creating: false
                };
    }
}
