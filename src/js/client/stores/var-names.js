/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

const data = {
    component: {
        "x": ["X", "mm"],
        "y": ["Y", "mm"],
        "rot": ["Rotation", "°"],
        "ap": ["Aperture", "mm"],
        "th": ["Thickness", "mm"],
        "n": ["Refractive index", ""],
        "v": ["Abbe number", ""],
        "r": ["Radius", "mm"],
        "f": ["Focal distance", "mm"],
        "hsa": ["Semi axis (H)", "mm"],
        "vsa": ["Semi axis (V)", "mm"],
        "a": ["Angle", "°"],
        "hr": ["Hole radius", "mm"]
    },
    analysis: {
        "a": ["Angle", "°"],
        "n": entity=> {
            switch (entity.distribution) {
                case '3d':
                    return ["Circle count", ""];
                default:
                    return ["Ray count", ""];
            }
        }
    }
};

const getName = (varData, entity) => typeof varData === 'function' ? varData(entity) : varData;

export default {
    component: (varId, entity) => getName(data.component[varId.replace(/^[a-z]_/, '')], entity),
    analysis: (varId, entity) => getName(data.analysis[varId], entity)
}
