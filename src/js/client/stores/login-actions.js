/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

import net from "../net";
import actionTypes from "./action-types";

function login(user) {
    return {
        type: actionTypes.UPDATE_LOGIN,
        user: user
    };
}

export function updateLoginStatus() {
    return function (dispatch) {
        return net.get('/api/profile')
            .then(function (data) {
                return dispatch(login(data && data.user || null));
            });
    };
}

export function openGoogleLogin() {
    window.location.href = '/auth/google?backto=' + encodeURIComponent(window.location.pathname + window.location.search);
}


export function adminTokenLogin() {
    return function (dispatch) {
        return net.put('/auth/token', {token: 'admin'})
            .then(function (data) {
                return dispatch(login(data && data.user || null));
            });
    };
}

export function logout() {
    return function (dispatch) {
        return net.post('/auth/logout')
            .then(function () {
                return dispatch(login(null));
            });
    };
}
