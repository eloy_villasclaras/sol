'use strict';

import getMuiTheme from "material-ui/styles/getMuiTheme";

const theme = getMuiTheme({});

export default theme;

export const menuIconSettings = {
    style: {
        padding: '0px',
        height: '36px',
        width: '36px'
    }
};

export const treeIconMenuSettings = {
    iconStyle: {
        padding: '0px',
        width: '24px',
        height: '24px'
    },
    desktop: true
};

export const treeIconButtonSettings = {
    style: {
        padding: '6px',
        width: '36px',
        height: '36px'
    }
};

export const selectedTreeIconButtonSettings = {
    style: treeIconButtonSettings.style,
    iconStyle: {
        color: theme.inkBar.backgroundColor
    }
};

export const treeTitleSettings = {
    unselected: {
        title: {
            color: 'black',
            fontWeight: 400,
            lineHeight: '32px'
        },
        icon: {
            padding: '6px',
            color: 'black'
        }
    },
    selected: {
        title: {
            color: theme.inkBar.backgroundColor,
            fontWeight: 500,
            lineHeight: '32px'
        },
        icon: {
            padding: '6px',
            color: theme.inkBar.backgroundColor
        }
    }
};
