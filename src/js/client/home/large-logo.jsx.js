/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var React = require('react');

    return React.createClass({
        render: function () {
            return (
                <div className="home-large-logo">
                    <svg height="120" width="300">
                        <defs>
                            <linearGradient id="rg" x1="0%" y1="0%" x2="100%" y2="0%">
                                <stop offset="0%" stopColor="white" stopOpacity="0"></stop>
                                <stop offset="70%" stopColor="white" stopOpacity="1"></stop>
                            </linearGradient>
                        </defs>
                        <g className="main-group">
                            <path className="sol"
                                  d="M6.76 4.84l-1.8-1.79-1.41 1.41 1.79 1.79 1.42-1.41zM4 10.5H1v2h3v-2zm9-9.95h-2V3.5h2V.55zm7.45 3.91l-1.41-1.41-1.79 1.79 1.41 1.41 1.79-1.79zm-3.21 13.7l1.79 1.8 1.41-1.41-1.8-1.79-1.4 1.4zM20 10.5v2h3v-2h-3zm-8-5c-3.31 0-6 2.69-6 6s2.69 6 6 6 6-2.69 6-6-2.69-6-6-6zm-1 16.95h2V19.5h-2v2.95zm-7.45-3.91l1.41 1.41 1.79-1.8-1.41-1.41-1.79 1.8z"/>
                            <polygon className="lens"
                                     points="-0.381,-15 -0.8,-14.25 -1.191,-13.5 -1.556,-12.75 -1.9,-12 -2.211,-11.25 -2.502,-10.5 -2.771,-9.75 -3.018,-9 -3.243,-8.25 -3.447,-7.5 -3.631,-6.75 -3.794,-6 -3.937,-5.25 -4.0606,-4.5 -4.165,-3.75 -4.25,-3 -4.316,-2.25 -4.363,-1.5 -4.391,-0.75 -4.4,0 -4.391,0.75 -4.362,1.5 -4.316,2.25 -4.25,3 -4.165,3.75 -4.061,4.5 -3.937,5.25 -3.794,6 -3.631,6.75 -3.447,7.5 -3.243,8.25 -3.018,9 -2.771,9.75 -2.502,10.5 -2.211,11.25 -1.9,12 -1.556,12.75 -1.191,13.5 -0.8,14.25 -0.381,15 0.381,15 0.8,14.25 1.191,13.5 1.556,12.75 1.895,12 2.211,11.25 2.502,10.5 2.771,9.75 3.018,9 3.243,8.25 3.447,7.5 3.631,6.75 3.794,6 3.937,5.25 4.061,4.5 4.165,3.75 4.25,3 4.316,2.25 4.362,1.5 4.391,0.75 4.4,0 4.391,-0.75 4.362,-1.5 4.316,-2.25 4.25,-3 4.165,-3.75 4.061,-4.5 3.937,-5.25 3.794,-6 3.631,-6.75 3.447,-7.5 3.243,-8.25 3.018,-9 2.771,-9.75 2.5024909927927927,-10.5 2.211,-11.25 1.895,-12 1.556,-12.75 1.191,-13.5 0.8,-14.25 0.381,-15"></polygon>
                            <g className="rays">
                                <polyline
                                    points="-25,12 -1.895454169735011,12 2.1393575646393757,11.42488695353631 35,-5.5"
                                    strokeWidth="0.11421606381159963"></polyline>
                                <polyline
                                    points="-21,6 -3.7938769133981154,6 3.8953940900997495,5.479208653602158 35,-1"
                                    strokeWidth="0.11421606381159963"></polyline>
                                <polyline points="-20,0 -4.399999999999999,0 4.399999999999999,0 35,0"
                                          strokeWidth="0.11421606381159963"></polyline>
                                <polyline
                                    points="-21,-6 -3.7938769133981154,-6 3.8953940900997495,-5.479208653602158 35,1"
                                    strokeWidth="0.11421606381159963"></polyline>
                                <polyline
                                    points="-25,-12 -1.895454169735011,-12 2.1393575646393757,-11.42488695353631 35,5.5"
                                    strokeWidth="0.11421606381159963"></polyline>
                            </g>
                            <rect fill="url(#rg)" x="65" y="0" width="10" height="30"></rect>
                        </g>
                    </svg>
                    <div className="large-logo-title">
                        <div className="l">S</div>
                        <div className="t">imple</div>
                        <div className="l">O</div>
                        <div className="t">ptics</div>
                        <div className="l">L</div>
                        <div className="t">aboratory</div>
                    </div>
                </div>
            );
        }
    });

}();
