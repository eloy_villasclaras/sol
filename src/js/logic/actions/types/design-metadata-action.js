/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var _update = function (document, action, memento, key) {
            if (typeof action[key] === 'string') {
                memento[key] = document[key];
                document[key] = action[key];
            }
        },
        _undo = function (document, action, memento, key) {
            if (typeof memento[key] === 'string') {
                document[key] = memento[key];
            }
        },

        doAction = function (document, action, memento) {
            _update(document, action, memento, 'title');
            _update(document, action, memento, 'description');
        },

        undoAction = function (document, action, memento) {
            _undo(document, action, memento, 'title');
            _undo(document, action, memento, 'description');
        };

    return {
        doAction: doAction,
        undoAction: undoAction
    };

}();
