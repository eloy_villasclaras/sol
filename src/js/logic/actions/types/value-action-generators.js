/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var reserved = ['vars', 'children'],

        _isNotReserved = function (action) {
            return reserved.indexOf(action.key) < 0;
        },

        generateValueAction = function (find) {
            return {
                doAction: function (document, action, memento) {
                    var item = find(document, action.itemId);

                    if (item && _isNotReserved(action)) {
                        memento.previous = item[action.key];
                        item[action.key] = action.value;
                    }
                },
                undoAction: function (document, action, memento) {
                    var item = find(document, action.itemId);

                    if (item && _isNotReserved(action)) {
                        if (typeof memento.previous === 'undefined') {
                            delete item[action.key];
                        } else {
                            item[action.key] = memento.previous;
                        }
                    }
                }
            };
        },

        generateVarValueAction = function (find) {
            var _var = function (document, action) {
                var item = find(document, action.itemId);
                return item && item.vars && item.vars[action.varId];
            };

            return {
                doAction: function (document, action, memento) {
                    var varObj = _var(document, action);

                    if (varObj) {
                        memento.previous = varObj[action.key];
                        varObj[action.key] = action.value;
                    }
                },

                undoAction: function (document, action, memento) {
                    var varObj = _var(document, action);

                    if (varObj) {
                        varObj[action.key] = memento.previous;
                    }
                }
            };
        };

    return {
        generateValueAction: generateValueAction,
        generateVarValueAction: generateVarValueAction
    };

}();
