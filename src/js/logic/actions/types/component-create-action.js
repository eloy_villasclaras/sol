/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var systemManager = require('../../optics/system-manager'),

        doAction = function (document, action, memento) {
            memento.itemId = systemManager.createItem(action.itemType, document.content.components, action.parent, action);
        },
        undoAction = function (document, action, memento) {
            if (memento.itemId) {
                var id = systemManager.id(action.parent, memento.itemId);
                systemManager.deleteItem(document.content.components, id);
            }
        };

    return {
        doAction: doAction,
        undoAction: undoAction
    };

}();
