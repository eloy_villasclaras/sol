/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var systemManager = require('../../optics/system-manager'),
        opticsVarManarger = require('../../optics/optics-var-manager'),

        _key = function (element, action) {
            return element.optics === 'mirror' ? 'shape' : action.face + 'Shape';
        },

        doAction = function (document, action, memento) {
            var item = systemManager.find(document.content.components, action.itemId);

            if (item) {
                memento.previous = item[_key(item, action)];
                memento.changes = opticsVarManarger.setShape(item, action.shape, action.face || null);
            }
        },

        undoAction = function (document, action, memento) {
            var item = systemManager.find(document.content.components, action.itemId);

            if (item) {
                var vars = item.vars;

                item[_key(item, action)] = memento.previous;
                memento.changes.added.forEach(function (key) {
                    delete vars[key];
                });
                Object.keys(memento.changes.deleted).forEach(function (key) {
                    vars[key] = memento.changes.deleted[key];
                });
            }
        };

    return {
        doAction: doAction,
        undoAction: undoAction
    };

}();
