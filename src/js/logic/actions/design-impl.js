/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var extend = require('extend'),

        types = extend({
                'create-component': require('./types/component-create-action'),
                'delete-component': require('./types/component-delete-action'),
                'create-component-var': require('./types/component-var-create-action'),
                'delete-component-var': require('./types/component-var-delete-action'),
                'design-metadata': require('./types/design-metadata-action'),
                'set-component-shape': require('./types/component-shape-type-action'),
                'create-analysis': require('./types/analysis-create-action'),
                'delete-analysis': require('./types/analysis-delete-action')
            },
            require('./types/component-value-actions'),
            require('./types/analysis-value-actions')
        ),

        _bind = function (name) {
            return function (document, action, memento) {
                if (types[action.type]) {
                    types[action.type][name](document, action, memento);
                }
            }
        };

    return {
        doAction: _bind('doAction'),
        undoAction: _bind('undoAction')
    };

}
();
