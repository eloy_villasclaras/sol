/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var gen1 = function (op) {

            return function (args, vars) {
                var a = exec(args[0], vars);

                return (a !== null) ? op(a) : null;
            };
        },

        gen2 = function (op) {

            return function (args, vars) {
                var a = exec(args[0], vars),
                    b = exec(args[1], vars);

                return (a !== null && b !== null) ? op(a, b) : null;
            };
        },

        ops = {
            '*': gen2(function (a, b) {
                return a * b;
            }),
            '+': gen2(function (a, b) {
                return a + b;
            }),
            '-': gen2(function (a, b) {
                return a - b;
            }),
            '/': gen2(function (a, b) {
                return a / b;
            }),
            '^': gen2(function (a, b) {
                return Math.pow(a, b);
            }),
            'neg': gen1(function (a) {
                return -a;
            })
        },


        exec = function (op, vars) {
            var type = typeof op;

            if (type === 'number') {
                return op;
            } else if (type === 'string') {
                return vars.hasOwnProperty(op) ? vars[op] : null;
            } else if (op.type in ops) {
                return ops[op.type](op.args, vars);
            } else {
                return null;
            }
        };

    return exec;

}();