/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var execOp = require('./eq-funcs'),

        groups = function (tokens) {

            var ob;
            for (ob = 0; ob < tokens.length; ob++) {
                if (tokens[ob] === '(') {
                    break;
                }
            }

            if (ob < tokens.length) {
                var oc = 0, cb;
                for (cb = ob + 1; cb < tokens.length; cb++) {
                    if (tokens[cb] === '(') {
                        oc += 1;
                    } else if (tokens[cb] === ')') {
                        if (oc === 0) {
                            break;
                        } else {
                            oc -= 1;
                        }
                    }
                }

                if (cb < tokens.length) {
                    var inner = tokens.slice(ob + 1, cb);
                    groups(inner);
                    tokens.splice(ob, cb - ob + 1, inner);
                }
            }
        },

        funcSearch = function (tokens) {
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];
                if (Array.isArray(token)) {
                    funcSearch(token);
                } else if (token.hasOwnProperty('args')) {
                    funcSearch(token.args);
                }
            }

            for (i = 0; i < tokens.length - 1; i++) {
                token = tokens[i];

                if (typeof token === 'string' && token.search(/^[_a-zA-Z]/) === 0 && Array.isArray(tokens[i + 1])) {
                    tokens.splice(i, 2, {type: token, args: tokens[i + 1]});
                }
            }
        },

        op2Search = function (tokens, op) {
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];

                if (Array.isArray(token)) {
                    op2Search(token, op);
                } else if (token.hasOwnProperty('args')) {
                    op2Search(token.args, op);
                }
            }

            for (i = 1; i < tokens.length - 1; i++) {
                token = tokens[i];

                if (token === op) {
                    var op = {type: op, args: [tokens.slice(0, i), op2Search(tokens.slice(i + 1), op)]};
                    tokens.splice(0, tokens.length, op);
                }
            }

            return tokens;
        },

        op1preSearch = function (tokens, op, alias) {
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];

                if (Array.isArray(token)) {
                    op1preSearch(token, op, alias);
                } else if (token.hasOwnProperty('args')) {
                    op1preSearch(token.args, op, alias);
                }
            }

            for (i = 0; i < tokens.length - 1; i++) {
                token = tokens[i];

                if (token === op) {
                    tokens.splice(i, 2, {type: alias || op, args: [tokens[i + 1]]});
                }
            }
        },

        simplify = function (a, isArgs) {
            for (var i = 0; i < a.length; i++) {
                var token = a[i];

                if (Array.isArray(token)) {
                    a[i] = simplify(token);
                } else if (token.hasOwnProperty('args')) {
                    token.args = simplify(token.args, true);
                }
            }

            return !isArgs && a.length === 1 ? a[0] : a;
        },

        preCalc = function (op, deps) {
            if (typeof op === 'number') {
                return op;
            } else if (typeof op === 'string') {
                if (deps.indexOf(op) < 0) {
                    deps.push(op);
                }
                return op;
            } else if (Array.isArray(op)) {
                for (var i = 0; i < op.length; i++) {
                    op[i] = preCalc(op[i], deps);
                }
                return op;
            } else if (op.hasOwnProperty('args')) {
                preCalc(op.args, deps);
                if (op.args.reduce(function (prev, v) {
                        return prev && typeof v === 'number';
                    }, true)) {
                    return execOp(op);
                } else {
                    return op;
                }
            } else {
                return op;
            }
        },

        parseNumber = function (str, units, errors) {
            var regex = /([0-9]+(\.[0-9]+)?)(\s*([a-z']+)\s*)?/g,
                match,
                accValue = 0;

            while (match = regex.exec(str)) {
                var value = parseFloat(match[1]),
                    unit = match[4];

                if (unit) {
                    if (units && unit in units) {
                        value *= units[unit];
                    } else {
                        errors.push('unknown unit ' + unit);
                        return 0;
                    }
                }

                accValue += value;
            }

            return accValue;
        },

        parse = function (txt, units) {
            var matches = txt.trim().match(/(\[[^\]]+])|(([0-9]+(\.[0-9]+)?(\s*([a-z']+)\s*)?)+)|([_a-zA-Z]+[\._a-zA-Z0-9]*)|\+|\-|\*|\(|\)|\/|\^/g)
                    .filter(function (token) {
                        return token.length > 0;
                    }),
                errors = [],
                parts = matches ? matches.map(function (token) {
                    if (token.charAt(0) === '[') {
                        return token.substr(1, token.length - 2);
                    } else if (token.match(/^[0-9]/)) {
                        return parseNumber(token, units, errors);
                    } else {
                        return token;
                    }
                }) : [],
                deps = [];

            groups(parts);
            funcSearch(parts);

            op2Search(parts, '+');
            op2Search(parts, '-');
            op2Search(parts, '*');
            op2Search(parts, '/');
            op2Search(parts, '^');

            op1preSearch(parts, '-', 'neg');

            return {
                tree: preCalc(simplify(parts), deps),
                dependencies: deps,
                error: errors.length > 0 ? errors : false
            };
        },

        complexity = function (tree) {
            if (Array.isArray(tree)) {
                return tree.reduce(function (acc, v) {
                    return acc + complexity(v);
                }, 0);
            } else if (tree.hasOwnProperty('args')) {
                return 1 + complexity(tree.args);
            } else {
                return 0;
            }
        },


        evalExp = function (exp, vars) {
            if (Array.isArray(exp.tree)) {
                if (exp.error) {
                    exp.error.push('syntax');
                } else {
                    exp.error = ['syntax'];
                }
            }

            return exp.error ? null : execOp(exp.tree, vars || {});
        },

        replaceVar = function (tree, oldName, newName) {
            if (Array.isArray(tree)) {
                for (var i = 0; i < tree.length; i++) {
                    if (typeof tree[i] === 'string') {
                        if (tree[i] === oldName) {
                            tree[i] = newName;
                        }
                    } else {
                        replaceVar(tree[i], oldName, newName);
                    }
                }
            } else if (tree.hasOwnProperty('args')) {
                replaceVar(tree.args, oldName, newName);
            }
        },

        mapVar = function (exp, oldName, newName) {
            var index = exp.dependencies.indexOf(oldName);
            if (index >= 0) {
                exp.dependencies[index] = newName;
                if (exp.tree === oldName) {
                    exp.tree = newName;
                } else {
                    replaceVar(exp.tree, oldName, newName);
                }
            }
        };

    return {
        parse: parse,
        complexity: complexity,
        eval: evalExp,
        mapVar: mapVar
    };

}();
