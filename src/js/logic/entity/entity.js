/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var entityImpl = require('./entity-impl'),
        icons = require('./icons'),

        evalVars = function (rootEntity, units) {
            var entityMap = entityImpl.mapEntities(rootEntity),
                variableMap = entityImpl.mapVariables(entityMap, units),
                originalDependencyMap = entityImpl.resolveDependencies(variableMap),
                dependencyMap = entityImpl.fullDependencies(variableMap),
                sorted = entityImpl.sortDependencies(dependencyMap),
                values = entityImpl.eval(sorted, variableMap),
                result = {};

            for (var varId in variableMap) {
                if (variableMap.hasOwnProperty(varId)) {
                    var varObj = variableMap[varId],
                        entityId = varObj.eId,
                        val = values[varId];

                    if (!result.hasOwnProperty(entityId)) {
                        result[entityId] = {};
                    }

                    result[entityId][varObj.id] = {
                        eq: varObj.eq,
                        fullId: varId,
                        dependencies: originalDependencyMap[varId],
                        fullDependencies: dependencyMap[varId],
                        val: typeof val === 'number' ? val : null
                    };

                    if (varObj.v.error) {
                        result[entityId][varObj.id].error = varObj.v.error;
                    }
                }
            }

            return result;
        },

        _addEntityText = function (result, entities, baseId, defaultVarNames) {
            for (var eid in entities) {
                if (entities.hasOwnProperty(eid)) {
                    var fullId = baseId + eid,
                        entity = entities[eid],
                        data = {
                            title: typeof entity.title === 'string' ? entity.title : fullId,
                            icon: icons.entityIconName(entity),
                            vars: {}
                        };

                    for (var vid in entity.vars) {
                        if (entity.vars.hasOwnProperty(vid)) {
                            data.vars[vid] = entity.vars[vid].hasOwnProperty('name') ?
                                [entity.vars[vid].name, entity.vars[vid].units || ''] :
                                (defaultVarNames(vid, entity) || ['', '']);
                        }
                    }

                    result[fullId] = data;

                    if (entity.children) {
                        _addEntityText(result, entity.children, fullId + '.', defaultVarNames);
                    }
                }
            }
        },

        getInfo = function (root, defaultVarNames) {
            var result = {};
            _addEntityText(result, root.children, '', defaultVarNames);
            return result;
        },

        relative = function (data, eid, vid, dependencyData) {
            var result = {},
                parts = eid.split('.'),
                fullVid = vid && (eid + '.' + vid);

            for (var i in data) {
                if (data.hasOwnProperty(i)) {
                    var p = i.split('.'),
                        n = Math.min(parts.length, p.length),
                        shared = 0,
                        rid = [],
                        varPos,
                        entityData = data[i];

                    for (; shared < n; shared++) {
                        if (parts[shared] !== p[shared]) {
                            break;
                        }
                    }

                    for (var j = shared; j < parts.length; j++) {
                        rid.push('_');
                    }

                    for (j = shared; j < p.length; j++) {
                        rid.push(p[j]);
                    }

                    varPos = rid.length;

                    for (var v in entityData.vars) {
                        if (entityData.vars.hasOwnProperty(v) && (i !== eid || v !== vid)) {
                            var varData = entityData.vars[v];
                            rid[varPos] = v;
                            result[rid.join('.')] = {
                                name: varData[0],
                                units: varData[1],
                                entity: entityData.title,
                                icon: entityData.icon,
                                dep: !!dependencyData && dependencyData[i][v].fullDependencies.indexOf(fullVid) >= 0
                            };
                        }
                    }
                }
            }

            return result;
        };

    return {
        evalVars: evalVars,
        getInfo: getInfo,
        relative: relative
    };

}();
