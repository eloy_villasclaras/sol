/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var entityIconName = function (entity) {
        switch (entity.type) {
            case 'group':
                return 'group_work';
            case 'element':
            {
                switch (entity.optics) {
                    case 'lens':
                        return 'lens'
                    case 'mirror':
                        return 'panorama_fish_eye';
                    default:
                        return false;
                }
            }
            case 'inf-point':
                return 'assessment';
            default:
                return false;
        }
    };

    return {
        entityIconName: entityIconName
    };
}();
