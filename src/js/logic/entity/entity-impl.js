/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {


    var _ = require('underscore'),

        expressions = require('../eq/eq'),

        _mapEntities = function (entity, id, map) {
            map[id] = entity;
            if (entity.hasOwnProperty('children')) {
                for (var cid in entity.children) {
                    if (entity.children.hasOwnProperty(cid)) {
                        _mapEntities(entity.children[cid], concatIds(id, cid), map);
                    }
                }
            }
            return map;
        },

        mapEntities = function (root) {
            return _mapEntities(root, '', {});
        },

        concatIds = function (a, b) {
            return (a.length > 0 ? a + '.' : '') + b;
        },

        mapVariables = function (entityMap, units) {
            var varMap = {};
            for (var id in entityMap) {
                if (entityMap.hasOwnProperty(id)) {
                    for (var varId in entityMap[id].vars) {
                        varMap[concatIds(id, varId)] = {
                            eq: entityMap[id].vars[varId].eq,
                            v: expressions.parse(entityMap[id].vars[varId].eq || '0', units),
                            eId: id,
                            id: varId
                        };
                    }
                }
            }
            return varMap;
        },

        resolveDependencies = function (varMap) {
            var originalDeps = {};
            for (var varId in varMap) {
                if (varMap.hasOwnProperty(varId)) {
                    var v = varMap[varId],
                        deps = v.v.dependencies;

                    originalDeps[varId] = deps.slice();

                    for (var i = 0; i < deps.length; i++) {
                        var original = deps[i],
                            depId = original,
                            baseId = v.eId;

                        while (depId.search(/^_\./) >= 0 && baseId.length > 0) {
                            var pp = baseId.lastIndexOf('.');
                            baseId = pp >= 0 ? baseId.substr(0, pp) : '';
                            depId = depId.substr(2);
                        }

                        depId = concatIds(baseId, depId);

                        if (depId !== original) {
                            expressions.mapVar(v.v, original, depId);
                        }
                    }
                }
            }

            return originalDeps;
        },

        _fullDependencies = function (varMap, id, depMap) {
            if (depMap.hasOwnProperty(id)) {
                return depMap[id];
            } else if (varMap.hasOwnProperty(id)) {
                var varObj = varMap[id].v,
                    deps = varObj.dependencies,
                    fullDeps = [];

                depMap[id] = 'solving';

                for (var i = 0; i < deps.length; i++) {
                    var dep = deps[i],
                        depDeps = _fullDependencies(varMap, dep, depMap),
                        error = false;

                    if (fullDeps.indexOf(dep) < 0) {
                        fullDeps.push(dep);
                    }

                    if (depDeps === 'solving') {
                        error = 'circular';
                    } else if (typeof depDeps === 'string') {
                        error = depDeps;
                    } else {
                        fullDeps = _.union(fullDeps, depDeps);
                    }

                    if (error) {
                        if (!varObj.error) {
                            varObj.error = [error];
                        } else {
                            varObj.error.push(error);
                        }
                    }

                }

                depMap[id] = fullDeps;
                return fullDeps;
            } else {
                return "unknown " + id;
            }

        },

        fullDependencies = function (varMap) {
            var depMap = {};
            for (var id in varMap) {
                if (varMap.hasOwnProperty(id)) {
                    _fullDependencies(varMap, id, depMap);
                }
            }
            return depMap;
        },

        _depsOk = function (deps, sorted) {
            for (let i = 0; i < deps.length; i++) {
                if (!sorted[deps[i]]) {
                    return false;
                }
            }
            return true;
        },

        sortDependencies = function (dependencyMap) {
            let sorted = {};
            let vars = Object.keys(dependencyMap);
            let output = [];
            let goOn = true;

            while (goOn && vars.length > 0) {
                goOn = false;
                for (let i = vars.length - 1; i >= 0; i--) {
                    let v = vars[i];
                    let deps = dependencyMap[v];
                    if (deps === 'circular' || _depsOk(deps, sorted)) {
                        output.push(v);
                        vars.splice(i, 1);
                        sorted[v] = true;
                        goOn = true;
                    }
                }
            }

            return output;
        },

        evaluate = function (vars, varMap) {
            var values = {};
            for (var i = 0; i < vars.length; i++) {
                var id = vars[i];
                values[id] = expressions.eval(varMap[id].v, values);
            }
            return values;
        };

    return {
        mapEntities: mapEntities,
        mapVariables: mapVariables,
        resolveDependencies: resolveDependencies,
        fullDependencies: fullDependencies,
        sortDependencies: sortDependencies,
        eval: evaluate
    };

}();
