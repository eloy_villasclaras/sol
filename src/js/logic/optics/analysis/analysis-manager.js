/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    var clone = require('clone'),
        extend = require('extend'),

        defaultAnalysis = {
            type: 'inf-point',
            distribution: '2d',
            entrance: null,
            vars: {a: {eq: '0'}, n: {eq: '1'}}
        },

        findAnalysis = function (root, id) {
            return root.children[id];
        },

        createAnalysis = function (root) {
            var keys = Object.keys(root.children).map(function (key) {
                    return key.substr(1);
                }),
                maxId = keys.length > 0 ? Math.max.apply(null, keys) : 0,
                last = maxId > 0 && root.children['a' + maxId],
                nextIdN = (1 + maxId).toString(),
                nextId = 'a' + nextIdN,
                item = extend(clone(last || defaultAnalysis), {
                    title: nextIdN
                });

            root.children[nextId] = item;
            return nextId;
        },

        deleteAnalysis = function (root, id) {
            var item = root.children[id];
            delete root.children[id];

            return item;
        };

    return {
        findAnalysis: findAnalysis,
        createAnalysis: createAnalysis,
        deleteAnalysis: deleteAnalysis
    };

}();
