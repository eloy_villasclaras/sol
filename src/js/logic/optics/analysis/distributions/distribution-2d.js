/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var Distribution2D = function (item, firstElement) {
        this.item = item;
        this.firstElement = firstElement;

        this.n = this.item.n;
        this.i = 0;
        this.gap = (2 * this.firstElement.hap) / this.n;
        this.dy0 = .5 * this.gap - this.firstElement.hap;
    };

    Distribution2D.prototype.hasNext = function () {
        return this.i < this.n;
    };

    Distribution2D.prototype.next = function () {
        return [this.dy0 + this.gap * this.i++, 0];
    };

    return Distribution2D;
}();
