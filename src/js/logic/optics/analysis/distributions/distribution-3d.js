/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var Distribution3D = function (item, firstElement) {
        this.item = item;
        this.firstElement = firstElement;

        this.cn = this.item.n;
        this.ci = -1;

        this.gap = (2 * this.firstElement.hap) / (2 * this.cn - 1);

        this._nextCircle();
    };

    Distribution3D.prototype._nextCircle = function () {
        this.ci++;
        if (this.ci < this.cn) {
            this.pi = 0;
            this.pn = Math.round(Math.PI * this.ci) * 2 || 1;
        }
    };

    Distribution3D.prototype.hasNext = function () {
        return this.ci < this.cn;
    };

    Distribution3D.prototype.next = function () {
        var r = this.ci * this.gap;
        var a = 2 * Math.PI * this.pi / this.pn;

        if (++this.pi >= this.pn) {
            this._nextCircle();
        }

        return [r * Math.cos(a), r * Math.sin(a)];
    };

    return Distribution3D;
}();
