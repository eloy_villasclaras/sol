/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var validation = require('../validation'),
        simulatorImpl = require('../simulator/simulator-impl'),
        RayIterator = require('./ray-iterator'),

        analyze = function (root, vars, elementVars) {
            var items = {},
                elementIds = Object.keys(elementVars);


            Object.keys(root.children).forEach(function (id) {
                var analysis = root.children[id],
                    analysisVars = vars[id],
                    data = {
                        fullId: id,
                        type: analysis.type,
                        distribution: analysis.distribution,
                        entrance: analysis.entrance,
                        n: analysisVars.n.val,
                        a: analysisVars.a.val
                    };

                validation.validateVar('n', data.n, ['gt-zero', 'int'], analysisVars);

                if (!data.entrance || elementIds.indexOf(data.entrance) < 0) {
                    data.error = 'entrance';
                } else if (validation.checkErrorInChildren(analysisVars)) {
                    data.error = 'var';
                }

                items[id] = data;
            });


            return items;
        },

        simulate = function (elements, items) {
            var scene = simulatorImpl.prepare(elements),
                results = {};

            Object.keys(items).forEach(function (id) {
                var item = items[id];
                if (!item.error) {
                    var firstElement = scene.elements[item.entrance],
                        entranceId = item.entrance + '.' + (firstElement.optics === 'lens' ? 'front' : 'mirror'),
                        gap = (2 * firstElement.hap) / item.n,
                        dy0 = .5 * gap - firstElement.hap,
                        result = [];

                    for (var i = 0; i < item.n; i++) {
                        var dy = dy0 + gap * i,
                            ray = simulatorImpl.ray(scene.faces[entranceId], item.a * Math.PI / 180, 0, 2000, dy, 0),
                            path = simulatorImpl.trace(scene.faces, ray);
                        result.push(path);
                    }

                    results[id] = result;
                }
            });

            return results;
        },

        performanceNow = function () {
            return Date.now();
        },

        asyncState = {
            running: false,
            cancel: false
        },

        simulateAsync = function (elements, items, cb) {
            if (asyncState.running) {
                asyncState.cancel = function () {
                    runAsyncSimulation(elements, items, cb);
                };
            } else {
                runAsyncSimulation(elements, items, cb);
            }
        },

        runAsyncSimulation = function (elements, items, cb) {
            'sim started';
            asyncState.running = true;
            asyncState.cancel = false;

            var scene = simulatorImpl.prepare(elements),
                results = {},
                iterator = new RayIterator(items, scene),

                run = function () {
                    if (asyncState.cancel) {
                        asyncState.running = false;
                        asyncState.cancel();
                        return;
                    } else {
                        var t0 = performanceNow();

                        while (iterator.hasNext()) {
                            var next = iterator.next(),
                                path = simulatorImpl.trace(scene.faces, next.ray);

                            if (!results.hasOwnProperty(next.id)) {
                                results[next.id] = [path];
                            } else {
                                results[next.id].push(path);
                            }

                            if (performanceNow() > t0 + 40 && iterator.hasNext()) {
                                setTimeout(run, 0);
                                return;
                            }
                        }

                        asyncState.running = false;
                        if (asyncState.cancel) {
                            asyncState.cancel();
                        }
                        cb(results);
                    }
                };

            setTimeout(run, 0);
        };

    return {
        analyze: analyze,
        simulate: simulate,
        simulateAsync: simulateAsync
    };

}();
