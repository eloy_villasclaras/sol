/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simulatorImpl = require('../simulator/simulator-impl'),
        distributions = {
            "2d": require('./distributions/distribution-2d'),
            "3d": require('./distributions/distribution-3d')
        },

        RayIterator = function (items, scene) {
            this.items = items;
            this.scene = scene;
            this.i = -1;
            this.ids = Object.keys(this.items);
            this.in = this.ids.length;
            this.current = {};

            this._loadNextItem();
        };

    RayIterator.prototype._loadNextItem = function () {
        this.i++;

        while (this.i < this.in) {
            this.current.id = this.ids[this.i];
            this.current.item = this.items[this.current.id];

            if (!this.current.item.error && this.current.item.distribution in distributions) {
                this.current.firstElement = this.scene.elements[this.current.item.entrance];
                this.current.rays = new distributions[this.current.item.distribution](this.current.item, this.current.firstElement);
                if (this.current.rays.hasNext()) {
                    this.current.entranceId = this.current.item.entrance + '.' + (this.current.firstElement.optics === 'lens' ? 'front' : 'mirror');
                    this.current.result = [];
                    break;
                }
            }

            this.i++;
        }
    };

    RayIterator.prototype.hasNext = function () {
        return this.i < this.in;
    };

    RayIterator.prototype.next = function () {
        var dpos = this.current.rays.next(),
            result = {
                id: this.current.id,
                ray: simulatorImpl.ray(this.scene.faces[this.current.entranceId], this.current.item.a * Math.PI / 180, 0, 2000, dpos[0], dpos[1])
            };

        if (!this.current.rays.hasNext()) {
            this._loadNextItem();
        }

        return result;
    };

    return RayIterator;
}();
