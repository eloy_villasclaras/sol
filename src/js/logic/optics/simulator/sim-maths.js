/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {

    var simParams = require('./sim-params'),
        solveEq2 = require('./eq2'),
        solveEq4 = require('./eq4'),

        a2r = function (line, pos) {
            var x = line[0] - pos.x,
                y = line[1] - pos.y;

            // zr = z
            // xr = x*cos(a) + y*sin(a)
            // yr = y*cos(a) - x*sin(a)
            return [
                x * pos.cosRot + y * pos.sinRot,
                y * pos.cosRot - x * pos.sinRot,
                line[2],
                line[3] * pos.cosRot + line[4] * pos.sinRot,
                line[4] * pos.cosRot - line[3] * pos.sinRot,
                line[5]
            ];
        },

        r2a = function (line, pos) {
            // zr = z
            // xr = x*cos(a) + y*sin(a)
            // yr = y*cos(a) - x*sin(a)

            return [
                pos.x + line[0] * pos.cosRot - line[1] * pos.sinRot,
                pos.y + line[1] * pos.cosRot + line[0] * pos.sinRot,
                line[2],
                line[3] * pos.cosRot - line[4] * pos.sinRot,
                line[4] * pos.cosRot + line[3] * pos.sinRot,
                line[5]
            ];
        },

        eq2SortedGE0 = function (a, b, c) {
            var roots = solveEq2(a, b, c),
                filtered = [];
            for (var i = 0; i < roots.length; i++) {
                if (roots[i] > simParams.tolerance) {
                    filtered.push(roots[i]);
                }
            }
            return filtered;
        },

        eq4SortedGE0 = function (a, b, c, d, e) {
            var roots = solveEq4(a, b, c, d, e),
                filtered = [];
            for (var i = 0; i < roots.length; i++) {
                if (roots[i][0] > simParams.tolerance && roots[i][1] === 0) {
                    filtered.push(roots[i][0]);
                }
            }
            filtered.sort();
            return filtered;
        },

        intersect = function (eq1, eq2) {
            var length = Math.max(eq1.length, eq2.length);
            while (length > eq1.length) {
                eq1.unshift(0);
            }
            while (length > eq2.length) {
                eq2.unshift(0);
            }

            switch (length) {
                case 3:
                {
                    return solveEq2()
                }
            }
        },

        collisionPoint = function (shape, ray, t, hap, hr) {
            var pos = [
                ray[0] + t * ray[3],
                ray[1] + t * ray[4],
                ray[2] + t * ray[5]
            ];

            if (hap === false) {
                return pos;
            } else {
                var r2 = pos[1] * pos[1] + pos[2] * pos[2];
                return r2 <= hap * hap && r2 >= hr * hr ? pos : false;
            }
        };

    return {
        r2a: r2a,
        a2r: a2r,
        solveEq2: solveEq2,
        intersect: intersect,
        eq2SortedGE0: eq2SortedGE0,
        eq4SortedGE0: eq4SortedGE0,
        collisionPoint: collisionPoint
    }
}();
