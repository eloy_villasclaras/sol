/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var clone = require('clone'),
        shapeOps = require('../shape-ops'),
        simMaths = require('./sim-maths'),
        collisionGroups = require('./box-collisions'),
        simParams = require('./sim-params'),
        resolveCollisions = require('./resolve-collisions'),

        _prepareShape = function (result, data, shape, id, suffix, basePos, dx, isBack) {
            var xs = shapeOps.xMax(shape, data.hap, 0),
                x0 = shapeOps.simX0(shape, 0);

            data.elementId = id;
            data.shape = shape;
            data.isBack = isBack;
            data.pos = basePos;
            data.pos.x += (x0 + dx) * basePos.cosRot;
            data.pos.y += (x0 + dx) * basePos.sinRot;
            data.bb = [xs[0] - x0, -data.hap, xs[1] - x0, data.hap];

            result[id + '.' + suffix] = data;
        },

        _prepareAdd = function (result, element) {
            if (element.type === 'element') {
                var absPos = element.absPos,
                    angle = absPos.rot * Math.PI / 180,
                    id = element.fullId,
                    basePos = {
                        x: absPos.x,
                        y: absPos.y,
                        rot: angle,
                        cosRot: Math.cos(angle),
                        sinRot: Math.sin(angle)
                    },

                    data = {
                        optics: element.optics,
                        hap: element.hap
                    };

                if (element.optics === 'lens') {
                    var dx = .5 * element.th,
                        fx = shapeOps.drawPoint(element.frontShape, element.hap, -dx),
                        bx = shapeOps.drawPoint(element.backShape, element.hap, dx);

                    data.n = element.n;
                    data.v = element.v;
                    data.th = element.th;

                    _prepareShape(result.faces, clone(data), element.frontShape, id, 'front', clone(basePos), -dx, false);
                    _prepareShape(result.faces, clone(data), element.backShape, id, 'back', clone(basePos), dx, true);
                    if (bx > fx) {
                        _prepareShape(result.faces, clone(data), {
                            type: 'cylinder',
                            hs: .5 * data.th,
                        }, id, 'side', clone(basePos), .5 * (fx + bx), false);
                    }

                    data.frontShape = element.frontShape;
                    data.backShape = element.backShape;
                } else {
                    data.hr = element.hr;
                    _prepareShape(result.faces, clone(data), element.shape, id, 'mirror', clone(basePos), 0, false);
                    data.shape = element.shape;
                }

                data.pos = basePos;
                result.elements[element.fullId] = data;

            } else if (element.children) {
                for (var key in element.children) {
                    if (element.children.hasOwnProperty(key)) {
                        _prepareAdd(result, element.children[key]);
                    }
                }
            }
        },

        prepare = function (elements) {
            var result = {elements: {}, faces: {}};

            _prepareAdd(result, elements);

            return result;
        },

        ray = function (entrance, offAxisAngle, zAngle, distance, dy, dz) {
            var dx = entrance.bb ? entrance.bb[2] : 0,
                cosOAA = Math.cos(offAxisAngle),
                bx = cosOAA * Math.cos(zAngle),
                by = Math.sin(offAxisAngle),
                bz = cosOAA * Math.sin(zAngle),

                rline = [
                    dx - bx * distance,
                    dy - by * distance,
                    dz - bz * distance,
                    bx,
                    by,
                    bz
                ];

            return simMaths.r2a(rline, entrance.pos);
        },

        findCollision = function (faces, collisionGroups) {
            var firstT = Infinity,
                collisions = [];

            for (var i = 0; i < collisionGroups.length; i++) {
                var group = collisionGroups[i];
                for (var j = 0; j < group.length; j++) {
                    var boxCollision = group[j],
                        elementId = boxCollision.element,
                        ray = boxCollision.ray,
                        face = faces[elementId],
                        collision = shapeOps.checkCollision(face.shape, face.hap, face.hr || 0, ray);

                    if (collision && collision.t > simParams.tolerance) {
                        if (Math.abs(collision.t - firstT) < simParams.tolerance) {
                            collisions.push({
                                face: elementId,
                                ray: [
                                    collision.pos[0],
                                    collision.pos[1],
                                    collision.pos[2],
                                    ray[3],
                                    ray[4],
                                    ray[5]
                                ]
                            });
                        } else if (collision.t < firstT) {
                            firstT = collision.t;
                            collisions = [{
                                face: elementId,
                                ray: [
                                    collision.pos[0],
                                    collision.pos[1],
                                    collision.pos[2],
                                    ray[3],
                                    ray[4],
                                    ray[5]
                                ]
                            }];
                        }
                    }
                }

                if (collisions.length > 0) {
                    return collisions;
                }
            }

            return false;
        },

        trace = function (faces, ray) {
            var path = [[ray[0], ray[1], ray[2]]];

            while (ray) {
                var groups = collisionGroups(faces, ray),
                    collisions = findCollision(faces, groups);

                if (!collisions) {
                    path.push([
                        ray[0] + 10000 * ray[3],
                        ray[1] + 10000 * ray[4],
                        ray[2] + 10000 * ray[5]
                    ]);
                    ray = false;
                } else {
                    var nextPos = false;

                    for (var i = 0; i < collisions.length; i++) {
                        var collision = collisions[i],
                            face = faces[collision.face],
                            n = face.optics === 'lens' ? (face.isBack ? 1 / face.n : face.n) : -1,
                            position = [collision.ray[0], collision.ray[1], collision.ray[2]],
                            vray = i === 0 ? collision.ray : simMaths.a2r(ray, face.pos),
                            vector = [vray[3], vray[4], vray[5]],
                            normal = shapeOps.normal(face.shape, position),
                            newVector = resolveCollisions.solve(vector, normal, n);

                        if (newVector) {
                            ray = simMaths.r2a([
                                position[0], position[1], position[2],
                                newVector[0], newVector[1], newVector[2]
                            ], face.pos);
                            nextPos = [ray[0], ray[1], ray[2]];
                        } else {
                            ray = simMaths.r2a(collision.ray, face.pos);
                            nextPos = [ray[0], ray[1], ray[2]];
                            ray = false;
                            break;
                        }
                    }

                    if (nextPos) {
                        path.push(nextPos);
                    }
                }
            }

            return path;
        };

    return {
        prepare: prepare,
        ray: ray,
        collisionGroups: collisionGroups,
        findCollision: findCollision,
        trace: trace
    };

}();
