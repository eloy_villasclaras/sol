/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('./sim-maths'),
        simParams = require('./sim-params'),

        _copyCollisions = function (fromGroup, toGroup) {
            for (var i = 0; i < fromGroup.collisions.length; i++) {
                toGroup.collisions.push(fromGroup.collisions[i]);
            }
        },

        _bbis = function (test, line, values) {
            var fci = test[0],
                vc1i = test[1],
                vc2i = test[2],
                isMin = test[3],

                fcv = values[fci + (isMin ? 0 : 3)],
                a = line[fci],
                b = line[fci + 3];
            // a + b*t = fcv

            if (b !== 0) {
                var t = (fcv - a) / b;
                if (t > simParams.tolerance) {
                    var vc1v = line[vc1i] + line[vc1i + 3] * t,
                        vc2v = line[vc2i] + line[vc2i + 3] * t;

                    return vc1v >= values[vc1i] && vc1v <= values[vc1i + 3] &&
                        vc2v >= values[vc2i] && vc2v <= values[vc2i + 3] && t;
                }
            }

            return false;
        },

        _collideTests = [
            [0, 1, 2, false],
            [0, 1, 2, true],
            [1, 0, 2, false],
            [1, 0, 2, true],
            [2, 0, 1, false],
            [2, 0, 1, true]
        ],

        collide = function (line, elementId, element) {
            var rline = simMaths.a2r(line, element.pos),
                bb = element.bb,
                minX = bb[0],
                minYZ = bb[1],
                maxX = bb[2],
                maxYZ = bb[3],
                min = Infinity,
                max = 0,
                values = [minX, minYZ, minYZ, maxX, maxYZ, maxYZ];

            for (var i = 0; i < 6; i++) {
                var c = _bbis(_collideTests[i], rline, values);
                if (c !== false) {
                    if (c < min) {
                        min = c;
                    }
                    if (c > max) {
                        max = c;
                    }
                }
            }

            return max > 0 && {
                    enter: min,
                    exit: max,
                    col: {element: elementId, ray: rline}
                };
        },


        group = function (collisions) {
            var groups = [];

            for (var i = 0; i < collisions.length; i++) {
                var collision = collisions[i],
                    enter = collision.enter,
                    exit = collision.exit,
                    group = false;

                for (var j = 0; j < groups.length; j++) {
                    var g = groups[j];

                    if (enter <= g.exit && exit >= g.enter) {
                        group = g;
                        break;
                    }
                }

                if (group) {
                    group.collisions.push(collision.col);
                    group.enter = Math.min(group.enter, enter);
                    group.exit = Math.max(group.exit, exit);

                    if (j < groups.length - 1) {
                        var next = groups[j + 1];
                        if (group.exit >= next.enter) {
                            group.exit = next.exit;
                            _copyCollisions(next, group);
                            groups.splice(j + 1, 1);
                        }
                    }

                    if (j > 0) {
                        var previous = groups[j - 1];
                        if (group.enter <= previous.exit) {
                            previous.exit = groups.exit;
                            _copyCollisions(group, previous);
                            groups.splice(j, 1);
                        }
                    }
                } else {
                    for (j = groups.length; j > 0; j--) {
                        if (enter > groups[j - 1].enter) {
                            break;
                        }
                    }
                    if (j < groups.length) {
                        groups.splice(j, 0, {enter: enter, exit: exit, collisions: [collision.col]});
                    } else {
                        groups.push({enter: enter, exit: exit, collisions: [collision.col]});
                    }
                }
            }

            return groups.map(function (group) {
                return group.collisions;
            });
        };

    return function (elements, ray) {
        var collisions = [];
        for (var id in elements) {
            if (elements.hasOwnProperty(id)) {
                var collision = collide(ray, id, elements[id]);
                if (collision) {
                    collisions.push(collision);
                }
            }
        }
        return group(collisions);
    };
}();
