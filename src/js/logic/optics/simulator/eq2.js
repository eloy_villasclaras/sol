/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

/*
 ax2 + bx + c = 0
 (-b +- sqrt(b2 - 4 * a * c)/(2a)
 */

module.exports = function (a, b, c) {
    if (a === 0) {
        return b === 0 ? [] : [-c / b];
    } else {
        var det = b * b - 4 * a * c;
        if (det < 0) {
            return [];
        } else {
            var detr = Math.sqrt(det),
                s1 = -b + detr,
                s2 = -b - detr,
                v1 = Math.abs(a) > Math.abs(s2) ? s1 / (2 * a) : (2 * c) / s2,
                v2 = Math.abs(a) > Math.abs(s1) ? s2 / (2 * a) : (2 * c) / s1;

            return v1 < v2 ?
                [v1, v2] :
                [v2, v1];
        }
    }
};
