/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var abs = function (c) {
        return Math.sqrt(c[0] * c[0] + c[1] * c[1]);
    },

    arg = function (c) {
        return Math.atan2(c[1], c[0]);
    },

    rSqrt = function (real) {
        return real < 0 ? [0, Math.sqrt(-real)] : [Math.sqrt(real), 0.0];
    },

    cSqrt = function (c) {
        if (c[0] !== 0 && c[1] !== 0) {
            var real = c[0];
            var abs = abs(c);
            if (c[0] < 0) {
                c[0] = Math.sqrt((abs + real) / 2);
                c[1] = -Math.sqrt((abs - real) / 2);
            } else {
                c[0] = Math.sqrt((abs + real) / 2);
                c[1] = Math.sqrt((abs - real) / 2);
            }
        }

        return c;
    },

    cNeg = function (c) {
        c[0] = -c[0];
        c[1] = -c[1];
        return c;
    },

    crAdd = function (c, r) {
        c[0] += r;
        return c;
    },

    crSub = function (c, r) {
        c[0] -= r;
        return c;
    },

    ccnAdd = function (a, b) {
        return [a[0] + b[0], a[1] + b[1]];
    },
    ccnSub = function (a, b) {
        return [a[0] - b[0], a[1] - b[1]];
    },

    ccSub = function (a, b) {
        a[0] -= b[0];
        a[1] -= b[1];
        return a;
    },

    crnSub = function (c, r) {
        return [c[0] - r, c[1]];
    },

    crPow = function (c, r) {
        if (c[0] !== 0 || c[1] === 0) {
            var logAbs = Math.log(abs(c));
            var arg = arg(c);
            var pabs = Math.exp(r * logAbs);
            var parg = r * arg;

            c[0] = pabs * Math.cos(parg);
            c[1] = pabs * Math.sin(parg);
        }

        return c;
    },

    crMul = function (c, r) {
        c[0] *= r;
        c[1] *= r;
        return c;
    },

    crnMul = function (c, r) {
        return [c[0] * r, c[1] * r];
    },

    ccDiv = function (a, b) {
        var denom = b[0] * b[0] + b[1] * b[1];
        var a0 = (a[0] * b[0] + a[1] * b[1]) / denom;
        var a1 = (a[1] * b[0] - a[0] * b[1]) / denom;
        a[0] = a0;
        a[1] = a1;
        return a;
    },

    solve = function (a, c, d, e) {
        var alpha = c / a;
        var beta = d / a;
        var gamma = e / a;

        var P = -alpha * alpha / 12 - gamma;
        var Q = -alpha * alpha * alpha / 108 + alpha * gamma / 3 - beta * beta / 8;
        var R = crSub(rSqrt(Q * Q / 4 + P * P * P / 27), Q / 2);


        var U = crPow(R, 1 / 3);

        var y = crnSub(U, 5 / 6 * alpha);
        if (U[0] === 0) {
            crSub(y, Math.pow(Q, 1 / 3));
        } else {
            ccSub(y, ccDiv([P, 0], crMul(U, 3)));
        }

        var W = cSqrt(crAdd(crnMul(y, 2), alpha));

        var Da = crAdd(crMul(y, 2), 3 * alpha);
        var Db = ccDiv([2 * beta, 0], W);

        var D1 = cSqrt(cNeg(ccnAdd(Da, Db)));
        var D2 = cSqrt(cNeg(ccSub(Da, Db)));

        return [
            crMul(ccnAdd(W, D1), 0.5),
            crMul(ccnAdd(W, D2), -0.5),
            crMul(ccnSub(W, D1), 0.5),
            crMul(ccSub(W, D2), -0.5)
        ];
    },


    sortedPositive = function (a, c, d, e) {
        return solve(a, c, d, e)
            .filter(function (root) {
                return root[0] >= 0 && root[1] === 0;
            })
            .map(function (root) {
                return root[0];
            })
            .sort();
    };

module.exports = {
    sortedPositive: sortedPositive
};
