/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var normalize = function (a) {
            var d = a[0] * a[0] + a[1] * a[1] + a[2] * a[2];
            if (d > 0) {
                d = 1 / Math.sqrt(d);
                a[0] *= d;
                a[1] *= d;
                a[2] *= d;
            }
            return a;
        },

        cross = function (a, b) {
            return [
                a[1] * b[2] - a[2] * b[1],
                a[2] * b[0] - a[0] * b[2],
                a[0] * b[1] - a[1] * b[0]
            ];
        },

        dot = function (a, b) {
            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
        },

        solve = function (v, normal, n21) {
            var nu = normalize(normal),
                vu = normalize(v),
                pop = normalize(cross(vu, nu)),
                pip = normalize(cross(nu, pop)),
                cosIn = dot(nu, vu),
                sinIn, n, sinOut, cosOut;

            if (n21 < 0 && cosIn > 0) {
                return false;
            }

            /* always positive */
            sinIn = dot(pip, vu);
            n = cosIn < 0 ? 1 / n21 : n21;
            sinOut = n * sinIn;

            if (n < 0 || sinOut > 1) {
                cosOut = -cosIn;
                sinOut = sinIn;
            } else {
                cosOut = Math.cos(Math.asin(sinOut)) * (cosIn < 0 ? -1 : 1);
            }

            return [
                nu[0] * cosOut + pip[0] * sinOut,
                nu[1] * cosOut + pip[1] * sinOut,
                nu[2] * cosOut + pip[2] * sinOut
            ];
        };

    return {
        solve: solve
    };
}();
