/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function () {
            return false;
        },

        drawPoint = function (data, y, delta) {
            return delta;
        },

        xMax = function (data, y, delta) {
            return [delta, delta];
        },

        intersectionEq = function () {
            return false;
        },

        validation = {},

        simX0 = function (data, delta) {
            return delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var r0 = relRay[0],
                r3 = relRay[3];

            if (r3 !== 0) {
                var t = -r0 / r3;
                if (t > 0) {
                    var pos = simMaths.collisionPoint(data, relRay, t, hap, hr);
                    return pos && {
                            t: t,
                            pos: pos
                        };
                }
            }
            return false;
        },
        normal = function (data, point) {
            return [-1, 0, 0];
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };

}();
