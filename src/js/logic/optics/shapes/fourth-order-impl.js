/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function () {
            return false;
        },

    /*
     * x = D4 * (y^2 + z^2)^2 + D2 * (y^2+z^2)
     * x = D2*y^2 + D4*y^4 + delta
     * */
        drawPoint = function (data, y, delta) {
            var y2 = y * y;

            return data.d2 * y2 + data.d4 * y2 * y2 + delta;
        },

    /*
     * dx/dy = 2*D2*y + 4*D4*y^3
     * roots:+-sqt(-D2/2D4), 0
     *
     */
        xMax = function (data, y, delta) {
            var d2 = data.d2,
                d4 = data.d4,
                xs = [delta, drawPoint(data, y, delta)];

            if (d4 !== 0) {
                var a_b = d2 / d4;
                if (a_b < 0) {
                    var ymax = Math.sqrt(-0.5 * a_b);
                    if (ymax <= y) {
                        xs.push(drawPoint(data, ymax, delta));
                    }
                }
            }
            xs.sort();
            return [xs[0], xs[xs.length - 1]];
        },

        intersectionEq = function () {
            return false;
        },

        validation = {
            d4: 'non-zero',
            d2: false
        },

        simX0 = function (data, delta) {
            return delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var d2 = data.d2,
                d4 = data.d4,
                r0 = relRay[0],
                r1 = relRay[1],
                r2 = relRay[2],
                r3 = relRay[3],
                r4 = relRay[4],
                r5 = relRay[5],

                r12 = r1 * r1,
                r13 = r12 * r1,
                r14 = r13 * r1,
                r22 = r2 * r2,
                r23 = r22 * r2,
                r24 = r23 * r2,
                r42 = r4 * r4,
                r43 = r42 * r4,
                r44 = r43 * r4,
                r52 = r5 * r5,
                r53 = r52 * r5,
                r54 = r53 * r5,


                a = d4 * (r54 + 2 * r42 * r52 + r44),

                b = 4 * d4 * (r2 * r53 + r1 * r4 * r52 + r2 * r42 * r5 + r1 * r43),
                c = 2 * d4 * (r12 * r52 + r22 * r42 + 3 * (r22 * r52 + r12 * r42) + 4 * r1 * r2 * r4 * r5) + d2 * (r52 + r42),

                d = 4 * d4 * (r23 * r5 + r12 * r2 * r5 + r1 * r22 * r4 + r13 * r4) + 2 * d2 * (r2 * r5 + r1 * r4) - r3,

                e = d4 * (r24 + 2 * r12 * r22 + r14) + d2 * (r22 + r12) - r0,

                ts = simMaths.eq4SortedGE0(a, b, c, d, e);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, hap, hr),
                    x = pos[0],
                    y = pos[1],
                    z = pos[2],
                    pr2 = y * y + z * z,
                    dot = -x + (2 * (2 * d4 * pr2 + d2)) * pr2;

                if (pos && dot <= 0) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            var d2 = data.d2,
                d4 = data.d4,
                y = point[1],
                z = point[2],
                k = 2 * (2 * d4 * (y * y + z * z) + d2);

            return [-1, k * y, k * z];
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };
}();
