/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        xMax = function (data, y, delta) {
            return [-data.hs + delta, data.hs + delta];
        },

        simX0 = function () {
            return 0;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var r = hap,
                hs = data.hs,

                r1 = relRay[1],
                r2 = relRay[2],
                r4 = relRay[4],
                r5 = relRay[5],

                a = r5 * r5 + r4 * r4,
                b = 2 * (r1 * r4 + r2 * r5),
                c = r1 * r1 + r2 * r2 - r * r,

                ts = simMaths.eq2SortedGE0(a, b, c);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, false, false);

                if (pos && pos[0] >= -hs && pos[0] <= hs) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            return [0, point[1], point[2]];
        };

    return {
        xMax: xMax,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };

}();
