/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function () {
            return false;
        },

    /* (x/hsa)^2 - (y/vsa)^2 = 1 */
        drawPoint = function (data, y, delta) {
            var vsa = data.vsa,
                hsa = data.hsa,
                y_sa = y / vsa;

            return Math.sqrt(1 + y_sa * y_sa) * hsa - hsa + delta;
        },


        xMax = function (data, y, delta) {
            var x = drawPoint(data, y, delta);

            return x > delta ? [delta, x] : [x, delta];
        },

    /* y^2 = (vsa/hsa)^2*x^2 + -vsa^2 */
        intersectionEq = function (data, delta) {
            var vsa2 = data.vsa * data.vsa,
                ihsa = 1 / data.hsa,
                v2h = vsa2 * ihsa,
                v2h2 = v2h * ihsa,
                dv2h2 = delta * v2h2;

            return [v2h2, 2 * (v2h - dv2h2), delta * (dv2h2 - 2 * v2h)];
        },

        validation = {
            hsa: 'non-zero',
            vsa: 'gt-zero'
        },

        simX0 = function (data, delta) {
            return data.hsa + delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var hsa = data.hsa,
                vsa = data.vsa,
                r0 = relRay[0],
                r1 = relRay[1],
                r2 = relRay[2],
                r3 = relRay[3],
                r4 = relRay[4],
                r5 = relRay[5],

                r0_hsa = r0 / hsa,
                r1_vsa = r1 / vsa,
                r2_vsa = r2 / vsa,
                r3_hsa = r3 / hsa,
                r4_vsa = r4 / vsa,
                r5_vsa = r5 / vsa,

                a = r5_vsa * r5_vsa + r4_vsa * r4_vsa + r3_hsa * r3_hsa,
                b = 2 * (r1_vsa * r4_vsa + r2_vsa * r5_vsa + r0_hsa * r3_hsa),
                c = r1_vsa * r1_vsa + r2_vsa * r2_vsa + r0_hsa * r0_hsa - 1,

                ts = simMaths.eq2SortedGE0(a, b, c);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, hap, hr);
                
                if (pos && pos[0] * hsa <= 0) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            var k = data.hsa < 0 ? -1 : 1;
            return [k * point[0] * data.vsa * data.vsa, k * point[1] * data.hsa * data.hsa, k * point[2] * data.hsa * data.hsa];
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };
}();
