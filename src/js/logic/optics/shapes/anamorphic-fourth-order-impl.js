/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function () {
            return false;
        },

    /*
     * x = D4 * (y^2 + D4z * z^2)^2 + D2 * (y^2 + D2z * z^2)
     * x = D2*y^2 + D4*y^4 + delta
     * */
        drawPoint = function (data, y, delta) {
            var y2 = y * y;
            return data.d2 * y2 + data.d4 * y2 * y2 + delta;
        },

    /* x = D4 * (y^2 + D4z * z^2)^2 + D2 * (y^2 + D2z * z^2) */
        xsAt = function (data, y, delta, xs) {
            var y2 = y * y,
                y4 = y2 * y2,
                d2 = data.d2,
                d4 = data.d4,
                d2z = data.d2z,
                d4z = data.d4z,
                d4z2 = d4z * d4z,

                k2 = d2 * y2,
                k4 = d4 * y4;

            xs.push(k2 + k4 + delta);
            xs.push(d2z * k2 + k4 + delta);
            xs.push(k2 + d4z2 * k4 + delta);
            xs.push(d2z * k2 + d4z2 * k4 + delta);
        },

        xMax = function (data, y, delta) {
            var d2 = data.d2,
                d4 = data.d4,
                d2z = data.d2z,
                d4z = data.d4z,
                d4z2 = d4z * d4z,
                d2d2z = d2 * d2z,
                d4d4z2 = d4 * d4z2,
                xs = [delta];

            xsAt(data, y, delta, xs);

            if (d4 !== 0 && d4z !== 0 && d2 * d4 < 0) {
                var ab = [[d2, d4], [d2d2z, d4], [d2, d4d4z2], [d2d2z, d4d4z2]];
                for (var i = 0; i < 4; i++) {
                    var k = ab[i],
                        a = k[0],
                        b = k[1],
                        ymax = Math.sqrt(-0.5 * a / b),
                        y2 = ymax * ymax;

                    xs.push(b * y2 * y2 + a * y2 + delta);
                }
            }
            xs.sort();
            return [xs[0], xs[xs.length - 1]];
        },

        intersectionEq = function () {
            return false;
        },

        validation = {
            d4: 'non-zero',
            d2: false,
            d4z: 'gt-zero',
            d2z: 'gt-zero'
        },

        simX0 = function (data, delta) {
            return delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var d2 = data.d2,
                d4 = data.d4,
                d2z = data.d2z,
                d4z = data.d4z,
                d4z2 = d4z * d4z,

                r0 = relRay[0],
                r1 = relRay[1],
                r2 = relRay[2],
                r3 = relRay[3],
                r4 = relRay[4],
                r5 = relRay[5],

                r12 = r1 * r1,
                r13 = r12 * r1,
                r14 = r13 * r1,
                r22 = r2 * r2,
                r23 = r22 * r2,
                r24 = r23 * r2,
                r42 = r4 * r4,
                r43 = r42 * r4,
                r44 = r43 * r4,
                r52 = r5 * r5,
                r53 = r52 * r5,
                r54 = r53 * r5,


                a = d4 * (d4z2 * r54 + 2 * d4z * r42 * r52 + r44),

                b = 4 * d4 * (d4z2 * r2 * r53 + d4z * (r1 * r4 * r52 + r2 * r42 * r5) + r1 * r43),

                c = 2 * d4 * (d4z * (r12 * r52 + r22 * r42) + 3 * (d4z2 * r22 * r52 + r12 * r42) + 4 * d4z * r1 * r2 * r4 * r5) + d2 * (d2z * r52 + r42),

                d = 4 * d4 * (d4z2 * r23 * r5 + d4z * r12 * r2 * r5 + d4z * r1 * r22 * r4 + r13 * r4) + 2 * d2 * (d2z * r2 * r5 + r1 * r4) - r3,

                e = d4 * (d4z2 * r24 + 2 * d4z * r12 * r22 + r14) + d2 * (d2z * r22 + r12) - r0,

                ts = simMaths.eq4SortedGE0(a, b, c, d, e);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, hap, hr),
                    n = normal(data, pos),
                    dot = pos[0] * n[0] + pos[1] * n[1] + pos[2] * n[2];

                if (pos && dot <= 0) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            var d2 = data.d2,
                d4 = data.d4,
                d4z = data.d4z,
                d2z = data.d2z,
                y = point[1],
                z = point[2],
                kr2 = d4z * z * z + y * y,
                k1 = 4 * d4 * kr2;

            return [-1, y * (k1 + 2 * d2), z * (d4z * k1 + 2 * d2z * d2)];
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };
}();
