/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function () {
            return false;
        },

    /* x = y^2 / 4f */
        drawPoint = function (data, y, delta) {
            return y * y / (4 * data.f) + delta;
        },


        xMax = function (data, y, delta) {
            var x = drawPoint(data, y, delta);

            return x > delta ? [delta, x] : [x, delta];
        },

    /* y^2 = 4*f*x */
        intersectionEq = function (data, delta) {
            var f4 = 4 * data.f;
            return [0, f4, -delta * f4];
        },

        validation = {
            'f': 'non-zero'
        },

        simX0 = function (data, delta) {
            return delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var f = data.f,
                r0 = relRay[0],
                r1 = relRay[1],
                r2 = relRay[2],
                r3 = relRay[3],
                r4 = relRay[4],
                r5 = relRay[5],

                a = r5 * r5 + r4 * r4,
                b = 2 * (r1 * r4 + r2 * r5) - 4 * f * r3,
                c = r1 * r1 + r2 * r2 - 4 * f * r0,

                ts = simMaths.eq2SortedGE0(a, b, c);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, hap, hr);

                if (pos) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            var r = Math.sqrt(point[1] * point[1] + point[2] * point[2]),
                angle = Math.atan(r / (2 * data.f)),
                cosA = Math.cos(angle),
                sinA = Math.sin(angle);

            if (r > 0) {
                return [-cosA, sinA * point[1] / r, sinA * point[2] / r];
            } else {
                return [-1, 0, 0];
            }
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };

}();
