/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var simMaths = require('../simulator/sim-maths'),

        maxHalfAperture = function (data) {
            return Math.abs(data.r);
        },

    /* x^2 + y^2 = r^2 */
        drawPoint = function (data, y, delta) {
            var r = data.r,
                y_r = y / r;

            return r * (1 - Math.sqrt(1 - y_r * y_r)) + delta;
        },

        xMax = function (data, y, delta) {
            var x = drawPoint(data, y, delta);

            return x > delta ? [delta, x] : [x, delta];
        },

    /* y^2 = -1*x^2 + r^2 */
        intersectionEq = function (data, delta) {
            return [-1, 2 * (delta + data.r), -delta * (2 * data.r + delta)];
        },

        validation = {
            'r': 'non-zero'
        },

        simX0 = function (data, delta) {
            return data.r + delta;
        },

        checkCollision = function (data, hap, hr, relRay) {
            var r = data.r,
                r0 = relRay[0],
                r1 = relRay[1],
                r2 = relRay[2],
                r3 = relRay[3],
                r4 = relRay[4],
                r5 = relRay[5],

                a = r5 * r5 + r4 * r4 + r3 * r3,
                b = 2 * (r0 * r3 + r1 * r4 + r2 * r5),
                c = r0 * r0 + r1 * r1 + r2 * r2 - r * r,

                ts = simMaths.eq2SortedGE0(a, b, c);

            for (var i = 0; i < ts.length; i++) {
                var t = ts[i],
                    pos = simMaths.collisionPoint(data, relRay, t, hap, hr);

                if (pos && pos[0] * r <= 0) {
                    return {
                        t: t,
                        pos: pos
                    };
                }
            }

            return false;
        },
        normal = function (data, point) {
            var k = 1. / data.r;
            return [k * point[0], k * point[1], k * point[2]];
        };

    return {
        maxHalfAperture: maxHalfAperture,
        drawPoint: drawPoint,
        xMax: xMax,
        intersectionEq: intersectionEq,
        validation: validation,
        simX0: simX0,
        checkCollision: checkCollision,
        normal: normal
    };
}();
