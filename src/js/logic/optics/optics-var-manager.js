/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var defaultValues = {
            ap: '100',
            n: '1.5',
            v: '70',
            th: '25',
            flat: {},
            circle: {
                r: '1000'
            },
            parabola: {
                f: '1000'
            },
            ellipse: {
                vsa: '1000',
                hsa: '400'
            },
            hyperbola: {
                vsa: '1000',
                hsa: '400'
            },
            aspheric4: {
                d4: '0.001',
                d2: '0'
            },
            aspheric4am: {
                d4: '0.001',
                d2: '0',
                d4z: '1',
                d2z: '1'
            }
        },

        initOptics = function (element) {
            element.vars.ap = {eq: defaultValues.ap};

            if (element.optics === 'mirror') {
                element.shape = 'flat';
                element.vars.hr = {eq: '0'};
            } else if (element.optics === 'lens') {
                element.frontShape = 'flat';
                element.backShape = 'flat';

                element.vars.n = {eq: defaultValues.n};
                element.vars.v = {eq: defaultValues.v};
                element.vars.th = {eq: defaultValues.th};
            }
        },

        _replaceVars = function (vars, prefix, replacement) {
            var deleted = Object.keys(vars).reduce(function (d, key) {
                if (key.indexOf(prefix) === 0 && !replacement.hasOwnProperty(key)) {
                    d[key] = vars[key];
                    delete vars[key];
                }
                return d;
            }, {});

            var added = Object.keys(replacement).reduce(function (a, key) {
                if (!vars.hasOwnProperty(key)) {
                    vars[key] = replacement[key];
                    a.push(key);
                }
                return a;
            }, []);

            return {
                added: added,
                deleted: deleted
            };
        },

        _setShape = function (element, shapeKey, shape, varPrefix) {
            element[shapeKey] = shape;
            var replacement = Object.keys(defaultValues[shape]).reduce(function (r, key) {
                r[varPrefix + key] = {eq: defaultValues[shape][key]};
                return r;
            }, {});

            return _replaceVars(element.vars, varPrefix, replacement);
        },

        setShape = function (element, shape, face) {
            if (element.optics === 'mirror') {
                return _setShape(element, 'shape', shape, 'm_');
            } else if (element.optics === 'lens') {
                if (face === 'front') {
                    return _setShape(element, 'frontShape', shape, 'f_');
                } else if (face === 'back') {
                    return _setShape(element, 'backShape', shape, 'b_');
                }
            }

            return null;
        };

    return {
        initOptics: initOptics,
        setShape: setShape
    };

}();
