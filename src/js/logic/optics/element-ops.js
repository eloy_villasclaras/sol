/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var systemManager = require('./system-manager'),
        shapeOps = require('./shape-ops'),
        validation = require('./validation'),
        simMaths = require('./simulator/sim-maths'),

        _checkY2e2ndDegPol = function (data, eq, x) {
            var y2 = eq[0] * x * x + eq[1] * x + eq[2];
            if (y2 >= 0) {
                data.hap = Math.min(data.hap, Math.sqrt(y2));
            }
        },

        _checkAperture = function (data) {
            data.hap = ['shape', 'frontShape', 'backShape'].reduce(function (hap, key) {
                var shape = data[key];
                if (shape) {
                    var shapeMaxHap = shapeOps.maxHalfAperture(shape);
                    if (shapeMaxHap !== false && shapeMaxHap < hap) {
                        return shapeMaxHap;
                    }
                }
                return hap;
            }, data.hap);

            if (data.optics === 'lens' && data.frontShape && data.backShape) {
                var hth = .5 * data.th,
                    eq1 = shapeOps.intersectionEq(data.frontShape, -hth),
                    eq2 = shapeOps.intersectionEq(data.backShape, hth);

                if (eq1 && eq2) {
                    var a = eq1[0] - eq2[0],
                        b = eq1[1] - eq2[1],
                        c = eq1[2] - eq2[2],
                        roots = simMaths.solveEq2(a, b, c);


                    roots.forEach(function (x) {
                        _checkY2e2ndDegPol(data, eq1, x);
                    });
                } else if (eq1) {
                    _checkY2e2ndDegPol(data, eq1, hth);
                } else if (eq2) {
                    _checkY2e2ndDegPol(data, eq2, -hth);
                }
            }
        },

        _addShape = function (data, element, elementVars, attr, prefix) {
            if (element[attr]) {
                var pl = prefix.length,
                    shape = {
                        type: element[attr]
                    },
                    req = shapeOps.validation(shape);

                Object.keys(elementVars).filter(function (key) {
                    return key.indexOf(prefix) === 0;
                }).forEach(function (key) {
                    var shortKey = key.substr(pl),
                        value = elementVars[key].val;

                    shape[shortKey] = value;

                    validation.validateVar(key, value, req[shortKey], elementVars);
                });

                data[attr] = shape;
            }
        },


        _add = function (elements, children, parentId, vars, parentAbsPos) {
            for (var id in children) {
                if (children.hasOwnProperty(id)) {
                    var child = children[id],
                        fullId = systemManager.id(parentId, id),
                        childVars = vars[fullId],
                        pos = {
                            x: childVars.x.val,
                            y: childVars.y.val,
                            rot: childVars.rot.val
                        },
                        angle = parentAbsPos.rot * Math.PI / 180,
                        cosA = Math.cos(angle),
                        sinA = Math.sin(angle),
                        absPos = {
                            x: parentAbsPos.x + pos.x * cosA - pos.y * sinA,
                            y: parentAbsPos.y + pos.y * cosA + pos.x * sinA,
                            rot: parentAbsPos.rot + pos.rot
                        },
                        data = {
                            fullId: fullId,
                            parent: parentId,
                            type: child.type,
                            pos: pos,
                            absPos: absPos
                        };

                    if (data.type === 'element') {
                        data.optics = child.optics;
                        data.hap = .5 * childVars.ap.val;

                        validation.validateVar('ap', childVars.ap.val, 'gt-zero', childVars);

                        if (data.optics === 'mirror') {
                            _addShape(data, child, childVars, 'shape', 'm_');
                            data.hr = childVars.hr.val;

                            if (data.hr >= data.hap) {
                                if (!childVars.hr.error) {
                                    childVars.hr.error = [];
                                }
                                childVars.hr.error.push('lt-hap');
                            }
                        } else if (data.optics === 'lens') {
                            ['ap', 'n', 'v', 'th'].forEach(function (key) {
                                validation.validateVar(key, childVars[key].val, 'gt-zero', childVars);
                            });

                            data.n = childVars.n.val;
                            data.v = childVars.v.val;
                            data.th = childVars.th.val;

                            _addShape(data, child, childVars, 'frontShape', 'f_');
                            _addShape(data, child, childVars, 'backShape', 'b_');
                        }

                        _checkAperture(data);
                    }

                    if (validation.checkErrorInChildren(childVars)) {
                        data.error = 'var';
                    }

                    if (child.children) {
                        var childElements = {};
                        _add(childElements, child.children, fullId, vars, absPos);
                        data.children = childElements;

                        if (!data.error && validation.checkErrorInChildren(data.children)) {
                            data.error = 'child';
                        }
                    }


                    elements[id] = data;
                }
            }
        },

        analyze = function (root, vars) {
            var elements = {
                pos: {x: 0, y: 0, rot: 0},
                children: {}
            };

            _add(elements.children, root.children, false, vars, elements.pos);

            if (validation.checkErrorInChildren(elements.children)) {
                elements.error = 'child';
            }

            return elements;
        },

        boundingBox = function (item) {
            if (item.type === 'element') {
                var hap = item.hap;

                if (item.optics === 'mirror') {
                    var xs = shapeOps.xMax(item.shape, hap, 0);
                    return [xs[0], -hap, xs[1], hap];
                } else if (item.optics === 'lens') {
                    var hth = .5 * item.th,
                        xs1 = shapeOps.xMax(item.frontShape, hap, -hth),
                        xs2 = shapeOps.xMax(item.backShape, hap, hth);

                    return [xs1[0], -hap, xs2[1], hap];
                }
            } else {
                var bb = [0, 0, 0, 0];

                Object.keys(item.children).forEach(function (key) {
                    var child = item.children[key],
                        cbb = boundingBox(child),
                        rot = child.pos.rot * Math.PI / 180,
                        cosRot = Math.cos(rot),
                        sinRot = Math.sin(rot),
                        x1c = cbb[0] * cosRot,
                        x2c = cbb[2] * cosRot,
                        x1s = cbb[0] * sinRot,
                        x2s = cbb[2] * sinRot,
                        y1c = cbb[1] * cosRot,
                        y2c = cbb[3] * cosRot,
                        y1s = cbb[1] * sinRot,
                        y2s = cbb[3] * sinRot,
                        xs = [
                            x1c - y1s,
                            x1c - y2s,
                            x2c - y1s,
                            x2c - y2s
                        ],
                        ys = [
                            y1c + x1s,
                            y1c + x2s,
                            y2c + x1s,
                            y2c + x2s
                        ];


                    bb[0] = Math.min(bb[0], Math.min.apply(null, xs) + child.pos.x);
                    bb[1] = Math.min(bb[1], Math.min.apply(null, ys) + child.pos.y);
                    bb[2] = Math.max(bb[2], Math.max.apply(null, xs) + child.pos.x);
                    bb[3] = Math.max(bb[3], Math.max.apply(null, ys) + child.pos.y);
                });

                return bb;
            }
        };


    return {
        analyze: analyze,
        boundingBox: boundingBox
    };
}();
