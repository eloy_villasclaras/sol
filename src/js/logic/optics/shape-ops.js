/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var types = {
        flat: require('./shapes/flat-impl'),
        circle: require('./shapes/circle-impl'),
        parabola: require('./shapes/parabola-impl'),
        ellipse: require('./shapes/ellipse-impl'),
        hyperbola: require('./shapes/hyperbola-impl'),
        cylinder: require('./shapes/cylinder-impl'),
        aspheric4: require('./shapes/fourth-order-impl'),
        aspheric4am: require('./shapes/anamorphic-fourth-order-impl')
    };

    return {
        maxHalfAperture: function (data) {
            return types[data.type].maxHalfAperture(data);
        },
        drawPoint: function (data, y, delta) {
            return types[data.type].drawPoint(data, y, delta || 0);
        },
        xMax: function (data, y, delta) {
            return types[data.type].xMax(data, y, delta || 0);
        },
        intersectionEq: function (data, delta) {
            return types[data.type].intersectionEq(data, delta);
        },
        validation: function (data) {
            return types[data.type].validation;
        },
        simX0: function (data, delta) {
            return types[data.type].simX0(data, delta);
        },
        checkCollision: function (data, hap, hr, relRay) {
            return types[data.type].checkCollision(data, hap, hr, relRay);
        },
        normal: function (data, point) {
            return types[data.type].normal(data, point);
        }
    };

}();
