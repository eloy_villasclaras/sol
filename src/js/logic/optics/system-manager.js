/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    var opticsVarManager = require('./optics-var-manager'),

        _defaultVars = ['x', 'y', 'rot'],

        find = function (root, id) {
            if (typeof id !== 'string') {
                return root;
            } else {
                var segments = id.split('.'),
                    item = root;

                for (var i = 0; i < segments.length && item && item.children; i++) {
                    item = item.children[segments[i]];
                }

                return item;
            }
        },

        _defaultItem = function (type) {
            switch (type) {
                case 'element':
                    return {type: 'element', optics: null};
                case 'group':
                    return {type: 'group', children: {}};
                default:
                    return false;
            }
        },

        createItem = function (type, root, parent, data) {
            var group = find(root, parent),
                item = _defaultItem(type);

            if (item && group && group.children) {
                var container = group.children,
                    keys = Object.keys(container).map(function (key) {
                        return key.substr(1);
                    }),
                    maxId = keys.length > 0 ? Math.max.apply(null, keys) : 0,
                    nextIdN = (1 + maxId).toString(),
                    nextId = 'c' + nextIdN;

                item.title = nextIdN;

                item.vars = _defaultVars.reduce(function (vars, id) {
                    vars[id] = {eq: '0'};
                    return vars;
                }, {});

                if (type === 'element') {
                    if (data && data.optics) {
                        item.optics = data.optics;
                        opticsVarManager.initOptics(item);
                    }
                }

                container[nextId] = item;
                return nextId;
            } else {
                return false;
            }
        },

        deleteItem = function (root, id) {
            var ids = splitParentChildId(id);
            if (ids) {
                var parent = find(root, ids[0]);

                if (parent && parent.children) {
                    var item = parent.children[ids[1]];
                    delete parent.children[ids[1]];

                    return item;
                }
            }

            return false;
        },

        id = function () {
            return Array.prototype.join.call(Array.prototype.filter.call(arguments, function (segment) {
                return !!segment;
            }), '.');
        },

        parentLevel = function (child, parent) {
            return child !== parent && child.indexOf(parent) === 0 && (child.substr(parent.length).match(/\./g) || []).length;
        },

        splitParentChildId = function (id) {
            if (typeof id === 'string') {
                var segments = id.split('.'),
                    parentId = segments.length > 1 ? segments.slice(0, segments.length - 1).join('.') : false,
                    childId = segments[segments.length - 1];

                return [parentId, childId];
            }

            return false;
        },

        createVar = function (root, id, varId) {
            var item = find(root, id);
            if (item && item.vars) {
                item.vars[varId] = {eq: '0'};
                return true;
            } else {
                return false;
            }
        },

        deleteVar = function (root, id, varId) {
            if (_defaultVars.indexOf(varId) < 0) {
                var item = find(root, id);
                if (item && item.vars) {
                    var deletedVar = item.vars[varId];
                    delete item.vars[varId];
                    return deletedVar;
                }
            }
            return false;
        };


    return {
        find: find,
        id: id,
        parentLevel: parentLevel,
        splitParentChildId: splitParentChildId,
        createItem: createItem,
        deleteItem: deleteItem,
        createVar: createVar,
        deleteVar: deleteVar
    };

}();
