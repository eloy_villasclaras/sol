/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function (app) {
    'use strict';

    var path = require('path');

    app.get('/pages/*', function (req, res) {
        res.sendFile(path.join(__dirname, '../../../html/pages/', req.params[0] + '.md'), function (err) {
            if (err) {
                res.sendFile(path.join(__dirname, '../../../html/pages/', req.params[0], 'index.md'));
            }
        });
    });

    app.get('/images/*', function (req, res) {
        res.sendFile(path.join(__dirname, '../../../html/pages/', req.params[0]));
    });

    app.get('*', function (req, res) {
        res.sendFile(path.join(__dirname, '../../../html/index.html'));
    });
};


