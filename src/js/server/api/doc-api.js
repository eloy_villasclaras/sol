/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function (app) {
    'use strict';


    var userOps = require('./../user-ops'),
        enums = require('../../model/enums'),

        resolve = function (res, promise) {
            promise
                .then(function (data) {
                    res.json(data);
                })
                .catch(function (err) {
                    var status;

                    switch (err.name) {
                        case 'NoAccessError':
                            status = 401;
                            break;
                        case 'NotFoundError':
                            status = 404;
                            break;
                        default:
                            status = 500;
                            break;
                    }

                    res.status(status).send(err.message);
                });
        };


    app.get('/api/documents', function (req, res) {
        resolve(res, userOps.listDocuments(req.user, req.body));
    });

    app.post('/api/documents', function (req, res) {
        resolve(res, userOps.createDocument(req.user, req.body));
    });

    app.get('/api/documents/:docId', function (req, res) {
        resolve(res, userOps.retrieveDocument(req.user, parseInt(req.params.docId)));
    });


    app.patch('/api/documents/:docId', function (req, res) {
        resolve(res, userOps.updateDocument(req.user, parseInt(req.params.docId), req.body));
    });

    app.put('/api/documents/:docId', function (req, res) {
        resolve(res, userOps.updateDocument(req.user, parseInt(req.params.docId), req.body));
    });

    app.post('/api/documents/:docId/like', function (req, res) {
        resolve(res, userOps.reviewDocument(req.user, parseInt(req.params.docId), enums.review.LIKE));
    });

    app.post('/api/documents/:docId/unlike', function (req, res) {
        resolve(res, userOps.reviewDocument(req.user, parseInt(req.params.docId), enums.review.NEUTRAL));
    });

};


