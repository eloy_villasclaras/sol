/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function (app, settings) {
    'use strict';

    var passport = require('passport'),

        db = require('../db');


    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        db.findUserById(id)
            .then(function (user) {
                done(null, user);
            });
    });

    if (settings.strategy.token) {
        var UniqueTokenStrategy = require('passport-unique-token').Strategy;
        passport.use(new UniqueTokenStrategy(function (token, done) {
            db.findUser(token)
                .then(function (user) {
                    return done(user ? null : 'not found', user);
                });
        }));
    }
    if (settings.strategy.hasOwnProperty('google')) {
        var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

        passport.use(new GoogleStrategy({
                clientID: settings.strategy.google.clientId,
                clientSecret: settings.strategy.google.secret,
                callbackURL: settings.strategy.google.redirectUrl
            },
            function (accessToken, refreshToken, profile, done) {
                var email = profile.emails.reduce(function (prev, obj) {
                    return prev === null || obj.type === 'account' ? obj.value : prev;
                }, null);

                return db.findOrCreateUser('googleId', profile.id, email)
                    .then(function (user) {
                        return done(null, user);
                    });
            }
        ));
    }

    app.use(passport.initialize());
    app.use(passport.session());


    app.post('/auth/logout', function (req, res) {
        req.logout();
        res.json({user: null});
    });

    app.get('/auth/logoutb', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    if (settings.strategy.token) {
        app.put('/auth/token', passport.authenticate('token'), function (req, res, next) {
            if (!req.user) {
                res.status(401).json({message: "Incorrect token credentials"});
            } else {
                res.json({user: req.user});
            }

            next();
        });
    }
    if (settings.strategy.google) {
        app.get('/auth/google',
            function (req, res, next) {
                req.session.onloginpath = req.query.backto;
                next();
            },
            passport.authenticate('google', {
                scope: [
                    'https://www.googleapis.com/auth/plus.login',
                    'https://www.googleapis.com/auth/plus.profile.emails.read'
                ]
            })
        );

        app.get('/auth/google/callback',
            passport.authenticate('google', {failureRedirect: '/login'}),
            function (req, res) {
                res.redirect(req.session.onloginpath || '/');
            });
    }

};
