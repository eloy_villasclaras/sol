/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var Promise = require("bluebird"),
        Knex = require('knex'),

        enums = require('../model/enums'),

        errors = require('./errors'),

        NotFoundError = errors.NotFoundError,

        knex,

        init = function (knexSettings) {
            this.knex = knex = Knex(knexSettings);
            return this;
        },

        findUser = function (username) {
            return knex('user')
                .where('username', username)
                .then(function (users) {
                    return Promise.resolve(users.length === 1 ? users[0] : null);
                });
        },

        findOrCreateUser = function (idType, id, email) {
            return knex
                .select('id')
                .from('user')
                .where(idType, id)
                .then(function (users) {
                    if (users.length > 0) {
                        return Promise.resolve(users[0]);
                    } else {
                        var attempt = 0,
                            username = email.match(/^([^@]*)@/)[1],
                            test = function () {
                                var testUsername = username + (attempt > 0 ? '_' + attempt : '');
                                return knex('user')
                                    .where('username', testUsername)
                                    .then(function (users) {
                                        if (users.length > 0) {
                                            attempt += 1;
                                            return test();
                                        } else {
                                            var user = {
                                                username: testUsername,
                                                email: email
                                            };
                                            user[idType] = id;
                                            return knex('user')
                                                .insert(user, "id")
                                                .then(function (ids) {
                                                    return findUserById(parseInt(ids[0]));
                                                });
                                        }
                                    })
                            };

                        return test();
                    }
                });
        },

        findUserById = function (id) {
            return knex
                .select('*')
                .from('user')
                .where('id', id)
                .then(function (users) {
                    return Promise.resolve(users.length === 1 ? users[0] : null);
                });
        },

        findDocument = function (id) {
            return knex
                .select('*')
                .from('document')
                .where('id', id)
                .then(function (documents) {
                    return Promise.resolve(documents.length === 1 ? documents[0] : null);
                });
        },

        getDocumentOwner = function (docId) {
            return Document
                .query()
                .where('id', docId)
                .then(function (documents) {
                    return documents.length === 1 ? Promise.resolve(documents[0].ownerId) :
                        Promise.reject(new NotFoundError('Document not found'));
                });
        },

        document2client = function (id, user) {
            var result = {};
            return knex.table('document')
                .first('*')
                .where('id', id)
                .then(function (doc) {
                    result.id = id;
                    result.ownerId = doc.ownerId;

                    return knex.table('version')
                        .first('*')
                        .where('documentId', id)
                        .orderBy('vid', 'desc')
                        .then(function (version) {
                            result.title = version.title;
                            result.description = version.description;
                            result.content = version.content;
                            result.vid = version.vid;
                            result.mv = version.mv;

                            return knex.select('userId', 'value')
                                .from('review')
                                .where('documentId', id);
                        })
                        .then(function (reviews) {
                            var reports = user && user.isAdmin,
                                mine = user && user.id !== result.ownerId,
                                count = reviews.reduce(function (count, review) {
                                    if (review.value === enums.review.LIKE) {
                                        count[0]++;
                                    } else if (reports && review.value === enums.review.REPORT) {
                                        count[1]++;
                                    }

                                    if (mine && user.id === review.userId) {
                                        count[2] = review.value;
                                    }
                                    return count;
                                }, [0, 0, 0]);

                            result.reviews = {likes: count[0]};

                            if (reports) {
                                result.reviews.reports = count[1];
                            }

                            if (mine) {
                                result.reviews.mine = count[2];
                            }

                            return Promise.resolve(result);
                        });
                });
        },

        updateDocument = function (id, user, data) {
            return knex
                .select('*')
                .from('version')
                .where('documentId', id)
                .orderBy('vid', 'desc')
                .limit(1)
                .then(function (versions) {
                    var version = versions.length > 0 ? versions[0] : {vid: 0};
                    return knex('version')
                        .insert({
                            vid: parseInt(version.vid) + 1,
                            documentId: id,
                            title: data.title || version.title || ('' + id),
                            description: data.description || version.description || '',
                            content: JSON.stringify(data.content || version.content || {})
                        }, 'id');
                })
                .then(function () {
                    return document2client(id, user);
                });
        },

        createDocumentByUsername = function (username, data) {
            return findUser(username)
                .then(function (user) {
                    if (user) {
                        return knex('document')
                            .insert({ownerId: user.id, status: data.status}, 'id')
                            .then(function (ids) {
                                return updateDocument(parseInt(ids[0]), user.id, data);
                            });
                    } else {
                        return Promise.reject(new NotFoundError('Username not found'));
                    }
                });
        },

        createDocument = function (user, data) {
            return knex('document')
                .insert({ownerId: user.id}, 'id')
                .then(function (ids) {
                    return updateDocument(parseInt(ids[0]), user, data);
                });
        },

        reviewDocument = function (user, docId, value) {
            var query = knex('review')
                .where('userId', user.id)
                .andWhere('documentId', docId);

            return (
                value ?
                    query.update('value', value).then(function (count) {
                        return count > 0 ? Promise.resolve() :
                            knex('review').insert({
                                userId: user.id,
                                documentId: docId,
                                value: value
                            }, 'id');
                    }) :
                    query.del()
            )
                .then(function () {
                    return document2client(docId, user);
                });
        },

        listDocuments = function (user) {
            var query = knex.select('id').from('document');

            if (!user || !user.isAdmin) {
                query = query.where('status', enums.status.PUBLIC);
                if (user) {
                    query = query.orWhere('ownerId', user.id);
                }
            }

            return query.then(function (docIds) {
                return docIds.reduce(function (promise, id) {
                    return promise.then(function (documents) {
                        return document2client(id.id, user).then(function (document) {
                            documents.push(document);
                            return Promise.resolve(documents);
                        });
                    });

                }, Promise.resolve([]));
            });
        };

    return {
        init: init,
        findUser: findUser,
        findOrCreateUser: findOrCreateUser,
        findUserById: findUserById,
        getDocumentOwner: getDocumentOwner,
        createDocumentByUsername: createDocumentByUsername,
        createDocument: createDocument,
        updateDocument: updateDocument,
        reviewDocument: reviewDocument,
        findDocument: findDocument,
        document2client: document2client,
        listDocuments: listDocuments
    };

}();
