/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var Promise = require('bluebird'),

        db = require('./db'),
        enums = require('../model/enums'),
        permissions = require('../model/permissions'),

        errors = require('./errors'),
        NotFoundError = errors.NotFoundError,
        NoAccessError = errors.NoAccessError,

        permission = function (user, action, document) {
            var err = permissions.check(user, action, document);
            return err ? Promise.reject(new NoAccessError(err)) : Promise.resolve(document || null);
        },

        documentPermission = function (user, action, documentId) {
            return db.findDocument(documentId)
                .then(function (document) {
                    if (document === null) {
                        return Promise.reject(NotFoundError('Document not found'));
                    } else {
                        return permission(user, action, document);
                    }
                });
        },

        retrieveDocument = function (user, docId) {
            return documentPermission(user, 'fetch-doc', docId)
                .then(function () {
                    return db.document2client(docId, user);
                });
        },

        createDocument = function (user, data) {
            return permission(user, 'create-doc')
                .then(function () {
                    return db.createDocument(user, {
                        title: data.title,
                        description: data.description,
                        content: data.content || {
                            components: {type: 'root', children: {}},
                            analysis: {children: {}}
                        }
                    });
                });
        },

        reviewDocument = function (user, id, value) {
            return documentPermission(user, 'review-doc', id)
                .then(function () {
                    return db.reviewDocument(user, id, value);
                });
        },

        updateDocument = function (user, id, data) {
            return documentPermission(user, 'update-doc', id)
                .then(function () {
                    return db.updateDocument(id, user, data);
                });
        },

        listDocuments = function (user) {
            return db.listDocuments(user);
        };

    return {
        createDocument: createDocument,
        retrieveDocument: retrieveDocument,
        updateDocument: updateDocument,
        reviewDocument: reviewDocument,
        listDocuments: listDocuments
    };
}();
