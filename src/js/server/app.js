/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function (mode, settings) {
    var express = require('express'),
        cookieParser = require('cookie-parser'),
        bodyParser = require('body-parser'),
        expressSession = require('express-session'),
        favicon = require('serve-favicon'),
        KnexSessionStore = require('connect-session-knex')(expressSession),
        path = require('path'),
        logger = require("morgan"),

        registerApi = require('./api/api'),

        db = require('./db').init(settings.knex),
        sessionStore = new KnexSessionStore({knex: db.knex, tableName: 'sessions'}),
        app = express();

    app.use(logger("dev"));

    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(expressSession({
        secret: 'some secret',
        resave: false,
        saveUninitialized: false,
        store: sessionStore
    }));
    app.use(express.static(path.join(__dirname, settings.staticPath)));
    app.use(favicon(path.join(__dirname, '../../html/favicon.ico')));

    registerApi(app, settings);

    app.use(function (err, req, res, next) {
        if (err) {
            res.status(err.statusCode || err.status || 500).send(err.data || err.message || {});
        } else {
            next();
        }
    });

    return app;
};
