/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var enums = require('./enums'),

        permissionType = function (user, type, document) {
            if (!user) {
                if (type === 'read-access' && document.status === enums.status.PUBLIC) {
                    return false;
                } else {
                    return 'You are not logged in';
                }
            } else {
                switch (type) {
                    case 'active':
                        return user.isBlocked && 'Your profile is not active';
                    case 'logged':
                        return false;
                    case 'read-access':
                        return !user.isAdmin && user.id !== document.ownerId && document.status !== enums.status.PUBLIC && 'You are the owner of the document';
                    case 'write-access':
                        return (user.isBlocked || user.id !== document.ownerId) && 'You are not the owner of the document';
                    case 'not-owner':
                        return user.id === document.ownerId && 'You are the owner of the document';
                    default:
                        return false;
                }
            }
        },

        permission = function (user, types, document) {
            var err;

            if (!types) {
                err = false;
            } else {
                if (typeof types === 'string') {
                    err = permissionType(user, types, document);
                } else {
                    err = false;
                    for (var i = 0; i < types.length && !err; i++) {
                        err = permissionType(user, types[i], document);
                    }
                }
            }

            return err;
        },

        requirements = {
            'fetch-doc': ['read-access'],
            'create-doc': ['active'],
            'review-doc': ['active', 'read-access', 'not-owner'],
            'update-doc': ['write-access']
        },

        check = function (user, action, document) {
            return permission(user, requirements[action], document);
        };

    return {
        check: check
    };

}();
