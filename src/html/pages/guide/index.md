##Guide

SOL is a simulator of optical systems; its goal is to support you in creating and analysing your own designs. 



- [Simulation](/guide/components)
- [Components](/guide/components)
- Analysis
- [Quickstart](/guide/quickstart)
- [Tutorials](/guide/tutorials)
