## Parabolic shape

![Parabolic shape](/guide/components/shape/parabolic.png)

A parabolic shape is configured by its focal length. Depending on the sign of the focal length, the shape will be 
concave or convex:

- Negative focal length: concave shape (as in the image above).
- Positive focal length: convex shape.

