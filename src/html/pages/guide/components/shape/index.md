## Surface shapes

[Lenses](/guide/components/lens) and [mirrors](/guide/components/mirror) create reflective or refractive surfaces.
The shape of each of these surfaces can be configured. Currently, only conic surfaces are supported. 

To configure a shape, you need to first select its type:

- Flat
- [Circular](/guide/components/shape/circular)
- [Parabolic](/guide/components/shape/parabolic)
- [Hyperbolic](/guide/components/shape/hyperbolic)
- [Elliptic](/guide/components/shape/elliptic)

The default shape is flat, which has no further parameters. After you select a different type, it will need 
certain attributes to be completely configured.
