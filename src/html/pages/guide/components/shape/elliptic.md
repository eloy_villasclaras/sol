## Elliptic shape

![Elliptic shape](/guide/components/shape/elliptic.png)

An elliptic shape is configured by the length of its semi-axes, similarly as in a circular shape. Often, an ellipse
is described by giving the semi-major and semi-minor axes. In our case, rather than which axis is longer, it's more 
relevant to describe the horizontal semi-axis (HSA in the image) and vertical semi-axis (VSA).

Depending on the sign of the horizontal semi-axis, the shape will be concave or convex:

- Negative HSA: concave shape (as in the image above).
- Positive HSA: convex shape.

The vertical semi-axis sets a limit to the aperture of the element: the maximum aperture is the VSA times two.