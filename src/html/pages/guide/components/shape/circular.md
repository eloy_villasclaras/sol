## Circular shape

![Circular shape](/guide/components/shape/circular.png)

A circular shape is configured by its radius. Depending on the sign of the radius, the shape will be concave or 
convex:

- Negative radius: concave shape (as in the image above).
- Positive radius: convex shape.

The radius sets a limit to the aperture of the element: the maximum aperture is the (absolute value of) the radius
times two.