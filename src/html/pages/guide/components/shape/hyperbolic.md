## Hyperbolic shape

![Hyperbolic shape](/guide/components/shape/hyperbolic.png)

A hyperbolic shape obeys the following equation: 

![Hyperbola equation](/guide/components/shape/hyperbola-eq.png)

For analogy with the [elliptic shape](/guide/components/shape/elliptic), the parameter names are horizontal semi-axis 
(HSA in the image) and vertical semi-axis (VSA).

Given the equation above, a hyperbola has two branches, a concave (as seen by light rays coming from the left) left 
branch, and a convex right branch. Depending on the sign of the horizontal semi-axis, one of these branches is chosen:

- Negative HSA: concave branch (as in the image above).
- Positive HSA: convex branch.

