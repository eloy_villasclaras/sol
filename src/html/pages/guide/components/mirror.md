## Mirrors


Mirrors model reflective elements in an optical system.

A mirror is defined by the following attributes:
                
- **Position**: The X, Y coordinates of the element, and its rotation (*[read more](/guide/components/position)*).
- **Aperture**: Mirror aperture, in mm. Please note that the mirror may sometimes have a smaller actual aperture than
requested in this field, depending on its shape. For instance, if the shape is circular, the aperture cannot be greater than the
radius.
- **Shape**: The shape of the mirror surface (*read more: [shapes](/guide/components/shape)*).
- **Inner hole**: The radius (in mm) determines the size of the hole in the middle of the mirror.

