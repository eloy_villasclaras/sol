## Positions

[Lenses](/guide/components/lens), [mirrors](/guide/components/mirror), and [groups](/guide/components/group) have
their position within the model. The position is a 2D location and a rotation.

The 2D position is determined by the **X** and **Y** variables, measured in mm. The rotation, measured in degrees,
rotates the element or group around the 2D position.

When elements are nested within groups, the element's X, Y and rotation are relative to the parent group position.
See [groups](/guide/components/group) to read more about this.
