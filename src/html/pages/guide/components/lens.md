## Lenses


Lenses model refractive elements in an optical system.

A lens is defined by the following attributes:
                
- **Position**: The X, Y coordinates of the element, and its rotation (*[read more](/guide/components/position)*).
- **Aperture**: Lens aperture, in mm. Please note that the lens may sometimes have a smaller actual aperture than
requested in this field. For instance, if one of the shapes is circular, the aperture cannot be greater than the
radius. On the other hand, if the two shapes intersect, this also creates a limit to the maximum aperture of the lens.
- **Front shape**: The shape of the front surface of the lens (*read more: [shapes](/guide/components/shape)*).
- **Back shape**: The shape of the back surface of the lens (*read more: [shapes](/guide/components/shape)*).
- **Thickness**, at center, measured in mm.
- **Refractive index** of the material.
- **Abbe number** of the material, to describe color dispersion.
