## Components

The components view is where an optical system is defined.

There are three types components that you can use:

- [Mirrors](/guide/components/mirror)
- [Lenses](/guide/components/lens)
- [Groups](/guide/components/group)
