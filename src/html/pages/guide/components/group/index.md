## Groups

Groups are used to organize model elements ([lenses](/guide/components/lens) and 
[mirrors](/guide/components/mirror)). They are useful to:

0 Create a logical structure in the model, that makes it easier to understand. For instance, one group
could hold a telescope, while a second group could hold the elements of an eyepiece.
0 Move element groups together easily. 

It's possible to add mirrors, lenses, and other groups to a group. A group has only **position** properties 
(*[read more](/guide/components/position)*). Changing the X or Y values of the group moves all elements inside it.
Changing the group rotation will rotate all elements inside it around the group position.

The position of an element nested within a group is always relative to the group position. For instance, the 
following figure shows a lens within a group, with the following positions:

- Group: (x: **0mm**, y: **0mm**, rotation: **0°**)
- Lens: (x: **100mm**, y: **0mm**, rotation: **0°**)

![Figure 1](/guide/components/group/f1.png)

If we rotate the group, the lens and its position are rotated 30° around the group position.

- Group: (x: **0mm**, y: **0mm**, rotation: **30°**)
- Lens: (x: **100mm**, y: **0mm**, rotation: **0°**)

![Figure 2](/guide/components/group/f2.png)

