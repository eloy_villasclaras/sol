## Newtonian reflector tutorial

In this tutorial we'll build a simple Newtonian reflector. Currently bafflers are not implemented in SOL, so only
the mirrors will be created. After the reflector is ready, we'll briefly test how it performs.

Steps:

0 [Create a new design](./1)
0 [Create primary mirror](./2)
0 [Create secondary mirror](./3)
0 [Test the optics](./4)
0 [Adjust secondary mirror](./5)
