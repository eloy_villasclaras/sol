## Hubble OTA tutorial

In this tutorial we'll build the OTA of the Hubble Space Telescope, as originally designed.
Bafflers are not yet implemented in SOL, so only the mirrors will be created.
Once the OTA is ready, we'll briefly test how it performs.

Later, we will try to replicate the original Hubble flaw,
and compare the performance of both systems.

Information about OTA specifications have been extracted from
[The Hubble Space Telescope Optical Systems Failure Report](https://www.ssl.berkeley.edu/~mlampton/AllenReportHST.pdf){external}.

Steps:

0 [Create a new design](./1)
0 [Create primary mirror](./2)
0 [Create secondary mirror](./3)
0 [Test the optics](./4)
0 [Clone the design](./5)
0 [Test HST flaw](./6)
