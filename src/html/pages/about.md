## About SOL

Simple Optics Laboratory is a toy app to play with optical elements: mirrors and lenses.
In the future bafflers (opaque elements) will be added.

The goal of this tool is simply to help understand the optical systems that amateur astronomers
user every day, weather permitting. For this purpose, a simplistic simulation is expected to be
enough. Light rays are simulated using Snell's law; also the following assumptions are made in
the simulation:

- Optical elements have no imperfections
- There is no atmosphere
- Light diffraction has not yet been invented in SOL's universe
- Lens light transmission is 100%


If you are interested in reading about the development of SOL, see [development](/development).

