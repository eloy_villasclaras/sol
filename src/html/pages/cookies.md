## Cookies and privacy

SOL uses cookies to:

- Identify logged in user in a session. Designs are linked with an user account. SOL will only
 display the display name to other users. Email or third party identifiers will never be shown to
 other users.
- Collect anonymous Google Analytics data (page views and navigation)

### Information stored by the server

When you log in, the server stores the following information:

- display name. You can change this; besides your public designs, the display name is the only
information that SOL displays to other users.
- email. The email is intended to allow for account recovery. SOL will not display your email
to other users, or send you any email (unless you explicitly opt-in for notifications).
- Google ID. Your Google account identifier. This is not critical information: it is actually
part of your Google + account public URL.

SOL never receives, and cannot access in any way, your Google account password. You can read
more on this [here](https://support.google.com/accounts/answer/112802).

