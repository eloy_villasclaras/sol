/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    entity = require('../../src/js/logic/entity/entity');

describe('entity', function () {

    it('should eval variables', function () {
        var result = entity.evalVars({
            vars: {vr1: {eq: '1 + ca.vca1'}},
            children: {
                ca: {
                    vars: {
                        vca1: {eq: '2 + vca2 + _.cb.vcb1'},
                        vca2: {eq: '3'}
                    }
                },
                cb: {vars: {vcb1: {eq: '4'}}}
            }
        });

        expect(result).to.deep.eql({
            '': {
                'vr1': {
                    eq: '1 + ca.vca1',
                    fullId: 'vr1',
                    dependencies: ['ca.vca1'],
                    fullDependencies: ['ca.vca1', 'ca.vca2', 'cb.vcb1'],
                    val: 10
                }
            },
            'ca': {
                'vca1': {
                    eq: '2 + vca2 + _.cb.vcb1',
                    fullId: 'ca.vca1',
                    dependencies: ['vca2', '_.cb.vcb1'],
                    fullDependencies: ['ca.vca2', 'cb.vcb1'],
                    val: 9
                },
                'vca2': {
                    eq: '3',
                    fullId: 'ca.vca2',
                    dependencies: [],
                    fullDependencies: [],
                    val: 3
                }
            },
            'cb': {
                'vcb1': {
                    eq: '4',
                    fullId: 'cb.vcb1',
                    dependencies: [],
                    fullDependencies: [],
                    val: 4
                }
            }
        });
    });

    it('should eval variables in right order', function () {
        var result = entity.evalVars({
            children: {
                '1': {
                    vars: {
                        a: {eq: 'b'},
                        b: {eq: '10'}
                    }
                }
            }
        });

        expect(result).to.deep.eql({
            '1': {
                'a': {
                    eq: 'b',
                    fullId: '1.a',
                    dependencies: ['b'],
                    fullDependencies: ['1.b'],
                    val: 10
                },
                'b': {
                    eq: '10',
                    fullId: '1.b',
                    dependencies: [],
                    fullDependencies: [],
                    val: 10
                }
            }
        });
    });

    it('should detect wrong syntax', function () {
        var result = entity.evalVars({
            vars: {v1: {eq: '1k'}}
        });

        expect(result).to.deep.eql({
            '': {
                'v1': {
                    eq: '1k',
                    fullId: 'v1',
                    dependencies: [],
                    fullDependencies: [],
                    error: ['unknown unit k'],
                    val: null
                }
            }
        });
    });

    it('should identify vars using []', function () {
        var result = entity.evalVars({
            vars: {
                'k': {eq: '[1.a]'}
            },
            children: {
                '1': {
                    vars: {
                        a: {eq: '10'}
                    }
                }
            }
        });

        expect(result).to.deep.eql({
            '': {
                'k': {
                    eq: '[1.a]',
                    fullId: 'k',
                    dependencies: ['1.a'],
                    fullDependencies: ['1.a'],
                    val: 10
                }
            },
            '1': {
                'a': {
                    eq: '10',
                    fullId: '1.a',
                    dependencies: [],
                    fullDependencies: [],
                    val: 10
                }
            }
        });

    });

});
