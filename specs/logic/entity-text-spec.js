/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    entity = require('../../src/js/logic/entity/entity');

describe('entity', function () {


    it('should get entity text', function () {
        var result = entity.getInfo({
            children: {
                '1': {
                    title: 't1',
                    vars: {
                        a: {eq: 'b'},
                        b: {eq: '10'},
                        '1': {eq: '1', name: 'v1.1'}
                    }
                },
                '2': {
                    vars: {
                        a: {eq: 'b'}
                    },
                    children: {
                        '1': {
                            title: 't2.1',
                            vars: {
                                a: {eq: 'b'},
                                b: {eq: '10'},
                                '1': {eq: '1', name: 'v2.1.1'}
                            }
                        }
                    }
                }
            }
        }, function () {
        });

        expect(result).to.deep.eql({
            '1': {
                title: 't1',
                icon: false,
                vars: {
                    'a': ['', ''],
                    'b': ['', ''],
                    '1': ['v1.1', '']
                }
            },
            '2': {
                title: '2',
                icon: false,
                vars: {
                    'a': ['', '']
                }
            },
            '2.1': {
                title: 't2.1',
                icon: false,
                vars: {
                    '1': ['v2.1.1', ''],
                    'a': ['', ''],
                    'b': ['', '']
                }
            }
        });
    });

    describe('relative variable paths', function () {
        beforeEach(function () {
            this.vars = {
                '1': {
                    title: 't1',
                    icon: false,
                    vars: {
                        'a': ['1-A', ''],
                        'b': ['1-B', '']
                    }
                },
                '2': {
                    title: 't2',
                    icon: false,
                    vars: {
                        'a': ['2-A', '']
                    }
                },
                '2.1': {
                    title: 't2.1',
                    icon: false,
                    vars: {
                        'a': ['2.1-A', '']
                    }
                },
                '2.2': {
                    title: 't2.2',
                    icon: false,
                    vars: {
                        'a': ['2.2-A', '']
                    }
                }
            };
        });

        it('should relativize paths (1)', function () {
            expect(entity.relative(this.vars, '1')).to.deep.eql({
                'a': {name: '1-A', units: '', icon: false, entity: 't1', dep: false},
                'b': {name: '1-B', units: '', icon: false, entity: 't1', dep: false},
                '_.2.a': {name: '2-A', units: '', icon: false, entity: 't2', dep: false},
                '_.2.1.a': {name: '2.1-A', units: '', icon: false, entity: 't2.1', dep: false},
                '_.2.2.a': {name: '2.2-A', units: '', icon: false, entity: 't2.2', dep: false}
            });
        });

        it('should relativize paths (2)', function () {
            expect(entity.relative(this.vars, '2')).to.deep.eql({
                '_.1.a': {name: '1-A', units: '', icon: false, entity: 't1', dep: false},
                '_.1.b': {name: '1-B', units: '', icon: false, entity: 't1', dep: false},
                'a': {name: '2-A', units: '', icon: false, entity: 't2', dep: false},
                '1.a': {name: '2.1-A', units: '', icon: false, entity: 't2.1', dep: false},
                '2.a': {name: '2.2-A', units: '', icon: false, entity: 't2.2', dep: false}
            });
        });

        it('should relativize paths (3)', function () {
            expect(entity.relative(this.vars, '2.1')).to.deep.eql({
                '_._.1.a': {name: '1-A', units: '', icon: false, entity: 't1', dep: false},
                '_._.1.b': {name: '1-B', units: '', icon: false, entity: 't1', dep: false},
                '_.a': {name: '2-A', units: '', icon: false, entity: 't2', dep: false},
                'a': {name: '2.1-A', units: '', icon: false, entity: 't2.1', dep: false},
                '_.2.a': {name: '2.2-A', units: '', icon: false, entity: 't2.2', dep: false}
            });
        });

        it('should exclude var', function () {
            expect(entity.relative(this.vars, '2.1', 'a')).to.deep.eql({
                '_._.1.a': {name: '1-A', units: '', icon: false, entity: 't1', dep: false},
                '_._.1.b': {name: '1-B', units: '', icon: false, entity: 't1', dep: false},
                '_.a': {name: '2-A', units: '', icon: false, entity: 't2', dep: false},
                '_.2.a': {name: '2.2-A', units: '', icon: false, entity: 't2.2', dep: false}
            });
        });

        it('should indicate cyclical dependencies', function () {
            var dependencyData = {
                '1': {
                    'a': {fullDependencies: ["2.1.a"]},
                    'b': {fullDependencies: []}
                },
                '2': {
                    'a': {fullDependencies: []}
                },
                '2.1': {
                    'a': {fullDependencies: []}
                },
                '2.2': {
                    'a': {fullDependencies: []}
                }
            };

            expect(entity.relative(this.vars, '2.1', 'a', dependencyData)).to.deep.eql({
                '_._.1.a': {name: '1-A', units: '', icon: false, entity: 't1', dep: true},
                '_._.1.b': {name: '1-B', units: '', icon: false, entity: 't1', dep: false},
                '_.a': {name: '2-A', units: '', icon: false, entity: 't2', dep: false},
                '_.2.a': {name: '2.2-A', units: '', icon: false, entity: 't2.2', dep: false}
            });
        });

    });
});
