/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    return [
        {
            label: 'change var value',
            action: {type: 'set-component-var-value', itemId: 'c1', varId: 'x', key: 'eq', value: '1'},
            expected: function (document) {
                document.content.components.children['c1'].vars['x'].eq = '1';
            }
        },
        {
            label: 'change component value',
            action: {type: 'set-component-value', itemId: 'c1', key: 'title', value: 't'},
            expected: function (document) {
                document.content.components.children['c1'].title = 't';
            }
        }
    ];

}();