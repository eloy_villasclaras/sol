/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    return [
        {
            label: 'change title',
            action: {type: 'design-metadata', title: 't2', description: null},
            expected: function (doc) {
                doc.title = 't2';
            }
        },
        {
            label: 'change description',
            action: {type: 'design-metadata', description: 'm2'},
            expected: function (doc) {
                doc.description = 'm2';
            }
        },
        {
            label: 'change both',
            action: {type: 'design-metadata', title: 't3', description: 'd3'},
            expected: function (doc) {
                doc.title = 't3';
                doc.description = 'd3';
            }
        }
    ];

}();
