/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    return [
        {
            label: 'create item at root',
            action: {type: 'create-component', itemType: 'element', parent: null},
            expected: function (doc) {
                doc.content.components.children['c2'] = {
                    type: 'element',
                    title: '2',
                    optics: null,
                    "vars": {"x": {"eq": "0"}, "y": {"eq": "0"}, "rot": {"eq": "0"}}
                };
            }
        },
        {
            label: 'create item at child',
            action: {type: 'create-component', itemType: 'element', parent: 'c1'},
            expected: function (doc) {
                doc.content.components.children['c1'].children['c2'] = {
                    type: 'element',
                    title: '2',
                    optics: null,
                    "vars": {"x": {"eq": "0"}, "y": {"eq": "0"}, "rot": {"eq": "0"}}
                };
            }
        },
        {
            label: 'create mirror at root',
            action: {type: 'create-component', itemType: 'element', optics: 'mirror'},
            expected: function (doc) {
                doc.content.components.children['c2'] = {
                    type: 'element',
                    title: '2',
                    optics: 'mirror',
                    shape: 'flat',
                    "vars": {
                        "x": {"eq": "0"},
                        "y": {"eq": "0"},
                        "rot": {"eq": "0"},
                        "ap": {"eq": "100"},
                        "hr": {"eq": "0"}
                    }
                };
            }
        },
        {
            label: 'create lens at root',
            action: {type: 'create-component', itemType: 'element', optics: 'lens'},
            expected: function (doc) {
                doc.content.components.children['c2'] = {
                    type: 'element',
                    title: '2',
                    optics: 'lens',
                    frontShape: 'flat',
                    backShape: 'flat',
                    "vars": {
                        "x": {"eq": "0"},
                        "y": {"eq": "0"},
                        "rot": {"eq": "0"},
                        "ap": {"eq": "100"},
                        "th": {"eq": "25"},
                        "n": {"eq": "1.5"},
                        "v": {"eq": "70"}
                    }
                };
            }
        }
    ];

}();
