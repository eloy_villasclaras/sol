/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    return [
        {
            label: 'change analysis value',
            action: {type: 'set-analysis-value', itemId: 'a1', key: 'exit', value: 'ex-t'},
            expected: function (document) {
                document.content.analysis.children['a1'].exit = 'ex-t';
            }
        },
        {
            label: 'change analysis var value',
            action: {type: 'set-analysis-var-value', itemId: 'a2', varId: 'a', key: 'eq', value: '-5'},
            expected: function (document) {
                document.content.analysis.children['a2'].vars['a'].eq = '-5';
            }
        }
    ];

}();