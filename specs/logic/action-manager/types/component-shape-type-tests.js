/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    return [
        {
            label: 'lens shape',
            action: {type: 'set-component-shape', itemId: 'c1.c1.c2', shape: 'parabola', face: 'front'},
            expected: function (doc) {
                var item = doc.content.components.children['c1'].children['c1'].children['c2'],
                    vars = item.vars;

                item.frontShape = 'parabola';
                delete vars['f_b'];
                delete vars['f_a'];
                vars['f_f'] = {eq: '1000'};
            }
        },
        {
            label: 'mirror shape',
            action: {type: 'set-component-shape', itemId: 'c1.c1.c3', shape: 'parabola', face: null},
            expected: function (doc) {
                var item = doc.content.components.children['c1'].children['c1'].children['c3'],
                    vars = item.vars;

                item.shape = 'parabola';
                delete vars['m_a'];
                delete vars['m_b'];
                vars['m_f'] = {eq: '1000'};
            }
        }
    ];

}();
