/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';


    return [
        {
            label: 'delete item at root',
            action: {type: 'delete-component', itemId: 'c1'},
            expected: function (doc) {
                delete doc.content.components.children['c1'];
            }
        },
        {
            label: 'delete item from group',
            action: {type: 'delete-component', itemId: 'c1.c1'},
            expected: function (doc) {
                doc.content.components.children['c1'].children = {};
            }
        }
    ];

}();
