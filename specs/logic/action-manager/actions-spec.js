/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    impl = require('./test-undo-impl'),
    ActionManager = require('./action-manager');

describe('entity', function () {

    beforeEach(function () {
        this.document = {
            a: 1,
            b: 2
        };

        this.actionManager = new ActionManager(impl);
        this.actionManager.action(this.document, {key: 'a', value: 2});
        this.actionManager.action(this.document, {key: 'b', value: 3});
    });

    it('should invoke impl do', function () {
        expect(this.document).to.deep.eql({a: 2, b: 3});
    });

    it('should undo action', function () {
        this.actionManager.undo(this.document);
        expect(this.document).to.deep.eql({a: 2, b: 2});
    });

    it('should undo several actions', function () {
        this.actionManager.undo(this.document);
        this.actionManager.undo(this.document);
        expect(this.document).to.deep.eql({a: 1, b: 2});
    });

    it('should redo action', function () {
        this.actionManager.undo(this.document);
        this.actionManager.redo(this.document);
        expect(this.document).to.deep.eql({a: 2, b: 3});
    });

    it('should clear later undone actions on new action', function () {
        this.actionManager.undo(this.document);
        this.actionManager.undo(this.document);
        this.actionManager.action(this.document, {key: 'a', value: 0});
        this.actionManager.redo(this.document);

        expect(this.document).to.deep.eql({a: 0, b: 2});
    });
});
