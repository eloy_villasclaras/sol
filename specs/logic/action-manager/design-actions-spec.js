/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var chai = require('chai'),
    expect = chai.expect,

    clone = require('clone'),

    designImpl = require('../../../src/js/logic/actions/design-impl'),
    ActionManager = require('./action-manager'),

    testDocument = require('./test-document.json'),

    types = [
        require('./types/component-create-tests'),
        require('./types/component-delete-tests'),
        require('./types/component-var-tests'),
        require('./types/component-value-tests'),
        require('./types/design-metadata-tests'),
        require('./types/component-shape-type-tests'),
        require('./types/analysis-create-tests'),
        require('./types/analysis-delete-tests'),
        require('./types/analysis-value-tests')
    ],

    test = function (typeTests) {
        typeTests.forEach(function (actionTest) {
            describe(actionTest.label, function () {
                beforeEach(function () {
                    this.expectedDocument = clone(this.originalDocument);
                    actionTest.expected(this.expectedDocument);

                    this.actionManager.action(this.workingDocument, actionTest.action);
                });

                it('should do action', function () {
                    expect(this.workingDocument).to.deep.equal(this.expectedDocument);
                });

                it('should undo action', function () {
                    this.actionManager.undo(this.workingDocument);
                    expect(this.workingDocument).to.deep.equal(this.originalDocument);
                });

                it('should redo action', function () {
                    this.actionManager.undo(this.workingDocument);
                    this.actionManager.redo(this.workingDocument);
                    expect(this.workingDocument).to.deep.equal(this.expectedDocument);
                });
            });
        });
    };

describe('design actions implementation', function () {

    beforeEach(function () {
        this.actionManager = new ActionManager(designImpl);
        this.originalDocument = clone(testDocument);
        this.workingDocument = clone(this.originalDocument);
    });

    types.forEach(test);
});