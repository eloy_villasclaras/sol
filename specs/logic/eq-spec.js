/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    expressions = require('../../src/js/logic/eq/eq');

describe('eq', function () {

    describe('with numbers', function () {
        var tests = {
                '1': 1,
                '-1': -1,
                '2 * (3+4)': 14,
                '2 * 3 + 4': 10,
                '3 + 4 / 2': 5,
                '3 * 4 ^ 2': 48,
                '(3 * 4) ^ 2': 144
            },


            test = function (txt, value) {
                var exp = expressions.parse(txt);
                it('should eval ' + txt, function () {
                    expect(expressions.eval(exp)).to.equal(value);
                });
                it('should simplify ' + txt, function () {
                    expect(expressions.complexity(exp.tree)).to.equal(0);
                });
            };

        for (var exp in tests) {
            if (tests.hasOwnProperty(exp)) {
                test(exp, tests[exp]);
            }
        }
    });


    describe('with units', function () {
        var tests = {
                '1': 1,
                '1mm': 1,
                '1um': 0.001,
                '1m': 1000,
                '1 m': 1000,
                '1m 1': 1001,
                '1m 1mm': 1001,
            },

            units = {
                'mm': 1,
                'um': 1e-3,
                'm': 1e3,
                'km': 1e6
            },

            test = function (txt, value) {
                var exp = expressions.parse(txt, units);
                it('should eval ' + txt, function () {
                    expect(expressions.eval(exp, null)).to.equal(value);
                });
                it('should simplify ' + txt, function () {
                    expect(expressions.complexity(exp.tree)).to.equal(0);
                });
            };

        for (var exp in tests) {
            if (tests.hasOwnProperty(exp)) {
                test(exp, tests[exp]);
            }
        }
    });


    describe('with vars', function () {
        var vars = {
                a: 1,
                b: 2,
                c: 3
            },
            tests = {
                '-b': [1, -2, ['b']],
                'b': [0, 2, ['b']],
                '2 * (3+a)': [2, 8, ['a']],
                'b * 3 + 4': [2, 10, ['b']],
                'b * (3 + 4)': [1, 14, ['b']],
                'a + c / b': [2, 2.5, ['a', 'c', 'b']],
                'k': [0, null, ['k']]
            },

            test = function (txt, value) {
                var exp = expressions.parse(txt);
                it('should eval ' + txt, function () {
                    expect(expressions.eval(exp, vars)).to.equal(value[1]);
                });
                it('should simplify ' + txt, function () {
                    expect(expressions.complexity(exp.tree)).to.equal(value[0]);
                });
                it('should identify dependencies of ' + txt, function () {
                    expect(exp.dependencies).to.eql(value[2]);
                });
            };

        for (var exp in tests) {
            if (tests.hasOwnProperty(exp)) {
                test(exp, tests[exp]);
            }
        }

        it('should map var name', function () {
            var exp = expressions.parse('2 * (3+a)');
            expressions.mapVar(exp, 'a', 'a2');
            expect(expressions.eval(exp, {a2: 1})).to.equal(8);
        });
    });

});
