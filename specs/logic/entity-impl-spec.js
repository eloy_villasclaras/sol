/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    entity = require('../../src/js/logic/entity/entity-impl');

describe('entity', function () {

    beforeEach(function () {
        this.root1 = {
            vars: {vr1: {eq: '1'}},
            children: {
                ca: {
                    vars: {
                        vca1: '2'
                    }
                }
            }
        };

        this.root2 = {
            vars: {vr1: {eq: '1 + ca.vca1'}},
            children: {
                ca: {
                    vars: {
                        vca1: {eq: '2 + vca2 + _.cb.vcb1'},
                        vca2: {eq: '3'}
                    }
                },
                cb: {vars: {vcb1: {eq: '4'}}}
            }
        };
    });


    it('should map entities', function () {
        var map = entity.mapEntities(this.root1);
        expect(Object.keys(map)).to.eql(['', 'ca']);
    });

    it('should map variables', function () {
        var entityMap = entity.mapEntities(this.root1),
            variableMap = entity.mapVariables(entityMap);

        expect(Object.keys(variableMap)).to.eql(['vr1', 'ca.vca1']);
    });

    it('should resolve variable names', function () {
        var entityMap = entity.mapEntities(this.root2),
            variableMap = entity.mapVariables(entityMap);

        entity.resolveDependencies(variableMap);

        expect(variableMap['vr1'].v.dependencies).to.eql(['ca.vca1']);
        expect(variableMap['ca.vca1'].v.dependencies).to.eql(['ca.vca2', 'cb.vcb1']);
    });

    it('should resolve complete dependencies', function () {
        var entityMap = entity.mapEntities(this.root2),
            variableMap = entity.mapVariables(entityMap),
            dependencyMap;

        entity.resolveDependencies(variableMap);

        dependencyMap = entity.fullDependencies(variableMap);

        expect(dependencyMap['vr1']).to.eql(['ca.vca1', 'ca.vca2', 'cb.vcb1']);
    });

    it('should sort dependencies', function () {
        var entityMap = entity.mapEntities(this.root2),
            variableMap = entity.mapVariables(entityMap),
            dependencyMap,
            sorted;

        entity.resolveDependencies(variableMap);

        dependencyMap = entity.fullDependencies(variableMap);
        sorted = entity.sortDependencies(dependencyMap);

        expect(sorted).to.eql(['cb.vcb1', 'ca.vca2', 'ca.vca1', 'vr1']);
    });

    it('should calculate values', function () {
        var entityMap = entity.mapEntities(this.root2),
            variableMap = entity.mapVariables(entityMap),
            dependencyMap,
            sorted,
            values;

        entity.resolveDependencies(variableMap);

        dependencyMap = entity.fullDependencies(variableMap);
        sorted = entity.sortDependencies(dependencyMap);
        values = entity.eval(sorted, variableMap);

        expect(values).to.eql({
            'ca.vca2': 3,
            'cb.vcb1': 4,
            'ca.vca1': 9,
            'vr1': 10
        });
    });

});
