/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var chai = require('chai'),
    expect = chai.expect,

    extend = require('extend'),

    entity = require('../../src/js/logic/entity/entity'),
    elementOps = require('../../src/js/logic/optics/element-ops');

describe('element analysis', function () {

    it('should notice syntac error in var', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "k"},
                            "ap": {"eq": "1000"},
                            "m_r": {"eq": "1000"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "circle"
                    }
                }
            },
            expected = {
                error: 'child',
                pos: {
                    x: 0,
                    y: 0,
                    rot: 0
                },
                children: {
                    "1": {
                        fullId: "1",
                        error: 'var',
                        parent: false,
                        type: "element",
                        optics: "mirror",
                        pos: {
                            x: 0,
                            y: 0,
                            rot: null
                        },
                        absPos: {
                            x: 0,
                            y: 0,
                            rot: 0
                        },
                        hap: 500,
                        shape: {
                            type: 'circle',
                            r: 1000
                        },
                        hr: 0
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(elements).to.deep.eql(expected);
    });

    it('should detect circular shape var error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "m_r": {"eq": "0"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "circle"
                    }
                }
            },
            expected = {
                error: 'child',
                pos: {
                    x: 0,
                    y: 0,
                    rot: 0
                },
                children: {
                    "1": {
                        fullId: "1",
                        error: 'var',
                        parent: false,
                        type: "element",
                        optics: "mirror",
                        pos: {
                            x: 0,
                            y: 0,
                            rot: 0
                        },
                        absPos: {
                            x: 0,
                            y: 0,
                            rot: 0
                        },
                        hap: 0,
                        shape: {
                            type: 'circle',
                            r: 0
                        },
                        hr: 0
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(vars['1']['m_r'].error).to.eql(['non-zero']);
        expect(elements).to.deep.eql(expected);
    });

    it('should detect ellipse shape var error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "m_hsa": {"eq": "0"},
                            "m_vsa": {"eq": "-1"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "ellipse"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(vars['1']['m_hsa'].error).to.eql(['non-zero']);
        expect(vars['1']['m_vsa'].error).to.eql(['gt-zero']);
        expect(elements.error).to.equal('child');
    });

    it('should detect parabola shape var error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "m_f": {"eq": "0"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "parabola"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(vars['1']['m_f'].error).to.eql(['non-zero']);
        expect(elements.error).to.equal('child');
    });

    it('should detect ap var error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "0"},
                            "m_f": {"eq": "10"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "parabola"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(vars['1']['ap'].error).to.eql(['gt-zero']);
        expect(elements.error).to.equal('child');
    });

    it('should detect lens var error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "lens",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "0"},
                            "n": {"eq": "0"},
                            "v": {"eq": "0"},
                            "th": {"eq": "-30"}
                        },
                        "frontShape": "flat",
                        "backShape": "flat"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(vars['1']['n'].error).to.eql(['gt-zero']);
        expect(vars['1']['v'].error).to.eql(['gt-zero']);
        expect(vars['1']['th'].error).to.eql(['gt-zero']);
        expect(elements.error).to.equal('child');
    });

    it('should detect mirror hole error', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "100"},
                            "m_f": {"eq": "10"},
                            "hr": {"eq": "200"}
                        },
                        "shape": "parabola"
                    }
                }
            },
            vars = entity.evalVars(root);

            elementOps.analyze(root, vars);

        expect(vars['1']['hr'].error).to.eql(['lt-hap']);
    });

});


