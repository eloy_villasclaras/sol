/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    systemManager = require('../../src/js/logic/optics/system-manager.js');

describe('optic system manager', function () {

    beforeEach(function () {
        this.root = {type: 'root', children: {}};
    });

    it('should add child element to root', function () {
        var childId = systemManager.createItem('element', this.root),
            child = this.root.children[childId],
            childId2 = systemManager.createItem('element', this.root),
            child2 = this.root.children[childId2];

        expect(childId).to.equal('c1');
        expect(childId2).to.equal('c2');

        expect(child).to.deep.eql({
            type: 'element',
            title: '1',
            optics: null,
            vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}
        });
        expect(child2).to.deep.eql({
            type: 'element',
            title: '2',
            optics: null,
            vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}
        });
    });

    it('should not allow adding item to element', function () {
        var childId = systemManager.createItem('element', this.root),
            childId2 = systemManager.createItem('element', this.root, childId);

        expect(childId).to.equal('c1');
        expect(childId2).to.equal(false);
    });

    it('should add group to root', function () {
        var childId = systemManager.createItem('group', this.root),
            child = this.root.children[childId],
            childId2 = systemManager.createItem('group', this.root),
            child2 = this.root.children[childId2];

        expect(childId).to.equal('c1');
        expect(childId2).to.equal('c2');

        expect(child).to.deep.eql({type: 'group', title: '1', children: {}, vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}});
        expect(child2).to.deep.eql({type: 'group', title: '2', children: {}, vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}});
    });

    it('should add mirror with proper parameters', function () {
        var childId = systemManager.createItem('element', this.root, null, {optics: 'mirror'}),
            child = this.root.children[childId];

        expect(child).to.deep.eql({
            type: 'element',
            title: '1',
            optics: 'mirror',
            shape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                hr: {eq: '0'}
            }
        });
    });

    it('should add lens with proper parameters', function () {
        var childId = systemManager.createItem('element', this.root, null, {optics: 'lens'}),
            child = this.root.children[childId];

        expect(child).to.deep.eql({
            type: 'element',
            title: '1',
            optics: 'lens',
            frontShape: 'flat',
            backShape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                th: {eq: '25'},
                v: {eq: '70'},
                n: {eq: '1.5'}
            }
        });
    });

    it('should calculate parent level', function () {
        expect(systemManager.parentLevel('1.2.1', '1.2')).to.equal(1);
        expect(systemManager.parentLevel('1.2.1', '1')).to.equal(2);
        expect(systemManager.parentLevel('1.2.1', '2')).to.equal(false);
    });


    describe('with one group', function () {
        beforeEach(function () {
            this.groupId = systemManager.createItem('group', this.root);
        });

        it('should add items to group', function () {
            var childId = systemManager.createItem('element', this.root, this.groupId);

            expect(Object.keys(this.root.children)).to.eql(['c1']);
            expect(Object.keys(this.root.children['c1'].children)).to.eql(['c1']);
            expect(this.root.children['c1'].children[childId]).to.deep.eql({
                type: 'element',
                title: '1',
                optics: null,
                vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}
            });
        });

        it('should add items to deep levels', function () {
            var childGroupId = systemManager.createItem('group', this.root, this.groupId),
                fullGroupId = systemManager.id(this.groupId, childGroupId),
                elementId = systemManager.createItem('element', this.root, fullGroupId);

            expect(elementId).to.equal('c1');
            expect(Object.keys(this.root.children)).to.eql(['c1']);
            expect(Object.keys(this.root.children['c1'].children)).to.eql(['c1']);
            expect(Object.keys(this.root.children['c1'].children['c1'].children)).to.eql(['c1']);
            expect(this.root.children['c1'].children['c1'].children['c1']).to.deep.eql({
                type: 'element',
                title: '1',
                optics: null,
                vars: {x: {eq: '0'}, y: {eq: '0'}, rot: {eq: '0'}}
            });
        });

        it('should delete item', function () {
            var childId = systemManager.createItem('element', this.root, this.groupId),
                fullChildId = systemManager.id(this.groupId, childId),
                childId2 = systemManager.createItem('element', this.root, this.groupId);

            expect(Object.keys(this.root.children)).to.eql([this.groupId]);
            expect(Object.keys(this.root.children[this.groupId].children)).to.eql([childId, childId2]);

            systemManager.deleteItem(this.root, fullChildId);

            expect(Object.keys(this.root.children)).to.eql([this.groupId]);
            expect(Object.keys(this.root.children[this.groupId].children)).to.eql([childId2]);

            systemManager.deleteItem(this.root, this.groupId);

            expect(Object.keys(this.root.children)).to.eql([]);
        });


        it('should create var', function () {
            systemManager.createVar(this.root, this.groupId, 'v');
            expect(this.root.children[this.groupId].vars).to.have.keys('x', 'y', 'rot', 'v');
            expect(this.root.children[this.groupId].vars['v']).to.deep.eql({eq: '0'});
        });

        it('should delete var', function () {
            systemManager.createVar(this.root, this.groupId, 'v');
            systemManager.deleteVar(this.root, this.groupId, 'v');
            expect(this.root.children[this.groupId].vars).to.have.keys('x', 'y', 'rot');
        });

        it('should not allow to delete default var', function () {
            var result = systemManager.deleteVar(this.root, this.groupId, 'x');
            expect(result).to.equal(false);
            expect(this.root.children[this.groupId].vars).to.have.keys('x', 'y', 'rot');
        });

        it('should not allow to add vars to root', function () {
            var result = systemManager.deleteVar(this.root, null, 'v');
            expect(result).to.equal(false);
            expect(this.root).not.to.have.key('vars');
        });
    });
});
