/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    analysisManager = require('../../src/js/logic/optics/analysis/analysis-manager.js');

describe('analysys manager', function () {

    beforeEach(function () {
        this.root = {children: {}};
    });

    it('should create analysis', function () {
        var a1id = analysisManager.createAnalysis(this.root),
            a1 = this.root.children[a1id],
            a2id = analysisManager.createAnalysis(this.root),
            a2 = this.root.children[a2id];

        expect(a1id).to.equal('a1');
        expect(a2id).to.equal('a2');

        expect(a1).to.deep.eql({
            type: 'inf-point',
            distribution: '2d',
            title: '1',
            entrance: null,
            vars: {a: {eq: '0'}, n: {eq: '1'}}
        });
        expect(a2).to.deep.eql({
            type: 'inf-point',
            distribution: '2d',
            title: '2',
            entrance: null,
            vars: {a: {eq: '0'}, n: {eq: '1'}}
        });
    });

    describe('with previous analysis', function () {
        beforeEach(function () {
            this.root.children = {
                'a1': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: 'en1',
                    vars: {a: {eq: '0'}, n: {eq: '1'}}
                },
                'a2': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: 'en2',
                    vars: {a: {eq: '4'}, n: {eq: '1'}}
                }
            };
        });

        it('should find analysis', function () {
            expect(analysisManager.findAnalysis(this.root, 'a2')).to.deep.eql({
                type: 'inf-point',
                distribution: '2d',
                entrance: 'en2',
                vars: {a: {eq: '4'}, n: {eq: '1'}}
            });
        });

        it('should clone data from previous item', function () {

            var aid = analysisManager.createAnalysis(this.root),
                a = this.root.children[aid];

            expect(aid).to.equal('a3');
            expect(a).to.deep.eql({
                type: 'inf-point',
                distribution: '2d',
                title: '3',
                entrance: 'en2',
                vars: {a: {eq: '4'}, n: {eq: '1'}}
            });
        });

        it('should delete analysis', function () {
            var a = analysisManager.deleteAnalysis(this.root, 'a1');

            expect(a).to.deep.eql({
                type: 'inf-point',
                distribution: '2d',
                entrance: 'en1',
                vars: {a: {eq: '0'}, n: {eq: '1'}}
            });
            expect(Object.keys(this.root.children)).to.deep.eql(['a2']);
        });
    });
});
