/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var entity = require('../../../src/js/logic/entity/entity'),
    elementOps = require('../../../src/js/logic/optics/element-ops'),
    simulatorImpl = require('../../../src/js/logic/optics/simulator/simulator-impl'),

    rootComponent = require('./s3.json');

var componentVars = entity.evalVars(rootComponent),
    elements = elementOps.analyze(rootComponent, componentVars),
    scene = simulatorImpl.prepare(elements);

console.time("c");

for (var y = -5; y <= 5; y += .05) {
    for (var a = -5; a <= 5; a += .05) {
        var ray = [-100, y, 0, Math.cos(a * Math.PI / 180), Math.sin(a * Math.PI / 180), 0],
            collisionGroups = simulatorImpl.collisionGroups(scene.faces, ray);

        simulatorImpl.findCollision(scene.faces, collisionGroups);
    }
}

console.timeEnd("c");
