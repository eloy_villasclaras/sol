/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    expect = chai.expect,

    entity = require('../../../src/js/logic/entity/entity'),
    elementOps = require('../../../src/js/logic/optics/element-ops'),
    simulatorImpl = require('../../../src/js/logic/optics/simulator/simulator-impl'),

    shapes = {
        flat: require('../../../src/js/logic/optics/shapes/flat-impl'),
        circle: require('../../../src/js/logic/optics/shapes/circle-impl')
    },

    rootComponent = require('./s2.json');

chai.use(sinonChai);

describe('simulation (scene 2)', function () {
    beforeEach(function () {
        this.componentVars = entity.evalVars(rootComponent);
        this.elements = elementOps.analyze(rootComponent, this.componentVars);
        this.scene = simulatorImpl.prepare(this.elements);
    });

    it('should find collisions groups', function () {
        var ray = [-1, 0, 0, 1, 0, 0],
            collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray);

        expect(collisionGroups.length).to.equal(1);
        expect(collisionGroups[0].length).to.equal(3);
    });

    it('should find collision', function () {
        var ray = [-1, 0, 0, 1, 0, 0],
            collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
            collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups, true);

        expect(collisions.length).to.equal(1);
        expect(collisions[0].face).to.equal('1.front');
    });

    it('should find collision from face', function () {
        var ray = [0, 0, 0, 1, 0, 0],
            collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
            collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups);

        expect(collisionGroups.length).to.equal(1);
        expect(collisionGroups[0].length).to.equal(2);
        expect(collisions.length).to.equal(1);
        expect(collisions[0].face).to.equal('1.back');
    });

    it('should find collision with side cylinder', function () {
        var ray = [-10, -40, 0, Math.cos(45 * Math.PI / 180), Math.sin(45 * Math.PI / 180), 0],
            collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
            collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups);

        expect(collisionGroups.length).to.equal(1);
        expect(collisionGroups[0].length).to.equal(2);
        expect(collisions.length).to.equal(1);
        expect(collisions[0].face).to.equal('1.side');
    });


    it('should find collision with correct point in side cylinder', function () {
        var ray = [5, -40, 0, Math.cos(85 * Math.PI / 180), Math.sin(85 * Math.PI / 180), 0],
            collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
            collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups);

        expect(collisionGroups.length).to.equal(1);
        expect(collisionGroups[0].length).to.equal(1);
        expect(collisions.length).to.equal(1);
        expect(collisions[0]).to.deep.eql({
            face: '1.side',
            ray: [-3.2502267294815206, -20, 0, Math.cos(85 * Math.PI / 180), Math.sin(85 * Math.PI / 180), 0]
        });
    });

});

