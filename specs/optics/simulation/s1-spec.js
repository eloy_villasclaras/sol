/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    sinon = require('sinon'),
    sinonChai = require('sinon-chai'),
    expect = chai.expect,

    entity = require('../../../src/js/logic/entity/entity'),
    elementOps = require('../../../src/js/logic/optics/element-ops'),
    simulatorImpl = require('../../../src/js/logic/optics/simulator/simulator-impl'),

    shapes = {
        flat: require('../../../src/js/logic/optics/shapes/flat-impl'),
        circle: require('../../../src/js/logic/optics/shapes/circle-impl')
    },

    rootComponent = require('./s1.json');

chai.use(sinonChai);

describe('simulation (scene 1)', function () {
    beforeEach(function () {
        this.componentVars = entity.evalVars(rootComponent);
        this.elements = elementOps.analyze(rootComponent, this.componentVars);
    });

    it('should prepare scene', function () {
        var scene = simulatorImpl.prepare(this.elements);
        expect(scene).to.deep.eql({
            elements: {
                '1.1': {
                    optics: 'lens',
                    pos: {
                        x: 50,
                        y: 20,
                        rot: 30 * Math.PI / 180,
                        cosRot: Math.cos(30 * Math.PI / 180),
                        sinRot: Math.sin(30 * Math.PI / 180)
                    },
                    hap: 15.612494995995995,
                    n: 1.5,
                    v: 70,
                    th: 5,
                    frontShape: {
                        type: 'circle',
                        r: 50
                    },
                    backShape: {
                        type: 'circle',
                        r: -50
                    }
                },
                '1.2': {
                    optics: 'mirror',
                    pos: {
                        x: 54.33012701892219,
                        y: 22.5,
                        rot: 30 * Math.PI / 180,
                        cosRot: Math.cos(30 * Math.PI / 180),
                        sinRot: Math.sin(30 * Math.PI / 180)
                    },
                    hap: 20,
                    shape: {
                        type: 'circle',
                        r: -60
                    },
                    hr: 0
                },
                '1.3': {
                    optics: 'lens',
                    pos: {
                        x: 64.82050807568878,
                        y: 34.33012701892219,
                        rot: 45 * Math.PI / 180,
                        cosRot: Math.cos(45 * Math.PI / 180),
                        sinRot: Math.sin(45 * Math.PI / 180)
                    },
                    hap: 10,
                    n: 1.5,
                    v: 70,
                    th: 4,
                    frontShape: {
                        type: 'flat'
                    },
                    backShape: {
                        type: 'flat'
                    }
                }
            },
            faces: {
                '1.1.front': {
                    elementId: '1.1',
                    optics: 'lens',
                    pos: {
                        x: 91.13620667976085,
                        y: 43.75,
                        rot: 30 * Math.PI / 180,
                        cosRot: Math.cos(30 * Math.PI / 180),
                        sinRot: Math.sin(30 * Math.PI / 180)
                    },
                    isBack: false,
                    hap: 15.612494995995995,
                    n: 1.5,
                    v: 70,
                    th: 5,
                    shape: {
                        type: 'circle',
                        r: 50
                    },
                    bb: [-50, -15.612494995995995, -47.5, 15.612494995995995]
                },
                '1.1.back': {
                    elementId: '1.1',
                    optics: 'lens',
                    pos: {
                        x: 8.86379332023916,
                        y: -3.7499999999999964,
                        rot: 30 * Math.PI / 180,
                        cosRot: Math.cos(30 * Math.PI / 180),
                        sinRot: Math.sin(30 * Math.PI / 180)
                    },
                    isBack: true,
                    hap: 15.612494995995995,
                    n: 1.5,
                    v: 70,
                    th: 5,
                    shape: {
                        type: 'circle',
                        r: -50
                    },
                    bb: [47.5, -15.612494995995995, 50, 15.612494995995995]
                },
                '1.2.mirror': {
                    elementId: '1.2',
                    optics: 'mirror',
                    pos: {
                        x: 2.3686027918558707,
                        y: -7.4999999999999964,
                        rot: 30 * Math.PI / 180,
                        cosRot: Math.cos(30 * Math.PI / 180),
                        sinRot: Math.sin(30 * Math.PI / 180)
                    },
                    hap: 20,
                    shape: {
                        type: 'circle',
                        r: -60
                    },
                    isBack: false,
                    bb: [56.568542494923804, -20, 60, 20],
                    hr: 0
                },
                '1.3.front': {
                    elementId: '1.3',
                    optics: 'lens',
                    pos: {
                        x: 63.406294513315686,
                        y: 32.9159134565491,
                        rot: 45 * Math.PI / 180,
                        cosRot: Math.cos(45 * Math.PI / 180),
                        sinRot: Math.sin(45 * Math.PI / 180)
                    },
                    isBack: false,
                    hap: 10,
                    n: 1.5,
                    v: 70,
                    th: 4,
                    shape: {
                        type: 'flat'
                    },
                    bb: [0, -10, 0, 10]
                },
                '1.3.back': {
                    elementId: '1.3',
                    optics: 'lens',
                    pos: {
                        x: 66.23472163806187,
                        y: 35.74434058129528,
                        rot: 45 * Math.PI / 180,
                        cosRot: Math.cos(45 * Math.PI / 180),
                        sinRot: Math.sin(45 * Math.PI / 180)
                    },
                    isBack: true,
                    hap: 10,
                    n: 1.5,
                    v: 70,
                    th: 4,
                    shape: {
                        type: 'flat'
                    },
                    bb: [0, -10, 0, 10]
                },
                '1.3.side': {
                    elementId: '1.3',
                    optics: 'lens',
                    pos: {
                        x: 64.82050807568878,
                        y: 34.33012701892219,
                        rot: 45 * Math.PI / 180,
                        cosRot: Math.cos(45 * Math.PI / 180),
                        sinRot: Math.sin(45 * Math.PI / 180)
                    },
                    isBack: false,
                    hap: 10,
                    n: 1.5,
                    v: 70,
                    th: 4,
                    shape: {
                        type: 'cylinder',
                        hs: 2
                    },
                    bb: [-2, -10, 2, 10]
                }
            }
        });
    });


    describe('scene suite', function () {
        beforeEach(function () {
            this.scene = simulatorImpl.prepare(this.elements);
        });

        describe('ray creation', function () {

            it('should create ray at inf', function () {
                var ray = simulatorImpl.ray(this.scene.elements['1.1'], 0, 0, 100, 0, 0);
                expect(ray).to.deep.eql([
                    -36.60254037844388,
                    -29.999999999999993,
                    0,
                    Math.cos(30 * Math.PI / 180),
                    Math.sin(30 * Math.PI / 180),
                    0
                ]);
            });

            it('should create ray at inf with offset', function () {
                var ray = simulatorImpl.ray(this.scene.elements['1.1'], 0, 0, 100, 10, 0);
                expect(ray).to.deep.eql([
                    -41.60254037844388,
                    -21.339745962155604,
                    0,
                    Math.cos(30 * Math.PI / 180),
                    Math.sin(30 * Math.PI / 180),
                    0
                ]);
            });

            it('should create ray at angle', function () {
                var ray = simulatorImpl.ray(this.scene.elements['1.1'], -20 * Math.PI / 180, 0, 100, 0, 0);
                expect(ray).to.deep.eql([
                    -48.48077530122081,
                    2.6351822333069705,
                    0,
                    0.9848077530122081,
                    0.1736481776669303,
                    0
                ]);
            });

        });

        describe('collision groups', function () {
            it('should find collisions groups', function () {
                var ray = simulatorImpl.ray(this.scene.elements['1.1'], 0, 0, 100, 0, 0),
                    collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray);

                expect(collisionGroups).to.deep.eql([
                    [{
                        element: '1.1.front',
                        ray: [-147.50000000000003, 0, 0, 1, 0, 0]
                    }, {
                        element: '1.1.back',
                        ray: [-52.5, 3.552713678800501e-15, 0, 1, 0, 0]
                    }, {
                        element: '1.2.mirror',
                        ray: [-45, 3.552713678800501e-15, 0, 1, 0, 0]
                    }],
                    [{
                        element: '1.3.front',
                        ray: [
                            -115.20519438020081,
                            26.22865628085716,
                            0,
                            0.9659258262890682,
                            -0.25881904510252074,
                            0
                        ]
                    }, {
                        "element": "1.3.side",
                        "ray": [
                            -117.20519438020081,
                            26.22865628085716,
                            0,
                            0.9659258262890682,
                            -0.25881904510252074,
                            0
                        ]
                    }, {
                        element: '1.3.back',
                        ray: [
                            -119.20519438020081,
                            26.22865628085716,
                            0,
                            0.9659258262890682,
                            -0.25881904510252074,
                            0
                        ]
                    }]
                ]);
            });


            it('should ignore faces behind', function () {
                var ray = [46, 28, 0, Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0],
                    collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray);

                expect(collisionGroups.length).to.equal(2);
                expect(collisionGroups[0].length).to.equal(2);
                expect(collisionGroups[0][0].element).to.equal('1.1.back');
                expect(collisionGroups[0][1].element).to.equal('1.2.mirror');
            });
        });


        describe('collisions', function () {

            beforeEach(function () {
                sinon.spy(shapes.flat, 'checkCollision');
                sinon.spy(shapes.circle, 'checkCollision');
            });

            afterEach(function () {
                shapes.flat.checkCollision.restore();
                shapes.circle.checkCollision.restore();
            });

            it('should find collision', function () {
                var ray = simulatorImpl.ray(this.scene.elements['1.1'], 0, 0, 100, 0, 0),
                    collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
                    collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups);

                expect(shapes.circle.checkCollision).to.have.callCount(3);
                expect(shapes.flat.checkCollision).to.have.callCount(0);
                expect(collisions.length).to.equal(1);
                expect(collisions[0]).to.deep.eql({
                    face: '1.1.front',
                    ray: [-50, 0, 0, 1, 0, 0]
                });
            });

            it('should find collision from inside a lens', function () {
                var ray = [46, 28, 0, Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0],
                    collisionGroups = simulatorImpl.collisionGroups(this.scene.faces, ray),
                    collisions = simulatorImpl.findCollision(this.scene.faces, collisionGroups);

                expect(collisions).to.deep.eql([{
                    face: '1.1.back',
                    ray: [49.196414372176534, 8.92820323027551, 0, 1, 0, 0]
                }]);
            });
        });
    });
});
