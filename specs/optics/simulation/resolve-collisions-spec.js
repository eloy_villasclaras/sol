/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,
    resolveCollisions = require('../../../src/js/logic/optics/simulator/resolve-collisions');

describe('collision solver', function () {

    it('should resolve normal ray', function () {
        var v = [1, 0, 0],
            n = [-1, 0, 0],
            solved = resolveCollisions.solve(v, n, 1);

        expect(solved).to.deep.eql([1, 0, 0]);
    });

    describe('mirror suite', function () {
        it('should reflect ray', function () {
            var v = [Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([-Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0]);
        });

        it('should reflect straight ray', function () {
            var v = [1, 0, 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([-1, 0, 0]);
        });

        it('should reflect reverse ray', function () {
            var v = [-1, 0, 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.eql(false);
        });

        it('should reflect ray at 30deg', function () {
            var v = [Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([-Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0]);
        });

        it('should reflect ray at -30deg', function () {
            var v = [Math.cos(-30 * Math.PI / 180), Math.sin(-30 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([-Math.cos(30 * Math.PI / 180), -Math.sin(30 * Math.PI / 180), 0]);
        });


        it('should reflect ray at 150deg', function () {
            var v = [Math.cos(150 * Math.PI / 180), Math.sin(150 * Math.PI / 180), 0],
                n = [1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0]);
        });

        it('should reflect ray at 210deg', function () {
            var v = [Math.cos(210 * Math.PI / 180), Math.sin(210 * Math.PI / 180), 0],
                n = [1, 0, 0],
                solved = resolveCollisions.solve(v, n, -1);

            expect(solved).to.deep.eql([-Math.cos(210 * Math.PI / 180), Math.sin(210 * Math.PI / 180), 0]);
        });

    });

    describe('lens suite', function () {
        it('should refract ray at 30deg', function () {
            var v = [Math.cos(30 * Math.PI / 180), Math.sin(30 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 2);

            expect(solved).to.deep.eql([0.9682458365518543, 0.24999999999999997, 0]);
        });

        it('should refract ray at -30deg', function () {
            var v = [Math.cos(-30 * Math.PI / 180), Math.sin(-30 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 2);

            expect(solved).to.deep.eql([0.9682458365518543, -0.24999999999999997, 0]);
        });


        it('should refract ray at 150deg', function () {
            var v = [Math.cos(150 * Math.PI / 180), Math.sin(150 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 1.2);

            expect(solved).to.deep.eql([-0.8, 0.5999999999999999, 0]);
        });


        it('should refract ray at 210deg', function () {
            var v = [Math.cos(210 * Math.PI / 180), Math.sin(210 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 1.2);

            expect(solved).to.deep.eql([-0.7999999999999999, -0.6000000000000001, 0]);
        });


        it('should reflect ray at 150deg, n=3', function () {
            var v = [Math.cos(150 * Math.PI / 180), Math.sin(150 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 3);

            expect(solved).to.deep.eql([0.8660254037844387, 0.49999999999999994, -0]);
        });

        it('should reflect ray at 210deg, n=3', function () {
            var v = [Math.cos(210 * Math.PI / 180), Math.sin(210 * Math.PI / 180), 0],
                n = [-1, 0, 0],
                solved = resolveCollisions.solve(v, n, 3);

            expect(solved).to.deep.eql([0.8660254037844386, -0.5000000000000001, 0]);
        });

    });
});