/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    entity = require('../../../src/js/logic/entity/entity'),
    elementOps = require('../../../src/js/logic/optics/element-ops'),
    simulatorImpl = require('../../../src/js/logic/optics/simulator/simulator-impl'),

    rootComponent = require('./s4b.json');

describe('simulation (scene 4)', function () {
    beforeEach(function () {
        this.componentVars = entity.evalVars(rootComponent);
        this.elements = elementOps.analyze(rootComponent, this.componentVars);
        this.scene = simulatorImpl.prepare(this.elements);
    });

    it('should simulate ray', function () {
        var angle = 45 * Math.PI / 180,
            ray = [0, 0, 0, Math.cos(angle), Math.sin(angle), 0],
            path = simulatorImpl.trace(this.scene.faces, ray);

        expect(path).to.deep.eql([
            [0, 0, 0],
            [20, 19.999999999999996, 0],
            [10, 29.999999999999993, 0],
            [25.000000000000014, 45, 0]
        ]);
    });
});

