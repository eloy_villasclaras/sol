/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var chai = require('chai'),
    expect = chai.expect,

    extend = require('extend'),

    entity = require('../../src/js/logic/entity/entity'),
    elementOps = require('../../src/js/logic/optics/element-ops');

describe('element analysis', function () {

    it('should resolve nested elements', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "10"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "m_r": {"eq": "1000"},
                            "hr": {"eq": "5"}
                        },
                        "shape": "circle"
                    },
                    "2": {
                        "type": "group",
                        "children": {
                            "1": {
                                "type": "element",
                                "optics": "lens",
                                "vars": {
                                    "x": {"eq": "20"},
                                    "y": {"eq": "40"},
                                    "rot": {"eq": "30"},
                                    "ap": {"eq": "100"},
                                    "n": {"eq": "1.5"},
                                    "v": {"eq": "70"},
                                    "th": {eq: "25"},
                                    "f_f": {"eq": "500"}
                                },
                                "frontShape": "parabola",
                                "backShape": "flat"
                            }
                        },
                        "vars": {"x": {"eq": "100"}, "y": {"eq": "50"}, "rot": {"eq": "45"}}
                    }
                }
            },
            expected = {
                pos: {
                    x: 0,
                    y: 0,
                    rot: 0
                },
                children: {
                    "1": {
                        fullId: "1",
                        parent: false,
                        type: "element",
                        optics: "mirror",
                        pos: {
                            x: 10,
                            y: 0,
                            rot: 0
                        },
                        absPos: {
                            x: 10,
                            y: 0,
                            rot: 0
                        },
                        hap: 500,
                        hr: 5,
                        shape: {
                            type: 'circle',
                            r: 1000
                        }
                    },
                    "2": {
                        fullId: "2",
                        parent: false,
                        type: "group",
                        pos: {
                            x: 100,
                            y: 50,
                            rot: 45
                        },
                        absPos: {
                            x: 100,
                            y: 50,
                            rot: 45
                        },
                        children: {
                            "1": {
                                fullId: "2.1",
                                parent: "2",
                                type: "element",
                                optics: "lens",
                                pos: {
                                    x: 20,
                                    y: 40,
                                    rot: 30
                                },
                                absPos: {
                                    x: 85.85786437626905,
                                    y: 92.42640687119285,
                                    rot: 75
                                },
                                hap: 50,
                                n: 1.5,
                                v: 70,
                                th: 25,
                                frontShape: {
                                    type: "parabola",
                                    f: 500
                                },
                                backShape: {
                                    type: "flat"
                                }
                            }
                        }
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(elements).to.deep.eql(expected);
    });

    it('should include max shape aperture', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "mirror",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "m_r": {"eq": "10"},
                            "hr": {"eq": "0"}
                        },
                        "shape": "circle"
                    },
                    "2": {
                        "type": "element",
                        "optics": "lens",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "n": {"eq": "1.5"},
                            "v": {"eq": "70"},
                            "th": {eq: "1000"},
                            "f_r": {"eq": "20"},
                            "b_vsa": {"eq": "15"},
                            "b_hsa": {"eq": "20"}
                        },
                        "frontShape": "circle",
                        "backShape": "ellipse"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(elements.children["1"].hap).to.equal(10);
        expect(elements.children["2"].hap).to.equal(15);
    });

    it('should compute lens faces intersection max aperture', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "lens",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "n": {"eq": "1.5"},
                            "v": {"eq": "70"},
                            "th": {eq: "25"},
                            "f_r": {"eq": "250"},
                            "b_r": {"eq": "-100"}
                        },
                        "frontShape": "circle",
                        "backShape": "circle"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(Math.abs(elements.children["1"].hap - 57.6201471791)).to.be.lessThan(.0000001);
    });

    it('should compute lens faces intersection with flat face max aperture', function () {
        var root = {
                "type": "root",
                "children": {
                    "1": {
                        "type": "element",
                        "optics": "lens",
                        "vars": {
                            "x": {"eq": "0"},
                            "y": {"eq": "0"},
                            "rot": {"eq": "0"},
                            "ap": {"eq": "1000"},
                            "n": {"eq": "1.5"},
                            "v": {"eq": "70"},
                            "th": {eq: "10"},
                            "f_r": {"eq": "25"}
                        },
                        "frontShape": "circle",
                        "backShape": "flat"
                    }
                }
            },
            vars = entity.evalVars(root),
            elements = elementOps.analyze(root, vars);

        expect(Math.abs(elements.children["1"].hap - 20)).to.be.lessThan(.0000001);
    });

    it('should compute mirror bounding box', function () {
        var bb = elementOps.boundingBox({
            type: "element",
            optics: "mirror",
            pos: {
                x: 20,
                y: 30,
                rot: 0
            },
            hap: 40,
            shape: {
                type: "circle",
                r: 50
            }
        });

        [0, -40, 20, 40].forEach(function (v, i) {
            expect(Math.abs(v - bb[i])).to.be.lessThan(1e-3);
        });
    });


    it('should compute lens bounding box', function () {
        var bb = elementOps.boundingBox({
            type: "element",
            optics: "lens",
            pos: {
                x: 0,
                y: 0,
                rot: 0
            },
            hap: 40,
            th: 10,
            frontShape: {
                type: "flat"
            },
            backShape: {
                type: "flat"
            }
        });

        [-5, -40, 5, 40].forEach(function (v, i) {
            expect(Math.abs(v - bb[i])).to.be.lessThan(1e-3, i + ': ' + v + ', ' + bb[i]);
        });
    });

    it('should compute group bounding box', function () {
        var bb = elementOps.boundingBox({
            type: "group",
            pos: {
                x: 0,
                y: 0,
                rot: 0
            },
            children: {
                '1': {
                    type: "element",
                    optics: "lens",
                    pos: {
                        x: 10,
                        y: 5,
                        rot: 0
                    },
                    hap: 40,
                    th: 10,
                    frontShape: {
                        type: "flat"
                    },
                    backShape: {
                        type: "flat"
                    }
                }
            }
        });

        [0, -35, 15, 45].forEach(function (v, i) {
            expect(Math.abs(v - bb[i])).to.be.lessThan(1e-3, i + ': ' + v + ', ' + bb[i]);
        });
    });

    it('should compute group bounding box with rotated elements', function () {
        var bb = elementOps.boundingBox({
            type: "group",
            pos: {
                x: 0,
                y: 0,
                rot: 0
            },
            children: {
                '1': {
                    type: "element",
                    optics: "lens",
                    pos: {
                        x: 10,
                        y: 5,
                        rot: 90
                    },
                    hap: 40,
                    th: 10,
                    frontShape: {
                        type: "flat"
                    },
                    backShape: {
                        type: "flat"
                    }
                }
            }
        });

        [-30, 0, 50, 10].forEach(function (v, i) {
            expect(Math.abs(v - bb[i])).to.be.lessThan(1e-3, i + ': ' + v + ', ' + bb[i]);
        });
    });

});


