/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = [
    {
        example: {type: 'circle', r: 1000},
        maxHalfAperture: 1000,
        draw: [[600, 200], [800, 400], [1000, 1000]],
        xMax: [[1000, [0, 1000]], [600, [0, 200]]],
        simX0: 1000,
        normals: [
        ]
    }, {
        example: {type: 'circle', r: -1000},
        maxHalfAperture: 1000,
        draw: [[600, -200], [800, -400], [1000, -1000]],
        xMax: [[1000, [-1000, 0]], [600, [-200, 0]]],
        simX0: -1000,
        normals: [
        ]
    }
];
