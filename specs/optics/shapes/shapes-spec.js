/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    extend = require('extend'),

    shapeOps = require('../../../src/js/logic/optics/shape-ops'),

    types = [
        require('./flat-type'),
        require('./circle-type'),
        require('./parabola-type'),
        require('./ellipse-type'),
        require('./hyperbola-type')
    ],

    test = function (tests) {
        tests.forEach(function (data, i) {
            describe(data.example.type + ' shape ' + i, function () {
                it('should compute max aperture', function () {
                    expect(shapeOps.maxHalfAperture(data.example)).to.equal(data.maxHalfAperture);
                });

                it('should compute draw points', function () {
                    data.draw.forEach(function (point) {
                        var y = point[0],
                            x = point[1],
                            cx = shapeOps.drawPoint(data.example, y);

                        expect(Math.abs(x - cx)).to.be.lessThan(.00000001);
                    });
                });

                it('should compute max and mins', function () {
                    data.xMax.forEach(function (point) {
                        var y = point[0],
                            xs = point[1],
                            cxs = shapeOps.xMax(data.example, y);

                        xs.forEach(function (x, i) {
                            expect(Math.abs(x - cxs[i])).to.be.lessThan(.00000001);
                        });
                    });
                });

                it('should calculate optimum x0 for simulation', function () {
                    expect(shapeOps.simX0(data.example, 0)).to.equal(data.simX0);
                    expect(shapeOps.simX0(data.example, 0)).to.equal(data.simX0);
                    expect(shapeOps.simX0(data.example, 10)).to.equal(data.simX0 + 10);
                    expect(shapeOps.simX0(data.example, -10)).to.equal(data.simX0 - 10);
                });

                it('should compute normals', function () {
                    data.normals.forEach(function (normalData) {
                        var point = normalData[0],
                            normal = normalData[1],
                            cn = shapeOps.normal(data.example, point);

                        normal.forEach(function (v, i) {
                            expect(Math.abs(v - cn[i])).to.be.lessThan(.00000001);
                        });
                    });
                });
            });
        });
    };

describe('shape analysis', function () {
    types.forEach(test);
});