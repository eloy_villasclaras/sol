/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


module.exports = [
    {
        example: {type: 'hyperbola', vsa: 10, hsa: 5},
        maxHalfAperture: false,
        draw: [[4, 0.385164807], [10, 2.071067812]],
        xMax: [[4, [0, 0.385164807]], [10, [0, 2.071067812]]],
        simX0: 5,
        normals: [
        ]
    },
    {
        example: {type: 'hyperbola', vsa: 10, hsa: -5},
        maxHalfAperture: false,
        draw: [[4, -0.385164807], [10, -2.071067812]],
        xMax: [[4, [-0.385164807, 0]], [10, [-2.071067812, 0]]],
        simX0: -5,
        normals: [
        ]
    }
];
