/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    opticsVarManager = require('../../src/js/logic/optics/optics-var-manager.js');

describe('optic var manager', function () {

    beforeEach(function () {
        this.lens = {
            type: 'element',
            optics: 'lens',
            frontShape: 'flat',
            backShape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                th: {eq: '25'},
                n: {eq: '1.5'},
                v: {eq: '70'}
            }
        };

        this.mirror = {
            type: 'element',
            optics: 'mirror',
            shape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'}
            }
        };
    });

    it('should set mirror shape vars', function () {
        var changes = opticsVarManager.setShape(this.mirror, 'aspheric4');

        expect(changes).to.eql({
            added: ['m_d4', 'm_d2'],
            deleted: {}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'aspheric4',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                m_d2: {eq: '0'},
                m_d4: {eq: '0.001'}
            }
        });
    });


    it('should set lens shape vars', function () {
        var changes = opticsVarManager.setShape(this.lens, 'aspheric4', 'front');

        expect(changes).to.eql({
            added: ['f_d4', 'f_d2'],
            deleted: {}
        });

        expect(this.lens).to.deep.eql({
            type: 'element',
            optics: 'lens',
            frontShape: 'aspheric4',
            backShape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                th: {eq: '25'},
                n: {eq: '1.5'},
                v: {eq: '70'},
                f_d2: {eq: '0'},
                f_d4: {eq: '0.001'}
            }
        });

        changes = opticsVarManager.setShape(this.lens, 'aspheric4', 'back');

        expect(changes).to.eql({
            added: ['b_d4', 'b_d2'],
            deleted: {}
        });

        expect(this.lens).to.deep.eql({
            type: 'element',
            optics: 'lens',
            frontShape: 'aspheric4',
            backShape: 'aspheric4',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                th: {eq: '25'},
                n: {eq: '1.5'},
                v: {eq: '70'},
                f_d2: {eq: '0'},
                f_d4: {eq: '0.001'},
                b_d2: {eq: '0'},
                b_d4: {eq: '0.001'}
            }
        });
    });

    it('should set keep vars', function () {
        this.mirror.vars['m_d4'] = {eq: '500'};

        var changes = opticsVarManager.setShape(this.mirror, 'aspheric4');

        expect(changes).to.eql({
            added: ['m_d2'],
            deleted: {}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'aspheric4',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                m_d4: {eq: '500'},
                m_d2: {eq: '0'}
            }
        });
    });

    it('should set delete unnecessary vars', function () {
        this.mirror.vars['m_u'] = {eq: '10'};

        var changes = opticsVarManager.setShape(this.mirror, 'aspheric4');

        expect(changes).to.eql({
            added: ['m_d4', 'm_d2'],
            deleted: {m_u: {eq: '10'}}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'aspheric4',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                m_d4: {eq: '0.001'},
                m_d2: {eq: '0'}
            }
        });
    });


    it('should set mirror flat vars', function () {
        var changes = opticsVarManager.setShape(this.mirror, 'flat');

        expect(changes).to.eql({
            added: [],
            deleted: {}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'flat',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'}
            }
        });
    });

    it('should set mirror circle vars', function () {
        var changes = opticsVarManager.setShape(this.mirror, 'circle');

        expect(changes).to.eql({
            added: ['m_r'],
            deleted: {}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'circle',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                m_r: {eq: '1000'}
            }
        });
    });

    it('should set mirror parabola vars', function () {
        var changes = opticsVarManager.setShape(this.mirror, 'parabola');

        expect(changes).to.eql({
            added: ['m_f'],
            deleted: {}
        });

        expect(this.mirror).to.deep.eql({
            type: 'element',
            optics: 'mirror',
            shape: 'parabola',
            vars: {
                x: {eq: '0'},
                y: {eq: '0'},
                rot: {eq: '0'},
                ap: {eq: '100'},
                m_f: {eq: '1000'}
            }
        });
    });


});
