/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


var chai = require('chai'),
    expect = chai.expect,

    extend = require('extend'),

    entity = require('../../src/js/logic/entity/entity'),
    analysisOps = require('../../src/js/logic/optics/analysis/analysis-ops'),
    elementOps = require('../../src/js/logic/optics/element-ops');

describe('element analysis', function () {

    beforeEach(function () {
        this.root = {
            children: {
                '1': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: '1',
                    vars: {a: {eq: '0'}, n: {eq: '1'}}
                },
                '2': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: '1',
                    vars: {a: {eq: '4'}, n: {eq: '1'}}
                },
                '3': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: '1',
                    vars: {a: {eq: '4'}, n: {eq: '5'}}
                },
                '4': {
                    type: 'inf-point',
                    distribution: '2d',
                    entrance: '10',
                    vars: {a: {eq: '4'}, n: {eq: '5'}}
                }
            }
        };

        this.components = {
            "type": "root",
            "children": {
                "1": {
                    "type": "element",
                    "optics": "mirror",
                    "vars": {
                        "x": {"eq": "0"},
                        "y": {"eq": "0"},
                        "rot": {"eq": "0"},
                        "ap": {"eq": "100"},
                        "hr": {"eq": "0"}
                    },
                    "shape": "flat"
                }
            }
        };

        this.componentVars = entity.evalVars(this.components);
        this.elements = elementOps.analyze(this.components, this.componentVars);

        this.vars = entity.evalVars(this.root);
        this.items = analysisOps.analyze(this.root, this.vars, this.componentVars);
    });

    it('should resolve nested elements', function () {
        var expected = {
            "1": {
                fullId: '1',
                type: 'inf-point',
                distribution: '2d',
                entrance: '1',
                a: 0,
                n: 1
            },
            "2": {
                fullId: '2',
                type: 'inf-point',
                distribution: '2d',
                entrance: '1',
                a: 4,
                n: 1
            },
            "3": {
                fullId: '3',
                type: 'inf-point',
                distribution: '2d',
                entrance: '1',
                a: 4,
                n: 5
            },
            "4": {
                fullId: '4',
                type: 'inf-point',
                distribution: '2d',
                entrance: '10',
                a: 4,
                n: 5,
                error: 'entrance'
            }
        };


        expect(this.items).to.deep.eql(expected);
    });

    it('should simulate valid analyses', function () {
        var results = analysisOps.simulate(this.elements, this.items);
        expect(Object.keys(results)).to.eql(['1', '2', '3']);

        expect(results['2'].length).to.eql(1);
        expect(results['2'][0].length).to.eql(3);

        expect(results['3'].length).to.eql(5);
    });


    it('should simulate valid analyses (async)', function (done) {
        var cb = function (results) {
            expect(Object.keys(results)).to.eql(['1', '2', '3']);

            expect(results['2'].length).to.eql(1);
            expect(results['2'][0].length).to.eql(3);

            expect(results['3'].length).to.eql(5);
            done();
        };

        analysisOps.simulateAsync(this.elements, this.items, cb);
    });
});
