/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

module.exports = function () {
    'use strict';

    var supertestSession = require('supertest-session'),

        appFactory = require('../../src/js/server/app'),
        settings = {
            "staticPath": "../../../build/dev/",
            "strategy": {
                "token": true
            },
            "knex": {
                "client": 'sqlite3',
                "connection": {
                    "filename": ':memory:'
                }
            }
        },
        app = appFactory('test', settings),
        db = require('../../src/js/server/db'),

        enums = require('../../src/js/model/enums'),

        defaultUsers = [
            {username: 'admin', isAdmin: true, isBlocked: false},
            {username: 'user1', isAdmin: false, isBlocked: false},
            {username: 'user2', isAdmin: false, isBlocked: false},
            {username: 'blocked', isAdmin: false, isBlocked: true}
        ],

        defaultDocs = [
            {owner: 'user1', status: enums.status.PUBLIC, title: 'u1pub', description: 'd', content: {}},
            {owner: 'user2', status: enums.status.PUBLIC, title: 'u2pub', description: 'd', content: {}},
            {owner: 'user1', status: enums.status.PRIVATE, title: 'u1priv', description: 'd', content: {}},
            {owner: 'user2', status: enums.status.PRIVATE, title: 'u2priv', description: 'd', content: {}},
            {owner: 'blocked', status: enums.status.PRIVATE, title: 'ubpriv', description: 'd', content: {}}
        ],

        migrated = false,

        initUser = function (user) {
            return function () {
                return db.knex('user').insert(user);
            }
        },

        initDoc = function (doc, container) {
            return function () {
                return db.createDocumentByUsername(doc.owner, doc).then(function (document) {
                    container[doc.title] = document.id;
                    return Promise.resolve();
                });
            };
        },

        prepareDb = function (obj) {
            obj.docs = {};

            var promise = migrated ? db.knex.truncate('review')
                .then(function () {
                    return db.knex.truncate('version');
                })
                .then(function () {
                    return db.knex.truncate('document');
                })
                .then(function () {
                    return db.knex.truncate('user');
                }) : db.knex.migrate.latest();

            migrated = true;

            for (var i = 0; i < defaultUsers.length; i++) {
                promise = promise.then(initUser(defaultUsers[i]));
            }

            for (i = 0; i < defaultDocs.length; i++) {
                promise = promise.then(initDoc(defaultDocs[i], obj.docs));
            }

            promise.then(function () {
                return Promise.resolve();
            });

            return promise;
        },

        check = function (done, f) {
            return function () {
                try {
                    f.apply(this, arguments);
                    done();
                } catch (e) {
                    done(e);
                }
            };
        },

        newSession = function () {
            return supertestSession(app);
        },

        login = function (agent, username, done) {
            agent.put('/auth/token')
                .send({token: username})
                .end(done);
        },


        logout = function (agent, done) {
            agent.post('/auth/logout')
                .end(done);
        };

    return {
        newSession: newSession,
        login: login,
        logout: logout,
        prepareDb: prepareDb,
        check: check
    };

}();
