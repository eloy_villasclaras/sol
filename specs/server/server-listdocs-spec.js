/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    helper = require('./helper');


describe('server functions', function () {
    beforeEach(function (done) {
        helper.prepareDb(this).then(function () {
            done();
        });
    });

    beforeEach(function () {
        this.session = helper.newSession();
    });

    afterEach(function () {
        this.session.destroy();
    });

    describe('as admin', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'admin', done);
        });

        it('should retrieve all documents', function (done) {
            this.session
                .get('/api/documents/')
                .expect(200, function (err, res) {
                    expect(res.body.length).to.equal(5);
                    done(err);
                });
        });
    });

    describe('as anonymous', function () {
        beforeEach(function (done) {
            helper.logout(this.session, done);
        });

        it('should retrieve only public documents', function (done) {
            this.session
                .get('/api/documents/')
                .expect(200, function (err, res) {
                    expect(res.body.length).to.equal(2);
                    done(err);
                });
        });
    });

    describe('as user 1', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'user1', done);
        });

        it('should retrieve only public and owned documents', function (done) {
            this.session
                .get('/api/documents/')
                .expect(200, function (err, res) {
                    expect(res.body.length).to.equal(3);
                    done(err);
                });
        });
    });
});
