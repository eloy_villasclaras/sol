/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    helper = require('./helper');


describe('server favorite functions', function () {
    beforeEach(function (done) {
        helper.prepareDb(this).then(function () {
            done();
        });
    });

    beforeEach(function () {
        this.session = helper.newSession();
    });

    afterEach(function () {
        this.session.destroy();
    });

    describe('as anonymous', function () {
        beforeEach(function (done) {
            helper.logout(this.session, done);
        });

        it('should not allow to like a document', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u1pub + '/like')
                .expect(401, function (err, res) {
                    done(err);
                });
        });
    });

    describe('as admin', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'user1', done);
        });

    });


    describe('as user 1', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'user1', done);
        });

        it('should not allow to like my own document', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u1pub + '/like')
                .expect(401, function (err, res) {
                    done(err);
                });
        });

        it('should not allow to unlike my own document', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u1pub + '/unlike')
                .expect(401, function (err, res) {
                    done(err);
                });
        });

        it('should not allow to like a private document', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u2priv + '/unlike')
                .expect(401, function (err, res) {
                    done(err);
                });
        });

        it('should allow to like others\' documents', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u2pub + '/like')
                .expect(200, function (err, res) {
                    expect(res.body.reviews).to.eql({likes: 1, mine: 1});
                    done(err);
                });
        });

        it('should just ignore request to unlike others\' not-liked documents', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u2pub + '/unlike')
                .expect(200, function (err, res) {
                    expect(res.body.reviews).to.eql({likes: 0, mine: 0});
                    done(err);
                });
        });

        describe('after liking one document', function () {
            beforeEach(function (done) {
                this.session
                    .post('/api/documents/' + this.docs.u2pub + '/like')
                    .end(done);
            });

            it('should be liked', function (done) {
                this.session
                    .get('/api/documents/' + this.docs.u2pub)
                    .expect(200, function (err, res) {
                        expect(res.body.reviews).to.eql({likes: 1, mine: 1});
                        done(err);
                    });
            });

            it('should unlike it', function (done) {
                this.session
                    .post('/api/documents/' + this.docs.u2pub + '/unlike')
                    .expect(200, function (err, res) {
                        expect(res.body.reviews).to.eql({likes: 0, mine: 0});
                        done(err);
                    });
            });

            it('should just ignore request to like others\' already-liked documents', function (done) {
                this.session
                    .post('/api/documents/' + this.docs.u2pub + '/like')
                    .expect(200, function (err, res) {
                        expect(res.body.reviews).to.eql({likes: 1, mine: 1});
                        done(err);
                    });
            });

            it('unliking a document should not affect others', function (done) {
                var me = this;
                me.session
                    .post('/api/documents/' + me.docs.u1priv + '/unlike')
                    .expect(200, function () {
                        me.session
                            .get('/api/documents/' + me.docs.u2pub)
                            .expect(200, function (err, res) {
                                expect(res.body.reviews).to.eql({likes: 1, mine: 1});
                                done(err);
                            });
                    });
            });
        });
    });

    describe('as blocked user', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'blocked', done);
        });

        it('should not allow to like a document', function (done) {
            this.session
                .post('/api/documents/' + this.docs.u1pub + '/like')
                .expect(401, function (err, res) {
                    done(err);
                });
        });

    });

});





