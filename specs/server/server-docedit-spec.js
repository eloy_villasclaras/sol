/**
 * Copyright 2015-present Eloy Villasclaras-Fernandez <eloy.villasclaras@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var chai = require('chai'),
    expect = chai.expect,

    helper = require('./helper');


describe('server functions', function () {
    beforeEach(function (done) {
        helper.prepareDb(this).then(function () {
            done();
        });
    });

    beforeEach(function () {
        this.session = helper.newSession();
    });

    afterEach(function () {
        this.session.destroy();
    });

    describe('as admin', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'admin', done);
        });
    });

    describe('as anonymous', function () {
        beforeEach(function (done) {
            helper.logout(this.session, done);
        });

        it('should not allow to create document', function (done) {
            this.session
                .post('/api/documents')
                .expect(401, function (err, res) {
                    done(err);
                });
        });
    });

    describe('as user 1', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'user1', done);
        });

        it('should allow to create document', function (done) {
            this.session
                .post('/api/documents')
                .send({title: 't0'})
                .expect(200, function (err, res) {
                    expect(res.body.title).to.equal('t0');
                    expect(res.body.description).to.equal('');
                    expect(res.body.content).to.eql({
                        components: {
                            type: 'root', children: {}
                        },
                        analysis: {
                            children: {}
                        }
                    });
                    done(err);
                });
        });

        it('should allow to clone document', function (done) {
            this.session
                .post('/api/documents')
                .send({
                    title: 't0', description: 'd', content: {
                        components: {
                            type: 'root', children: {
                                "c1": {
                                    "type": "element",
                                    "optics": "mirror",
                                    "shape": "flat",
                                    "vars": {
                                        "x": {
                                            "eq": "5"
                                        },
                                        "y": {
                                            "eq": "0"
                                        },
                                        "rot": {
                                            "eq": "0"
                                        },
                                        "ap": {
                                            "eq": "40"
                                        },
                                        "hr": {
                                            "eq": "0"
                                        }
                                    }
                                }
                            }
                        },
                        analysis: {
                            children: {}
                        }
                    }
                })
                .expect(200, function (err, res) {
                    expect(res.body.title).to.equal('t0');
                    expect(res.body.description).to.equal('d');
                    expect(res.body.content).to.deep.eql({
                        components: {
                            type: 'root', children: {
                                "c1": {
                                    "type": "element",
                                    "optics": "mirror",
                                    "shape": "flat",
                                    "vars": {
                                        "x": {
                                            "eq": "5"
                                        },
                                        "y": {
                                            "eq": "0"
                                        },
                                        "rot": {
                                            "eq": "0"
                                        },
                                        "ap": {
                                            "eq": "40"
                                        },
                                        "hr": {
                                            "eq": "0"
                                        }
                                    }
                                }
                            }
                        },
                        analysis: {
                            children: {}
                        }
                    });
                    done(err);
                });
        });

        it('should update own document title', function (done) {
            var docId = this.docs.u1priv;
            this.session
                .patch('/api/documents/' + docId)
                .send({title: 'updated title'})
                .expect(200, function (err, res) {
                    expect(res.body.id).to.equal(docId);
                    expect(res.body.vid).to.equal(2);
                    expect(res.body.title).to.equal('updated title');
                    done(err);
                });
        });

        it('should not allow to update others\' documents', function (done) {
            this.session
                .patch('/api/documents/' + this.docs.u2pub)
                .send({title: 'updated title'})
                .expect(401, function (err, res) {
                    done(err);
                });
        });
    });

    describe('as blocked user', function () {
        beforeEach(function (done) {
            helper.login(this.session, 'blocked', done);
        });

        it('should not allow to create document', function (done) {
            this.session
                .post('/api/documents')
                .send({title: 't0'})
                .expect(401, function (err, res) {
                    expect(res.body).to.eql({});
                    done(err);
                });
        });

        it('should not allow to update my documents', function (done) {
            this.session
                .patch('/api/documents/' + this.docs.ubpriv)
                .send({title: 'updated title'})
                .expect(401, function (err) {
                    done(err);
                });
        });
    });
});
