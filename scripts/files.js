var fs = require('fs');

var files = [];
var good = [];
var bad = [];

var checkFolder = function (folder) {
    fs.readdirSync(folder).forEach(function (entry) {
        var file = folder + '/' + entry;
        var stats = fs.lstatSync(file);
        if (stats.isDirectory()) {
            checkFolder(file);
        } else if (stats.isFile() && files.indexOf(file) < 0) {
            files.push(file);
        }
    });
};

var loadBundle = function () {
    var content = fs.readFileSync('./files.txt', 'utf8');
    var regex = /(\.\/src\/js\/[^\s]+)\s/g;
    var match;

    while (match = regex.exec(content)) {
        var file = match[1];
        var index = files.indexOf(file);
        if (index >= 0) {
            good.push(file);
            files.splice(index, 1);
        } else {
            bad.push(file);
        }
    }
};

checkFolder('./src/js');
loadBundle();

console.log(files.length, good.length, bad.length);
console.log(files);