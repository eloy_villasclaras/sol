var fs = require('fs'),
    p = require('../package.json'),
    libs = Object.keys(p.dependencies).concat(Object.keys(p.devDependencies)).sort(),
    output = libs.map(function (lib) {
        return '[' + lib + '](https://www.npmjs.com/package/' + lib + ')\n';
    }).join('');

fs.writeFileSync('./dd.md', output);




