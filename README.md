# Simple Optics Laboratory #

Simple Optics Laboratory (SOL) is a web simulation for optical designs. 
It allows users to create optical systems composed of mirrors and lenses,
and then simulate light rays crossing the system.

Please note that SOL is under development; a number of important features 
have not been implemented yet.

Contact: [Eloy Villasclaras](https://bitbucket.org//eloy_villasclaras)

### How do I get set up? ###

To set up the system in a local machine:

1. Set up a database and configure Knexfile accordingly.
2. ```cp src/js/client/settings/development.js src/js/client/settings/settings.js```
3. Configure ```src/js/client/settings/development.js``` (see ```default.js```)
4. ```npm install```
5. ```gulp```
6. ```node src/js/server/server-dev.js```

