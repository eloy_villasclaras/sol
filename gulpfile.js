var path = require('path'),

    gulp = require('gulp'),
    watch = require('gulp-watch'),
    mocha = require('gulp-mocha'),

    plumber = require('gulp-plumber'),
    gutil = require('gulp-util'),

    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    gulpIf = require('gulp-if'),
    cleanCSS = require('gulp-clean-css'),

    jsSpecs = './specs/**/*.js',
    jsWatch = ['./src/js/**/*.js', jsSpecs],
    cssSrc = './src/sass/**/*.scss',

    webpack = require('webpack'),

    jsTaskGen = function (mode) {
        var isProd = mode === 'prod',
            doWatch = mode === 'watch',
            output = './build/' + (isProd ? 'prod' : 'dev') + '/js/',
            plugins = isProd ? [
                new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('production')}}),
                new webpack.optimize.UglifyJsPlugin({output: {comments: false}, sourceMap: false})
            ] : [
                new webpack.DefinePlugin({'process.env': {'NODE_ENV': JSON.stringify('development')}}),
            ];


        return function () {
            return webpack({
                entry: {
                    'app': './src/js/client/index.jsx'
                },
                watch: doWatch,
                module: {
                    loaders: [{
                        test: /.jsx?$/,
                        loader: 'babel-loader',
                        exclude: /node_modules/,
                        query: {
                            presets: ['es2015', 'react']
                        }
                    }]
                },
                plugins: plugins,
                output: {
                    path: path.join(__dirname, output),
                    filename: '[name].js'
                },
                devtool: !isProd && '#inline-source-map'
            }, function (err, stats) {
                if (err) throw new gutil.PluginError("webpack", err);
                gutil.log("[webpack]", stats.toString({}));
            });
        }
    },

    cssTaskGen = function (isDev) {
        var outputPath = './build/' + (isDev ? 'dev' : 'prod') + '/css/';

        return function () {

            return gulp.src(cssSrc)
                .pipe(sass({
                    errLogToConsole: true,
                    style: 'nested'
                }))
                .pipe(autoprefixer({
                    browsers: ['last 2 versions'],
                    cascade: false
                }))
                .pipe(gulpIf(!isDev, cleanCSS()))
                .pipe(gulp.dest(outputPath));
        };
    };


gulp.task('js-dev', jsTaskGen('dev', 'web'));
gulp.task('js-watch', jsTaskGen('watch', 'web'));

gulp.task('css-dev', cssTaskGen(true));

gulp.task('css-watch', ['css-dev'], function () {
    watch(cssSrc, function () {
        gulp.start('css-dev');
    });
});


/* test */
gulp.task('test', function () {
    return gulp.src('./specs/**/*-spec.js', {read: false})
        .pipe(mocha({reporter: 'dot'}));
});

/* test logic*/

gulp.task('test-logic', function () {
    return gulp.src(['./specs/**/*-spec.js', '!./specs/server/**/*-spec.js'], {read: false})
        .pipe(mocha({reporter: 'dot'}));
});

gulp.task('test-watch', function () {
    gulp.watch(jsWatch, ['test']);
});

gulp.task('test-logic-watch', function () {
    gulp.watch(jsWatch, ['test-logic']);
});


gulp.task('watch', ['test', 'test-watch', 'css-watch', 'js-watch']);

gulp.task('default', ['watch']);

/** prod **/

gulp.task('js-prod', jsTaskGen('prod', 'web'));
gulp.task('css-prod', cssTaskGen(false));
gulp.task('build-prod', ['css-prod', 'js-prod']);
