module.exports = {

    development: {
        client: 'sqlite3',
        connection: {
            filename: './db/example.db'
        }
    },

    test: {
        client: 'sqlite3',
        connection: {
            //filename: './db/example-test.db'
            filename: ':memory:'
        }
    },

    production: {
        client: 'postgresql',
        connection: {
            database: 'sol',
            user: 'sol',
            password: 'sol',
        },
        pool: {
            min: 2,
            max: 10
        }
    }

};
